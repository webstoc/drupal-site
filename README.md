#NN/LM Drupal Site

##Overview
This repo contains a stock instance of the nnlm.gov configuration of Drupal.  It contains all the contrib and custom modules necessary to make it function properly, and has been tested and confirmed to run locally in a VirtualHost environment.

Note that this instance may have only partial functionality.  This environment relies on several non-drupal database instances to function completely, the credentials of which are not included in this repository.  Additionally, certain dependencies in sites/all/libraries reside in external git repositories themselves, and must be downloaded manually (for the time being).

##Building
To build the nnlm.gov instance of drupal:

1. clone this repository into a virtualhost directory in whatever location is appropriate for your environment.
1. Modify and run the `harden_permissions.sh` script present in the site root to set permissions appropriate for your platform.  This is important to ensure the user your webserver is running as has permissions to access the drupal site code.
1. Decompress and import *database.sql.tgz* from the site root into the database you are using for your local instance.  Update `sites/default/settings.php` to be correct for to the location where you're installing it (provide appropriate database settings and credentials.)
1. Run `drush image-flush all` and `drush cc all` to make sure no outdated references exist.
1. Log in to your new nnlm.gov drupal instance at [your local development domain name]/user with the username 'drupal_admin' and password 'oivqubeagoiubaoe'.