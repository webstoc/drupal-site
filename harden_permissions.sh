#!/usr/bin/env bash

echo_help(){
	echo "
Usage: (sudo) ./harden_permissions.sh
More info: http://drupal.org/node/244924
"
}


#Begin
if [ "$(whoami)" != "root" ]; then
	echo_help;
	echo "You need to execute this command as the root user, or with sudo."
	exit 1
fi

#begin permissions fix
origin=$(pwd)
cd "$path";

# Start with highly restrictive.
echo "Setting default permissions"
invoker=`who am i | awk '{print $1}'`
chown -R $invoker .
chgrp -R _www .
chmod -R 644 .
#set directories to be executable
chmod -R ugo+X .


echo "Granting _www write acccess to features directory"
chgrp -R _www ./sites/all/modules/features
chmod -R 775 ./sites/all/modules/features

# grant _www ability to create files
echo "Granting _www ability to create files in files directory"
chmod -R g+w ./sites/default/files/
chmod -R g+wX ./sites/default/files/
chmod -R g+x ./sites/all/drush

#end: set this script to remain executable, and root-owned
echo "Setting correct ownership and permissions for harden_permission.sh and pound-config.sh"
chown root:wheel ./harden_permissions.sh
chmod 770 ./harden_permissions.sh