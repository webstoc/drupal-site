#!/usr/bin/env bash
# WARNING WARNING WARNING: This script modifies LIVE content!  Be cautious!
#
# This file is invoked by various cron jobs on Westeros and Oscar.  
# It invokes a drush script which provides a regex output for pound 
# configuration purposes. For more information, see 
# https://bitbucket.org/webstoc/drush.
# If the output is incorrect for whatever reason, and somehow makes it into 
# production, a  line of known-good output has been provided at the 
# bottom of this file.  Comment out the drush invocation and enable the 
# echo of the known-good config until repair can be accomplished.

#/bin/echo "## Generated on "`date`
#/bin/echo "## Routes to: Legacy";
#/bin/echo "#############################################";

if [ -z "$1" ]; then
	drush @sandy-stage pound-config --noregion --regex --legacy;
	exit 0;
fi 
drush @sandy-stage pound-config --include=$1 --regex --legacy;
exit 0;

# Known good config
#echo "^(/403 Forbidden/?|/admin/?.*|/ajax/?.*|/all_regions/?.*|/atom/?.*|/batch/?.*|/ckeditor/?.*|/ckeditor_link/?.*|/ctools/?.*|/devel/?.*|/drupal_help/?|/drupal_help/css-layouts-table/?|/drupal_help/scald-fu/?|/drupal_help/simple-css-indent-block/?|/drupal_help/topics/?.*|/drupal_help/tutorials/?.*|/evaluation/?|/evaluation/about.html/?|/evaluation/about_history.html/?|/evaluation/booklets508/?.*|/evaluation/evaluation-funding-information/?|/evaluation/guides.html/?|/evaluation/outreach/?|/evaluation/reports/?.*|/evaluation/workshops/?|/file/?.*|/help/?|/help/snippets/?|/help/snippets/back-to-top/?|/help/snippets/toc/?|/linkchecker/?.*|/misc/?.*|/modules/?.*|/national/?.*|/node/?.*|/panels/?.*|/scald/?.*|/search-results/?.*|/services/?|/sites/?.*|/taxonomy/?.*|/test/?.*|/user/?.*|/users/?.*|/views/?.*|/webform/?.*)$"