(function ($) {

  $(function () {
    var googleCSEWatermark = function (id) {
      var f = $(id)[0];
      if (f && (f.query || f['edit-search-block-form--2'] || f['edit-keys'])) {
        var q = f.query ? f.query : (f['edit-search-block-form--2'] ? f['edit-search-block-form--2'] : f['edit-keys']);
        var n = navigator;
        var l = location;
        if (n.platform == 'Win32') {
          q.style.cssText = 'border: 1px solid #7e9db9; padding: 2px;';
        }
        var b = function () {
          if (q.value == '') {
            q.style.background = '#FFFFFF url(https://www.google.com/cse/intl/' + Drupal.settings.googleCSE.language + '/images/google_custom_search_watermark.gif) left no-repeat';
          }
        };
        var f = function () {
          q.style.background = '#ffffff';
        };
        q.onfocus = f;
        q.onblur = b;
//      if (!/[&?]query=[^&]/.test(l.search)) {
        b();
//      }
      }
    };
    googleCSEWatermark('#search-block-form.google-cse');
    googleCSEWatermark('#search-form.google-cse');
    googleCSEWatermark('#google-cse-results-searchbox-form');
  });

})(jQuery);
;
/**
 * From https://www.drupal.org/node/365241
 * Modifies the
 * @param  {[type]} $ [description]
 * @return {[type]}   [description]
 */
(function($) {
        Drupal.behaviors.NNLM = Drupal.behaviors.NNLM || {};
        Drupal.behaviors.NNLM.rosters = {
            attach: function(context) {
                console.log("nnlm_rosters.js boostrapped");
                    $("input#edit-field-course-developer-und-0-nnlm-rosters-fullname", context).bind('autocompleteSelect', function(event, node) {
	                    console.log('autocompleteSelect');
	                    var key = $(node).data('autocompleteValue');
	                    if(key === ''){
	                    	//TODO: deal with errors
	                    	return;
	                    }
	                    var keys = key.split('|');
	                    var peopleId = keys[0];
	                    var name = keys[1];
	                    //console.log("Hidden form", $(".staff_member_peopleid", context));
	                    $(".staff_member_peopleid", context).val(peopleId);
	                    $(this).val(name);
                    });
                }
            };
        })(jQuery);;
/**
 * @file
 * Views Slideshow Xtra Javascript.
 */
(function ($) {
  Drupal.behaviors.viewsSlideshowXtraOverlay = {
    attach: function (context) {

      // Return if there are no vsx elements on the page
      if ($('.views-slideshow-xtra-overlay').length == 0) {
        return;
      }

      // Hide all overlays for all slides.
      $('.views-slideshow-xtra-overlay-row').hide();

      var pageX = 0, pageY = 0, timeout;

      // Modify the slideshow(s) that have a vsx overlay.
      $('.views_slideshow_main').each(function() {
        var slideshowMain = $(this);

        // Get the view for this slideshow
        var view = slideshowMain.closest('.view');

        // Process the view if it has at least one overlay.
        if ($('.views-slideshow-xtra-overlay', view).length > 0) {

          // Get the View ID and Display ID so we can get the settings.
          var viewClasses = classList(view);

          $.each( viewClasses, function(index, item) {
            // We need this code because the id of the element selected will be something like:
            // "views_slideshow_cycle_main_views_slideshow_xtra_example-page"
            // We don't want to reference the string "cycle" in our code, and there is not a way to
            // get the "View ID - Display ID" substring from the id string, unless the string "cycle"
            // is referenced in a string manipulation function.

            // Get the View ID
            if((/^view-id-/).test(item)) {
              viewId = item.substring('view-id-'.length);
            }

            // Get the Display ID
            if((/^view-display-id-/).test(item)) {
              viewDisplayId = item.substring('view-display-id-'.length);
            }

          });

          if(typeof viewId != "undefined") {

            // Get the settings.
            var settings = Drupal.settings.viewsSlideshowXtraOverlay[viewId + '-' + viewDisplayId];

            // Set Pause after mouse movement setting.
            if (settings.hasOwnProperty('pauseAfterMouseMove')) {
              var pauseAfterMouseMove = settings.pauseAfterMouseMove;
              if (pauseAfterMouseMove > 0) {
                $(this).mousemove(function(e) {
                  if (pageX - e.pageX > 5 || pageY - e.pageY > 5) {
                    Drupal.viewsSlideshow.action({ "action": 'pause', "slideshowID": viewId + '-' + viewDisplayId });
                    clearTimeout(timeout);
                    timeout = setTimeout(function() {
                        Drupal.viewsSlideshow.action({ "action": 'play', "slideshowID": viewId + '-' + viewDisplayId });
                        }, 2000);
                  }
                  pageX = e.pageX;
                  pageY = e.pageY;
                });
              }
            }

          }

          // Process the overlay(s).
          $('.views-slideshow-xtra-overlay:not(.views-slideshow-xtra-overlay-processed)', view).addClass('views-slideshow-xtra-overlay-processed').each(function() {
              // Remove the overlay html from the dom
              var overlayHTML = $(this).detach();
              // Attach the overlay to the slideshow main div.
              $(overlayHTML).appendTo(slideshowMain);
          });

        }

      });
    }
  };

  Drupal.viewsSlideshowXtraOverlay = Drupal.viewsSlideshowXtraOverlay || {};

  Drupal.viewsSlideshowXtraOverlay.transitionBegin = function (options) {

    // Hide all overlays for all slides.
    $('#views_slideshow_cycle_main_' + options.slideshowID + ' .views-slideshow-xtra-overlay-row').hide();

    // Show the overlays for the current slide.
    $('#views_slideshow_cycle_main_' + options.slideshowID + ' [id^="views-slideshow-xtra-overlay-"]' + ' .views-slideshow-xtra-overlay-row-' + options.slideNum).each(function() {

      // Get the overlay settings.
      var overlay = $(this);
      var overlayContainerId = overlay.parent().attr('id');
      var settings = Drupal.settings.viewsSlideshowXtraOverlay[overlayContainerId];

      // Fade in or show overlay with optional delay.
      setTimeout(function() {
        if(settings.overlayFadeIn) {
          overlay.fadeIn(settings.overlayFadeIn);
        } else {
          overlay.show();
        }
      },
        settings.overlayDelay
      );

      // Fade out overlay with optional delay.
      if(settings.overlayFadeOut) {
        setTimeout(function() {
          overlay.fadeOut(settings.overlayFadeOut);
        },
        settings.overlayFadeOutDelay
        );
      }

    });
  };

  function classList(elem){
    var classList = elem.attr('class').split(/\s+/);
     var classes = new Array(classList.length);
     $.each( classList, function(index, item){
         classes[index] = item;
     });

     return classes;
  }

})(jQuery);
;
/**
 * Core NN/LM codespace
 *
 * For more on working with js in D7, @see https://www.drupal.org/node/304258.
 * @type {Object}
 */
(function($) {
  Drupal.behaviors.NNLM = Drupal.behaviors.NNLM || {};
  Drupal.behaviors.NNLM.core = {
    /**
     * Invoked on page load
     */
    attach: function(context, settings) {
      this.dump("* NN/LM core javascript initialized, version "+Drupal.behaviors.NNLM.core.version_number);
      this.update_origin_url(context);
      this.update_scald_links(context);
      $.each(Drupal.behaviors.NNLM.core.callbacks, function(fn) {
        fn();
      });
    },
    bind_code_mirror_editor: function(context) {
      if (window.CodeMirror === undefined) {
        //either there is an error, or we're not on an admin page.  Assume the latter for now.
        return;
      }
      if (this.style_editor !== undefined || this.script_editor !== undefined) {
        return;
      }
      var codemirror_defaults = {
        dragDrop: false,
        // Set this to the theme you wish to use (codemirror themes)
        theme: 'midnight',

        // Whether or not you want to show line numbers
        lineNumbers: true,

        // Whether or not you want to use line wrapping
        lineWrapping: true,

        // Whether or not you want to highlight matching braces
        matchBrackets: true,

        // Whether or not you want tags to automatically close themselves
        autoCloseTags: true,

        // Whether or not you want Brackets to automatically close themselves
        autoCloseBrackets: true,

        // Whether or not to enable search tools, CTRL+F (Find), CTRL+SHIFT+F (Replace), CTRL+SHIFT+R (Replace All), CTRL+G (Find Next), CTRL+SHIFT+G (Find Previous)
        enableSearchTools: true,

        // Whether or not you wish to enable code folding (requires 'lineNumbers' to be set to 'true')
        enableCodeFolding: true,

        // Whether or not to enable code formatting
        enableCodeFormatting: true,

        // Whether or not to automatically format code should be done when the editor is loaded
        autoFormatOnStart: true,

        // Whether or not to automatically format code should be done every time the source view is opened
        autoFormatOnModeChange: true,

        // Whether or not to automatically format code which has just been uncommented
        autoFormatOnUncomment: true,

        // Whether or not to highlight the currently active line
        highlightActiveLine: true,

        // Define the language specific mode 'htmlmixed' for html including (css, xml, javascript), 'application/x-httpd-php' for php mode including html, or 'text/javascript' for using java script only
        mode: 'css',

        // Whether or not to show the search Code button on the toolbar
        showSearchButton: true,

        // Whether or not to show Trailing Spaces
        showTrailingSpace: true,

        // Whether or not to highlight all matches of current word/selection
        highlightMatches: true,

        // Whether or not to show the format button on the toolbar
        showFormatButton: true,

        // Whether or not to show the comment button on the toolbar
        showCommentButton: true,

        // Whether or not to show the uncomment button on the toolbar
        showUncommentButton: true,

        // Whether or not to show the showAutoCompleteButton button on the toolbar
        showAutoCompleteButton: true

      };

      var stylesheet_field = jQuery("textarea[id^=edit-field-node-styles]").get(0);
      var script_field = jQuery("textarea[id^=edit-field-node-scripts]").get(0);
      if (stylesheet_field) {
        //this.dump("Styles field found - binding codemirror editor");
        var codemirror_styles_config = $.extend({}, codemirror_defaults);
        codemirror_styles_config.mode = 'css';
        this.style_editor = CodeMirror.fromTextArea(stylesheet_field, codemirror_styles_config);
      }
      if (script_field) {
        var codemirror_scripts_config = $.extend({}, codemirror_defaults);
        codemirror_scripts_config.mode = 'javascript';
        this.script_editor = CodeMirror.fromTextArea(script_field, codemirror_scripts_config);
      }
    },
    callbacks: {},
    dump: function(msg) {
      if (window.console !== undefined) {
        console.log(msg);
      }
    },
    update_origin_url: function(context) {
      var legacy_port_number = 7456;
      var origin_url = jQuery('.field-name-field-origin-url').find('a');
      if (!origin_url || !origin_url.attr('href')) {
        return;
      }
      origin_url.attr('href', origin_url.attr('href').replace('nnlm.gov', 'nnlm.gov:' + legacy_port_number));
    },
    /**
     * updates scald media links in a page to display the contents of the
     * atom caption for link text, if both exist.  Works in 'full' context
     * only.
     * @param  {object} context ?
     * @return {NULL}
     */
    update_scald_links: function(context) {
      console.log("Updating scald media links to use caption contents");
      var scald_links = jQuery(".dnd-atom-wrapper");
      if (scald_links.length === 0) {
        return;
      }
      scald_links.each(function(param, item) {
        var legend = jQuery(item).find('.dnd-legend-wrapper').text();
        if (!legend.trim()) {
          return;
        }
        jQuery(item).find('.dnd-legend-wrapper').text('');
        var scald_id = jQuery(item).find('.dnd-drop-wrapper').find('a:not(:has(img))').text(legend);
      });
    }
  };
  Drupal.behaviors.NNLM.core.version_number = '1.1.1';
  Drupal.behaviors.NNLM.attach = function(context, settings) {
    $.each(Drupal.behaviors.NNLM, function(codebase) {
      if(Drupal.behaviors.NNLM[codebase].attach !== undefined &&
        !Drupal.behaviors.NNLM[codebase].attached){
        Drupal.behaviors.NNLM[codebase].attach(context, settings);
        Drupal.behaviors.NNLM[codebase].attached = true;
      }
    });
  };
})(jQuery);;
(function ($) {

Drupal.extlink = Drupal.extlink || {};

Drupal.extlink.attach = function (context, settings) {
  if (!settings.hasOwnProperty('extlink')) {
    return;
  }

  // Strip the host name down, removing ports, subdomains, or www.
  var pattern = /^(([^\/:]+?\.)*)([^\.:]{4,})((\.[a-z]{1,4})*)(:[0-9]{1,5})?$/;
  var host = window.location.host.replace(pattern, '$3$4');
  var subdomain = window.location.host.replace(pattern, '$1');

  // Determine what subdomains are considered internal.
  var subdomains;
  if (settings.extlink.extSubdomains) {
    subdomains = "([^/]*\\.)?";
  }
  else if (subdomain == 'www.' || subdomain == '') {
    subdomains = "(www\\.)?";
  }
  else {
    subdomains = subdomain.replace(".", "\\.");
  }

  // Build regular expressions that define an internal link.
  var internal_link = new RegExp("^https?://" + subdomains + host, "i");

  // Extra internal link matching.
  var extInclude = false;
  if (settings.extlink.extInclude) {
    extInclude = new RegExp(settings.extlink.extInclude.replace(/\\/, '\\'), "i");
  }

  // Extra external link matching.
  var extExclude = false;
  if (settings.extlink.extExclude) {
    extExclude = new RegExp(settings.extlink.extExclude.replace(/\\/, '\\'), "i");
  }

  // Extra external link CSS selector exclusion.
  var extCssExclude = false;
  if (settings.extlink.extCssExclude) {
    extCssExclude = settings.extlink.extCssExclude;
  }

  // Extra external link CSS selector explicit.
  var extCssExplicit = false;
  if (settings.extlink.extCssExplicit) {
    extCssExplicit = settings.extlink.extCssExplicit;
  }

  // Find all links which are NOT internal and begin with http as opposed
  // to ftp://, javascript:, etc. other kinds of links.
  // When operating on the 'this' variable, the host has been appended to
  // all links by the browser, even local ones.
  // In jQuery 1.1 and higher, we'd use a filter method here, but it is not
  // available in jQuery 1.0 (Drupal 5 default).
  var external_links = new Array();
  var mailto_links = new Array();
  $("a:not(." + settings.extlink.extClass + ", ." + settings.extlink.mailtoClass + "), area:not(." + settings.extlink.extClass + ", ." + settings.extlink.mailtoClass + ")", context).each(function(el) {
    try {
      var url = this.href.toLowerCase();
      if (url.indexOf('http') == 0
        && ((!url.match(internal_link) && !(extExclude && url.match(extExclude))) || (extInclude && url.match(extInclude)))
        && !(extCssExclude && $(this).parents(extCssExclude).length > 0)
        && !(extCssExplicit && $(this).parents(extCssExplicit).length < 1)) {
        external_links.push(this);
      }
      // Do not include area tags with begin with mailto: (this prohibits
      // icons from being added to image-maps).
      else if (this.tagName != 'AREA' 
        && url.indexOf('mailto:') == 0 
        && !(extCssExclude && $(this).parents(extCssExclude).length > 0)
        && !(extCssExplicit && $(this).parents(extCssExplicit).length < 1)) {
        mailto_links.push(this);
      }
    }
    // IE7 throws errors often when dealing with irregular links, such as:
    // <a href="node/10"></a> Empty tags.
    // <a href="http://user:pass@example.com">example</a> User:pass syntax.
    catch (error) {
      return false;
    }
  });

  if (settings.extlink.extClass) {
    Drupal.extlink.applyClassAndSpan(external_links, settings.extlink.extClass);
  }

  if (settings.extlink.mailtoClass) {
    Drupal.extlink.applyClassAndSpan(mailto_links, settings.extlink.mailtoClass);
  }

  if (settings.extlink.extTarget) {
    // Apply the target attribute to all links.
    $(external_links).attr('target', settings.extlink.extTarget);
  }

  Drupal.extlink = Drupal.extlink || {};

  // Set up default click function for the external links popup. This should be
  // overridden by modules wanting to alter the popup.
  Drupal.extlink.popupClickHandler = Drupal.extlink.popupClickHandler || function() {
    if (settings.extlink.extAlert) {
      return confirm(settings.extlink.extAlertText);
    }
   }

  $(external_links).click(function(e) {
    return Drupal.extlink.popupClickHandler(e);
  });
};

/**
 * Apply a class and a trailing <span> to all links not containing images.
 *
 * @param links
 *   An array of DOM elements representing the links.
 * @param class_name
 *   The class to apply to the links.
 */
Drupal.extlink.applyClassAndSpan = function (links, class_name) {
  var $links_to_process;
  if (Drupal.settings.extlink.extImgClass){
    $links_to_process = $(links);
  }
  else {
    var links_with_images = $(links).find('img').parents('a');
    $links_to_process = $(links).not(links_with_images);
  }
  $links_to_process.addClass(class_name);
  var i;
  var length = $links_to_process.length;
  for (i = 0; i < length; i++) {
    var $link = $($links_to_process[i]);
    if ($link.css('display') == 'inline' || $link.css('display') == 'inline-block') {
      if (class_name == Drupal.settings.extlink.mailtoClass) {
        $link.append('<span class="' + class_name + '"><span class="element-invisible"> ' + Drupal.settings.extlink.mailtoLabel + '</span></span>');
      }
      else {
        $link.append('<span class="' + class_name + '"><span class="element-invisible"> ' + Drupal.settings.extlink.extLabel + '</span></span>');
      }
    }
  }
};

Drupal.behaviors.extlink = Drupal.behaviors.extlink || {};
Drupal.behaviors.extlink.attach = function (context, settings) {
  // Backwards compatibility, for the benefit of modules overriding extlink
  // functionality by defining an "extlinkAttach" global function.
  if (typeof extlinkAttach === 'function') {
    extlinkAttach(context);
  }
  else {
    Drupal.extlink.attach(context, settings);
  }
};

})(jQuery);
;
