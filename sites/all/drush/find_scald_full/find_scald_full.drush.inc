<?php
/**
 * @file
 * SCRATCH.
 */
//
// $Id$.
/**
 * Find-scald-full
 * Drush script to identify scald files that use the 'full' context within node body field.  For our purposes, this is a problem and needs to be identified and corrected.
 */

/**
 *
 */
function find_scald_full_drush_help() {
  switch ($section) {
    case 'drush:find-scald-full':
      return dt("\n\nUsage: \n>drush find-scald-full");
  }
}

/**
 *
 */
function find_scald_full_drush_command() {
  $items = array(
    'find-scald-full' => array(
      'description' => "Finds a list of atoms that use the 'full' context",
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
      'arguments' => array(),
      'options' => array(
        'published'=> 'Limits list to published items only',
        'regions'=>'Limits results to nodes with an alias beginning with the argument provided here'
      ),
      'examples' => array(
        'drush find-scald-full' => 'Shows all published content that has one or more atom with "full" context, along with the relevant content',
      ),
      'aliases' => array(),
    ),
  );
  return $items;
}


/**
 * Enter code in function body to be tested.
 *
 * @return void
 */
function drush_find_scald_full() {
  drush_log(t("Finds usage of scald files with full context in node body fields"), 'ok');
  $efq = new \EntityFieldQuery();
  $efq->entityCondition('entity_type', 'node')->entityCondition('bundle', array('basic_node', 'webform', 'course_home_page'), 'IN');
  if(drush_get_option('published', FALSE)){
    $efq->propertyCondition('status', NODE_PUBLISHED);
  }
  $efq_result = $efq->execute();
  if (empty($efq_result['node'])) {
    drush_log(t("No nodes found"), 'ok');
    return;
  }
  drush_log(t("Results:"), 'ok');
  // An example of a bad atom:
  // We want to avoid context 'full' in the results
  // <div class="dnd-atom-wrapper type- context-full"
  // contenteditable="false">
  // <div class="dnd-drop-wrapper">[scald=96439:full]</div>
  // <div class="dnd-legend-wrapper" contenteditable="true">
  // <div class="meta">Agenda</div>
  // </div>
  // </div>
  //
  print("\n##Full context usage\n");
  drush_log("\nThe following pages use full context in the page body:\n");
  $base_url = 'https://nnlm.gov';
  $included_regions = drush_get_option_list('regions');
  foreach ($efq_result['node'] as $nid => $o) {
    // if($nid != '10503'){
    //   continue;
    // }

    $url = $base_url . '/' . drupal_get_path_alias('node/' . $nid);
    if(!empty($included_regions)){
      $match = FALSE;
      foreach($included_regions as $region){
        if(preg_match("|/$region/|", $url)){
          $match = TRUE;
          break;
        }
      }
      if(!$match){
        continue;
      }
    }
    $node = node_load($nid);
    $entity = entity_metadata_wrapper('node', $node);
    // print("Testing node:" . $entity->label(), 'notice');
    $body = $entity->body->value();
    $body['value'] = preg_replace("/type-\s{1}/", 'type-file ', $body['value']);

    //drush_log("Body:".$body['value'], 'ok');
    if (preg_match("/type-file context-full/", $body['value'])) {
      print(t("\n- [$url]($url)"));
    }
  }
  print "\n\n";
}
