<?php

/**
 * @file
 */
namespace Drush\pound_config;
require_once "Segment.php";
require_once "RootSegment.php";

/**
 *
 */
/**
 * The root of the node tree we will build.
 */
class LinkTree {
  private $root = NULL;

  /**
   * Prints a message to the console, and stores it for later sending
   * in email.  This function may cause memory issues if clearLog
   * is not called between migrations.
   *
   * @param string $msg
   *   the message to be sent
   * @param string $label
   *   An optional label to describe the data
   * @param string $level
   *   one of 'notice' or 'error'
   *
   * @return NULL
   */
  public static function dump($msg, $label = '', $level = 'notice', $newline=TRUE) {
    static $filepath = FALSE;
    if ($filepath === FALSE) {
      $filepath = drush_get_option("file", NULL);
      if (!empty($filepath)) {
        try {
          if (!file_exists($filepath)) {
            if(!is_dir(dirname($filepath))){
              if(!mkdir(dirname($filepath), 0777, TRUE)){
                throw new \Exception("Could not create directory ".dirname($filepath));
              }
            }
            if (!is_writable(dirname($filepath))) {
              $filepath = -1;
              throw new \Exception("Cannot create file in directory");
            }
          }
          elseif (!is_writable($filepath)) {
            $filepath = -1;
            throw new \Exception("Cannot write to named output file");
          }
        }
        catch (\Exception $e) {
          $stderr = fopen('php://stderr', 'w');
          fwrite($stderr, $e->getMessage() . "\n");
          fclose($stderr);
          $filepath = -1;
          exit(1); //necessary?
        }
      }
      else{
        $filepath = -1;
      }
    }
    if (is_array($msg) || is_object($msg)) {
      $msg = print_r($msg, TRUE);
    }
    if(!empty($label)){
      $msg = implode(' ', array($label . ':', $msg));
    }
    switch ($level) {
      case 'mail':
        error_log($msg, 1, 'abeal@uw.edu');
        // Fall through.
      case 'error':
        $stderr = fopen('php://stderr', 'w');
        fwrite($stderr, $msg . "\n");
        fclose($stderr);
        if ($filepath !== -1) {
          $fh = fopen($filepath, 'a');
          fwrite($fh, "ERROR:" . $msg . "\n");
          fclose($fh);
        }
        break;

      default:
        if ($filepath !== -1) {
          $fh = fopen($filepath, 'a');
          fwrite($fh, $msg);
          if($newline){
            fwrite($fh, "\n");
          }
          fclose($fh);
        }
        else {
          print $msg;
          if($newline){
            print "\n";
          }
        }
        break;
    }
  }

  /**
   * Returns the amount of memory consumed by this command, in human readable format.
   *
   * @return {string} as above
   */
  private static function mem() {
    $usage = memory_get_usage(TRUE);
    $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
    return @round($usage / pow(1024, ($i = floor(log($usage, 1024)))), 2) . ' ' . $unit[$i];
  }

  /**
   * Upon construction, will build a tree representation of the
   * menu whose name was passed in the constructor.
   *
   * @param string $menu_name
   *   The name of a drupal menu.  By convention, all
   *                          NN/LM sectional menus in drupal are named
   *                          'menu-[section]-main-menu'
   *
   * @throws \Exception If    No root node (Node is named "Navigation", and
   *                           has a plid of 0) exists in the menu, or the
   *                           menu with the passed name does not exist.
   */
  public function __construct($links) {
    $this->root = new RootSegment();
    try {
      foreach ($links as $url => $status) {
        if (empty($url)) {
          self::dump("$url", "Empty URL encountered");
          continue;
        }
        // Do not modify menu items that the user has already moved.
        // Recursive callback, called with tree root and node to hang.
        switch ($status) {
          case TRUE:
          case 1:
          case '1':
            $status = Segment::PUBLISHED;
            break;

          case FALSE:
          case 0:
          case '0':
            $status = Segment::UNPUBLISHED;
            break;

          default:
            $status = Segment::NON_CONTENT;
        }
        try {
          $this->root->hang($url, $status);
        }
        catch (\Exception $e) {
          LinkTree::dump($e->getMessage(), "Skipping url '$url': ", 'error');
        }
      }
      // self::LinkTree::dump((string)$this, "Before reduction");
      if (!$noreduce = drush_get_option_list('noreduce')) {
        $this->root->optimize(TRUE);
      }
    }
    catch (\Exception$e) {
      LinkTree::dump($e->getMessage(), "Exception during link tree construction");
      throw $e;
    }
  }

  /**
   * The 'tree' is a series of nested arrays, keyed by path folder segments.  This
   * is the first step in determining whether everything in a given folder has
   * been published, and the resulting regex optimized down to a subfolder.
   *
   * @param string $url
   *   A valid nnlm.gov url
   * @param int $status
   *   1 if the url is a published node in drupal, 0 otherwise.
   */
  private function add_to_tree($url, $status) {
    $this->root->hang($url, $status);
  }
  /**
   *
   */
  public static function validate_url($url) {
    $result = filter_var($url, FILTER_VALIDATE_URL);
    if (!$result) {
      return $result;
    }
    return (!preg_match("|^http://nnlm.gov.*//.*|", $url));
  }

  /**
   * Displays the tree in a format suitable for the command line.
   *
   * @return string [description]
   */
  public function __toString() {
    return $this->root->rec_print() . "\n";
  }
  /**
   *
   */
  public function toRegex($route_to_legacy = FALSE) {
    if (drush_get_option('debug', FALSE)) {
      LinkTree::dump((($route_to_legacy) ? "Routing to legacy" : "Routing to Drupal"));
    }
    return $this->root->rec_print_regex('', $route_to_legacy);
  }

}
