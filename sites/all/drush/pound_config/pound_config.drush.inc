<?php
/**
 * @file
 * SCRATCH.
 */
//
// $Id$.
/**
 * Pound-config
 * Drush script to create pound configurations for NN/LM drupal instances.
 * This script is stored in repo.  For more information, see README.
 */
require_once "src/LinkTree.php";
use Drush\pound_config\LinkTree as LinkTree;
/**
 *
 */
function pound_config_drush_help() {
  switch ($section) {
    case 'drush:pound-config':
      return dt("\n\nUsage: \n>drush pound-config");
  }
}

/**
 *
 */
function pound_config_drush_command() {
  $items = array(
    'pound-config' => array(
      'description' => "Creates a list of url patterns of content that are meant to be handled by Drupal.  Used by the Pound reverse proxy server",
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
      'arguments' => array(),
      'options' => array(
        'debug' => "Prints additional output to allow debugging",
        'domain' => "Allows setting the domain name of the output links",
        'exclude' => "Allows exclusion of particular url patterns from the output.  This is a straight match, not a regex, so no wildcards please.  No protocol or domain, either",
        'file' => "writes the command output to the named file, instead of stdout",
        'include' => "Only includes particular url patterns that match in the output.  This is a straight match, not a regex, so no wildcards please.  No protocol or domain, either",
/* note the default as of version 2.0.0 is creating a routing pattern to
choose legacy items, with a drupal fall-through.  "legacy" has been changed
to mean "create a drupal routing pattern, for use with a legacy fall-back".*/
        'legacy' => "gives the optimization tree for drupal instead of for apache.  Note that this only has an effect in conjunction with the flags 'links' or 'regex'",
        'links' => "only outputs the links being considered in the link tree.  Useful for debugging.",
        'noreduce' => "Shows full tree output without optimization",
        'noreduce' => "Skips optimization on the output tree",
        'noregion' => "Limits output to hard coded entries",
        'protocol' => "Allows setting the protocol of the output links",
        'regex' => "Provides output as a regex pattern instead of single lines",
        'regiononly' => "Outputs *only* the regex for the region.  Assumes drupal defaults are handled elsewhere",
        'serialize' => "serializes data structures instead of giving string representations",
      ),
      'examples' => array(
        'drush pound-config' => 'Provides a configuration output for Pound, in the form of one line per pattern',
        'drush pound-config --exclude=/scr/' => 'Configuration output will exclude any links in the South Central Region (contains /scr/)',
        'drush pound-config --include=/scr/' => 'Configuration output will include only links in the South Central Region (contains /scr/)',
        'drush pound-config --domain=westeros.hsl.washington.edu' => 'Configuration output will change all links to begin with that domain (instead of nnlm.gov, most like)',
        'drush pound-config --protocol=https' => 'Configuration output will change all links to begin with https protocol',
      ),
      'aliases' => array(),
    ),
  );
  return $items;
}

/**
 * Determines if a url is a child of another.
 *
 * @param string $url1The
 *   url you believe is the parent
 *   The url you believe is the parent
 * @param string $url2The
 *   url you believe is the child
 *   The url you believe is the child
 *
 * @return boolean       TRUE if $url1 is a child of $url2, FALSE otherwise
 */
function drush_pound_is_child($url1, $url2) {
  if (is_empty($url1) || is_empty($url2)) {
    return FALSE;
  }
  if (strlen($url1) < strlen($url2)) {
    return FALSE;
  }
  $r1 = str_replace("http://nnlm.gov", '', $url1);
  $r2 = str_replace("http://nnlm.gov", '', $url2);
  $p1 = array_filter(explode('/', $r1));
  $p2 = array_filter(explode('/', $r2));
  if (count($p1) <= count($p2)) {
    // A shorter url cannot be the child of a longer one.
    return FALSE;
  }
  for ($i = 0; $i < count($p2); $i++) {
    if ($p1[$i] !== $p2[$i]) {
      // Parent url must be entirely contained within child. do not
      // hang.
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Enter code in function body to be tested.
 *
 * @return void
 */
function drush_pound_config() {

  global $databases;
  // Overrides global variable of same name.
  $base_url = 'http://nnlm.gov';
  $debugging = drush_get_option('debug', FALSE);
  //see note regarding this flag.
  $route_to_legacy = !(drush_get_option('legacy', FALSE));

  if (!isset($databases['migration_map'])) {
    LinkTree::dump("Migration map is not defined for this drupal instance; cannot continue.");
    return 1;
  }
  $primary_sections = array(
    'about',
    'evaluation',
    'funding',
    'gmr',
    'hip',
    'mar',
    'mcr',
    'ner',
    'ntc',
    'ntcc',
    'outreach',
    'pnr',
    'psr',
    'rsdd',
    'scr',
    'sea',
    'services',
    'training',
  );
  // drupal_set_message("running pound config.  Last modified: " . date ("F d Y H:i:s.", filemtime(__FILE__)));
  $links = array();
  // Paths created by drupal modules, including views.  Most of these will be admin or module backend stuff (cart, ajax, etc), but some of these will correspond to actual pages.  These should be ones that start with a "primary section" like those listed above.
  $drupal_native_links = array();
  // Paths created by node aliases.  These paths should only appear if a) they are published, and b) the map doesn't have a "Do not migrate" entry for that url (that latter method is how we control when there is a published Drupal version that is for some reason not ready for the public.)
  $drupal_published_nodes = array();
  // Any legacy urls that do not match urls that occur from one of the above two categories.  These are retrieved in order to determine where we can create optimization.
  $migration_map_links = array();
  // Add all current redirects present in Drupal to the evaluation.
  $drupal_redirects = array();
  // Add some hardcoded native exclusions that are not handled by the migration map.
  try {
    $config_defaults = require __DIR__ . "/pound_config.php";
    $hardcoded_legacy_exclusion_defaults = variable_get('pound_config_hardcoded_exclusions', '');
    if (!empty($hardcoded_legacy_exclusion_defaults)) {
      $hardcoded_legacy_exclusion_defaults = explode("\n", $hardcoded_legacy_exclusion_defaults);
    }
    else {
      $hardcoded_legacy_exclusion_defaults = $config_defaults['pound_config_hardcoded_legacy_exclusions_default'];
    }
    foreach ($hardcoded_legacy_exclusion_defaults as $i => $url) {
      $hardcoded_legacy_exclusions[$base_url . $url] = FALSE;
    }
  }
  catch (\Exception$e) {
    drupal_set_message(t("Could not read default config file"), 'error', FALSE);
    $hardcoded_legacy_exclusions = array();
  }
  unset($hardcoded_legacy_exclusion_defaults);
  // Load all redirect entities.
  $all_redirects = entity_load('redirect');
  foreach ($all_redirects as $redirect) {
    // LinkTree::dump($redirect, "Testing redirect");
    if (!isset($redirect->status)) {
      continue;
    }
    $status = (intval($redirect->status) === NODE_NOT_PUBLISHED) ? FALSE : TRUE;
    $drupal_redirects[$base_url . '/' . $redirect->source] = $status;
  }
  $connection = \Database::getConnection('default', 'migration_map');
  // Set up some default links
  // for resolving relative links
  // get all native Drupal paths.  Test as anonymous user for any paths that are in the primary sections, to see if they are published.
  drupal_save_session(FALSE);
  $original_user = $GLOBALS['user'];
  $GLOBALS['user'] = drupal_anonymous_user();
  foreach (module_invoke_all('menu') as $path => $info) {
    $parts = explode('/', $path);
    if (in_array($parts[0], $primary_sections)) {
      $machine_path = drupal_lookup_path('source', $path);
      if (!empty($machine_path)) {
        $path = $machine_path;
      }
      $item = menu_get_item($path);
      $drupal_native_links[$base_url . '/' . trim($path, '/')] = ($item && $item['access']);
      continue;
    }
    $drupal_native_links[$base_url . '/' . $parts[0] . '/*'] = TRUE;
  }
  foreach (views_get_all_views(FALSE) as $view) {
    if (!empty($view->disabled)) {
      continue;
    }
    if (empty($view->display)) {
      // Skip this view as it is broken.
      continue;
    }
    foreach ($view->display as $key => & $display) {
      if ($display->display_plugin !== 'page') {
        continue;
      }
      if (!$view->access($key)) {
        continue;
      }
      // Make sure to add wildcard at end to allow for views variables.
      $drupal_native_links[$base_url . '/' . $display->display_options['path'] . '*'] = drupal_valid_path($display->display_options['path']);
      $drupal_native_links[$base_url . '/' . $display->display_options['path'] . '/*'] = drupal_valid_path($display->display_options['path']);
    }
  }
  // Add a few custom paths that are served by, but not accounted for by the menu system
  // some css resources.
  $drupal_native_links[$base_url . '/misc/*'] = TRUE;
  $drupal_native_links[$base_url . '/modules/*'] = TRUE;
  $drupal_native_links[$base_url . '/scald/*'] = TRUE;
  $drupal_native_links[$base_url . '/users/*'] = TRUE;
  $drupal_native_links[$base_url . '/files/*'] = TRUE;

  foreach ($primary_sections as $section) {
    $drupal_native_links[$base_url . '/' . $section . '/files/*'] = TRUE;
  }
  $GLOBALS['user'] = $original_user;
  drupal_save_session(TRUE);
  // Load existing published nodes for this drupal instance.
  $efq = new \EntityFieldQuery();
  $efq_result = $efq->entityCondition('entity_type', 'node')->entityCondition('bundle', array('basic_node', 'webform', 'course_home_page'), 'IN')->propertyCondition('status', NODE_PUBLISHED)->execute();
  if (!empty($efq_result['node'])) {
    foreach ($efq_result['node'] as $nid => $o) {
      $new_url = $base_url . '/' . drupal_get_path_alias('node/' . $nid);
      // LinkTree::dump($new_url, "New URL");
      if (!LinkTree::validate_url($new_url)) {
        LinkTree::dump("The link $new_url has an invalid url, and cannot be processed.", "Pound config script error", 'mail');
        // Do not fail out for a bad url.
        continue;
      }
      if (preg_match('/\/node\//', $new_url)) {
        // Unaliased urls are handled with the 'noregion' case.
        continue;
      }
      // LinkTree::dump($status, "Published URL $new_url, Status ");
      $drupal_published_nodes[trim($new_url, '/')] = TRUE;
    }
  }
  // Retrieve all migration map entries.  Overwrite entries that have
  // published drupal equivalents, *except* where marked as "do not migrate"
  // NOTE: NO LONGER NECESSARY.
  // $mm_db_name = $databases['migration_map']['default']['database'];
  // $query_string = "
  //       SELECT mm.loc, mm.migrate
  //       FROM $mm_db_name.migration_map mm
  //       WHERE (mm.file_md5 IS NOT NULL AND mm.file_md5 <> '0')";
  // foreach ($connection->query($query_string) as $row) {
  //   $row->loc = trim($row->loc, '/');
  //   if (!LinkTree::validate_url($row->loc)) {
  //     LinkTree::dump("The link $row->loc (retrieved from the migration map) has an invalid url, and cannot be processed.", "Pound config script error", 'mail');
  //     continue;
  //   }
  //   /*if (!$route_to_legacy) {
  //     $migration_map_links[$row->loc] = (($row->migrate === 'y') ? TRUE : FALSE);
  //     continue;
  //   }*/
  //   //LinkTree::dump($row->migrate, "Value for $row->loc");
  //   if ($row->migrate !== 'l') {
  //     continue;
  //   }
  //   $migration_map_links[$row->loc] = FALSE;
  //}

  /**
   * A filtering function to remove either matching urls, or non-matching ones
   *
   * @param array $sections The primary sections to either remove or include
   * @param  array $arr The array to be filtered
   * @param  bool $include Whether to include or delete matching urls
   */
  $filter =
  function ($sections, &$arr, $include = TRUE)
  use ($primary_sections) {
    foreach ($sections as $section) {
      if (!in_array($section, $primary_sections)) {
        throw new \Exception("\Invalid exclusion '$primary_section'.  Possible choices are: " . print_r($primary_sections, TRUE));
      }
    }
    $regex = "#^http://nnlm.gov/(" . implode("|", $sections) . ")(/|$)#";
    // LinkTree::dump($regex, "Regex to match");
    foreach ($arr as $link => $value) {
      // If the link matches, and we're not supposed to include matches, remove.
      if (preg_match($regex, $link)) {
        if (!$include) {
          unset($arr[$link]);
        }
      }
      // If the link doesn't match, and we are supposed to include matches, remove.
      else {
        if ($include) {
          unset($arr[$link]);
        }
      }
    }
  };
  /*foreach ($migration_map_links as $k => $v) {
  if (!preg_match("|/pnr?/|", $k)) {
  unset($migration_map_links[$k]);
  }
  }
  ksort($migration_map_links);
  LinkTree::dump("Migration map links: " . print_r($migration_map_links, TRUE) . "\n");*/

  if ($exclude_sections = drush_get_option_list('exclude')) {
    try {
      $filter($exclude_sections, $drupal_native_links, FALSE);
      $filter($exclude_sections, $drupal_redirects, FALSE);
      $filter($exclude_sections, $drupal_published_nodes, FALSE);
      $filter($exclude_sections, $migration_map_links, FALSE);
      $filter($exclude_sections, $hardcoded_legacy_exclusions, FALSE);
    }
    catch (\Exception$e) {
      LinkTree::dump($e->getMessage(), "Exception in pound-config", "error");
      return 1;
    }
  }
  if ($include_sections = drush_get_option_list('include')) {
    try {
      $filter($include_sections, $drupal_native_links, TRUE);
      $filter($include_sections, $drupal_redirects, TRUE);
      $filter($include_sections, $drupal_published_nodes, TRUE);
      $filter($include_sections, $migration_map_links, TRUE);
      $filter($include_sections, $hardcoded_legacy_exclusions, TRUE);
    }
    catch (\Exception$e) {
      LinkTree::dump($e->getMessage(), "Exception in pound-config", "error");
      return 1;
    }
  }

  // Modify link lists based on passed options.
  if (drush_get_option('noregion', FALSE)) {
    try {
      // Exclude ALL primary sections.
      $filter($primary_sections, $drupal_native_links, FALSE);
      $filter($primary_sections, $drupal_redirects, FALSE);
      $filter($primary_sections, $drupal_published_nodes, FALSE);
      $filter($primary_sections, $migration_map_links, FALSE);
      $filter($primary_sections, $hardcoded_legacy_exclusions, FALSE);
    }
    catch (\Exception$e) {
      LinkTree::dump($e->getMessage(), "Exception in pound-config", "error");
      return 1;
    }
  }
  elseif (drush_get_option('regiononly', FALSE)) {
    try {
      // Exclude ALL primary sections.
      $filter($primary_sections, $drupal_native_links, TRUE);
      $filter($primary_sections, $drupal_redirects, TRUE);
      $filter($primary_sections, $drupal_published_nodes, TRUE);
      $filter($primary_sections, $migration_map_links, TRUE);
      $filter($primary_sections, $hardcoded_legacy_exclusions, TRUE);
    }
    catch (\Exception$e) {
      LinkTree::dump($e->getMessage(), "Exception in pound-config", "error");
      return 1;
    }
  }
  // The original version of this software always generated routings to
  // drupal content, with a legacy fall-through.  This reverses that paradigm,
  // and generates legacy regular expressions instead, relying on
  // content in the migration map to be marked as "legacy" if it is meant
  // to be served from legacy, and ignoring it completely if not.
  // Assemble.
  try {
    // $links = $drupal_native_links + $drupal_published_nodes + $drupal_redirects + $hardcoded_legacy_exclusions;
    $links = array_merge($drupal_published_nodes, $drupal_native_links, $drupal_redirects, $hardcoded_legacy_exclusions);

    // LinkTree::dump("Migration map links: " . print_r($migration_map_links, TRUE) . "\n");
    // LinkTree::dump(($route_to_legacy) ? "Routing to legacy" : "Routing to Drupal");
    foreach ($migration_map_links as $link => $published) {
      // LinkTree::dump("testing link $link\n");
      // If migrate is 'l' in the migration map, this entry MUST be served
      // from legacy.
      if ($published === FALSE) {
        if ($debugging) {
          LinkTree::dump("\tForcing entry $link to legacy\n");
        }
        $links[$link] = FALSE;
        continue;
      }
      // If no drupal entry exists and published is not FALSE, route to legacy
      // only if in classic mode (where legacy is fallback).
      if (!isset($links[$link])) {
        if (!$route_to_legacy) {
          if ($debugging) {
            LinkTree::dump("No entry for $link, establishing\n");
          }
          $links[$link] = FALSE;
        }
        continue;
      }
      // If *not* empty, and migrate is not 'l', then the Drupal link is
      // the definitive version.
    }
    ksort($links);
    if (drush_get_option('links', FALSE)) {
      if (drush_get_option('serialize', FALSE)) {
        $links = serialize($links);
        LinkTree::dump($links);
      }
      else {
        if ($debugging) {
          LinkTree::dump("Published Nodes: " . print_r($drupal_published_nodes, TRUE) . "\n");
          LinkTree::dump("Native links: " . print_r($drupal_native_links, TRUE) . "\n");
          LinkTree::dump("Redirects: " . print_r($drupal_redirects, TRUE) . "\n");
          LinkTree::dump("Hardcoded: " . print_r($hardcoded_legacy_exclusions, TRUE) . "\n");
        }
        foreach($links as $url=>$drupal){
          $status = ($drupal) ? 'drupal' : 'legacy';
          LinkTree::dump($url . '->' . $status);
        }
      }
      return 0;
    }
    // return;
    $tree = new LinkTree($links);
  }
  catch (Exception$e) {
    drush_log("An exception has occurred. This command cannot continue", "error", $e->getMessage());
  }
  if ($regex = drush_get_option('regex', FALSE)) {
    LinkTree::dump($tree->toRegex($route_to_legacy), '', '', FALSE);
  }
  else {
    LinkTree::dump((string) $tree);
  }
  // Print (string)$tree;
  return 0;
}
