<?php

use Drupal\nnlm_core\Libraries;

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 *
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */

/**
 * Override or insert variables into the html template.
 */
function nnlm_preprocess_html(&$vars) {
  /* Theme script - (I'm getting sick of trying to figure out why it won't load
    in the .info file.) */
  drupal_add_js(drupal_get_path('theme', 'nnlm').'/js/script-1.2.4.js', array(
    'type'=>'file',
    'group'=>JS_THEME,
    'every_page'=>TRUE,
    'defer'=>FALSE,
    'weight'=>0,
    'cache'=>TRUE,
    'requires_jquery'=>TRUE,
    'preprocess'=>FALSE
  ));
  // Add conditional CSS for IE7 and below.
  //global script additions
  if (!module_exists('libraries')) {
    watchdog('Libraries module missing', 'The required module "Libraries" was not found');
    return;
  }

  /* External styles */
  //maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css
  $external_styles = array(
    'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'
  );
  foreach($external_styles as $url){
    drupal_add_css($url, array(
      'type'=>'file',
      'group' => CSS_THEME,
      'external'=>TRUE,
      'every_page' => TRUE,
      'media'=>'all',
      'preprocess'=>TRUE));
  }
  /* Theme scripts */


  $theme_scripts = array(
    'Google font loader' => '//ajax.googleapis.com/ajax/libs/webfont/1.5.10/webfont.js'
  );
  if (!empty($theme_scripts)) {
    foreach ($theme_scripts as $name => $url) {
      drupal_add_js($url, array('type' => 'external', 'scope' => 'footer', 'group' => JS_THEME, 'every_page' => TRUE));
    }
  }

  // Setup IE meta tag to force IE rendering mode
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'content' => 'IE=edge,chrome=1',
      'http-equiv' => 'X-UA-Compatible',
    ),
    '#weight' => '-99999',
  );

  // Add header meta tag for IE to head
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');
}

function nnlm_preprocess_region(&$node) {
  //nnlm_core_dump($vars, "Preprocessing region. Vars ");
  if (in_array($node['elements']['#region'], array('content'))) {
    $theme = alpha_get_theme();
    //dpm("Preprocessing region ", $node['elements']['#region']);
    switch ($node['elements']['#region']) {
      case 'content':
        $vars['breadcrumb'] = $theme->page['breadcrumb'];
        //nnlm_core_dump($node, "Content");
        break;
      default:
      break;
    }
  }
}

function nnlm_preprocess_node(&$node){
  if(function_exists('nnlm_preprocess_node_'.$node['nid'])){
    call_user_func('nnlm_preprocess_node_'.$node['nid'], $node);
  }
}
/**
 * Home page.  Add js for custom map work.
 * @param  array &$node The node being customized
 * @return NULL
 */
function nnlm_preprocess_node_3540(&$node){
  //dpm(__FUNCTION__);
  Libraries::load('d3');
}

