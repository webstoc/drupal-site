(function($) {
  Drupal.behaviors.NNLM = Drupal.behaviors.NNLM || {};
  Drupal.behaviors.NNLM.theme = {
    /**
     * Invoked on page load by nnlm_core module's js
     */
    attach: function(context, settings) {
      this.dump("* NN/LM theme javascript initialized, version "+Drupal.behaviors.NNLM.theme.version_number);
      //this.bind_code_mirror_editor(context); //handled in core
      //this.style_navigational_menu(context);
      this.bind_google_cse(context);
      this.back_to_top_buttons(context);
      this.set_webfonts(context);
      this.replace_svg_files_with_inline_equivalents(context);
      //dev only
      var next_scheduled = new Date();
      if (window.location.href.indexOf("drupal.dev") > -1) {
        jQuery.getScript('script-dev.js', function(){
          console.log("development scripts loaded");
        });
      }
      else if (window.location.href.indexOf("sandy-dev") > -1) {
        console.log("Development environment detected");
        var days_until_this_saturday = Math.abs(next_scheduled.getDay() - 6);
        //console.log("Days until saturday: " + days_until_this_saturday);
        next_scheduled.setDate(next_scheduled.getDate() + days_until_this_saturday);
        next_scheduled.setHours(0);
        next_scheduled.setMinutes(0);
        next_scheduled.setSeconds(0);
        $("#overwrite-timer").text(next_scheduled.toLocaleString());
      }
      else if (window.location.href.indexOf("sandy-stage") > -1){
        console.log("Staging environment detected");
        next_scheduled.setDate(next_scheduled.getDate() + 1); //tomorrow at 12AM
        next_scheduled.setHours(0);
        next_scheduled.setMinutes(0);
        next_scheduled.setSeconds(0);
        console.log("Next Scheduled: " + next_scheduled);
        $("#overwrite-timer").text(next_scheduled.toLocaleString());
      }
    },
    callbacks: {},
    bind_callback: function(id, fn){
      Drupal.behaviors.NNLM.theme.callbacks['#'+id] = fn;
    },
    bind_google_cse: function(context){
      //@deprecated: implementation moved to nnlm_google_cse module.
    },
    dump: function(msg) {
      Drupal.behaviors.NNLM.core.dump(msg);
    },
    replace_svg_files_with_inline_equivalents: function(context) {
      this.dump("Replacing svg with inline equivalents");
      jQuery('img.svg', context).each(function() {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        console.log("replacing svg for element id ", imgID, 'imgURL', imgURL);
        //requires jQuery >= 1.5 for promise implementation
        jQuery.ajax({
          'url': imgURL,
          'dataType': 'xml',
          'context': context
        }).done(function(data, textStatus, jqXHR){
          console.log("Success from replace_svg_files_with_inline_equivalents");

          // Get the SVG tag, ignore the rest
          var $svg = jQuery(data, context).find('svg');
          console.log("SVG: ", $svg);
          if($svg.length === 0){
            console.log("No SVG to replace.");
            return;
          }
          //Add replaced image's ID to the new SVG
          if (typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
          }
          // Add replaced image's classes to the new SVG
          if (typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass + ' replaced-svg');
          }

          // // Remove any invalid XML tags as per http://validator.w3.org
          $svg = $svg.removeAttr('xmlns:a');
          console.log("New svg", $svg);
          // // Replace image with new SVG
          $img.replaceWith($svg);
          console.log("Looking for callback with id", imgID);
          if (typeof imgID !== 'undefined' && Drupal.behaviors.NNLM.theme.callbacks['#'+imgID] !== undefined){
            console.log("Invoking callback with id ", imgID);
            Drupal.behaviors.NNLM.theme.callbacks['#'+imgID]();
          }
          else{
            console.log("No callback found, or no id set on svg.  Probably fine.");
          }
        }).fail(function(jqXHR, textStatus, errorThrown) {
          console.error("FAIL from replace_svg_files_with_inline_equivalents function, jqXHR", jqXHR, 'textstatus', textStatus, 'errorThrown', errorThrown);
        }).always(function() {
          console.log("Finished from replace_svg_files_with_inline_equivalents function");
        });
      });
    },
    set_webfonts: function(context) {
      if (typeof WebFont === 'undefined') {
        this.dump("Skipping custom fonts - Google WebFonts loader not available");
        return;
      }
      var web_font_list = [
        'Roboto',
        'Cinzel'
      ];
      this.dump("binding " + web_font_list[0] + " webfont");
      WebFont.load({
        google: {
          families: web_font_list
        }
      });
    },
    back_to_top_buttons: function(context) {
      var duration = 500;
      jQuery('.back-to-top,.toc-back-to-top a').click(function(event) {
        event.preventDefault();
        jQuery('html, body').animate({
          scrollTop: 0
        }, duration);
        return false;
      });
    }
  };

  Drupal.behaviors.NNLM.theme.version_number = '1.2.4';
})(jQuery);
console.log("NN/LM Theme script.js loaded");