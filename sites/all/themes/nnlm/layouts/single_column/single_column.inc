<?php

// Plugin definition
$plugin = array(
  'title' => t('Single Column'),
  'category' => t('NN/LM theme specific layouts'),
  'icon' => 'single_column.png',
  'theme' => 'single_column',
  'css' => 'single_column.css',
  'regions' => array(
    'center' => t('Center content')
  ),
);
