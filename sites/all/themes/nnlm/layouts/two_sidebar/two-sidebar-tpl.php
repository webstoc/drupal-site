<?php
/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a two column panel display layout, with
 * each column roughly equal in width.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['left']: Content in the left column.
 *   - $content['right']: Content in the right column.
 */
?>
<div class="panel-display panel-left-sidebar clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <aside id="region-sidebar-first" class="sidebar grid-6 region region-sidebar-first equal-height-element">
    <div class="region-inner region-sidebar-first-inner">
      <?php print $content['left']; ?>
    </div>
  </aside>

  <div class="grid-12 region region-content equal-height-element">
    <div class="inside"><?php print $content['center']; ?></div>
  </div>
  <aside id="region-sidebar-second" class="sidebar grid-6 region region-sidebar-second equal-height-element">
    <div class="region-inner region-sidebar-second-inner">
      <?php print $content['right']; ?>
    </div>
  </aside>
</div>