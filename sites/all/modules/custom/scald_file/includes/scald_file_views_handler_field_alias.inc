<?php

/**
 * @file
 * Provides a field showing the atom alias
 */

/**
 * scald_file_views_handler_field_alias class.
 */
class scald_file_views_handler_field_alias extends views_handler_field {

  /**
   * Overrides views_handler_field::render().
   */
  public function render($atom) {
    return url("atom/".$atom->sid);
  }
}

