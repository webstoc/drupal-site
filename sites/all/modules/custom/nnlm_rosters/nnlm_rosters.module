<?php
/**
 * @file
 * .
 */
Use Drupal\nnlm_rosters\Utilities as U;
use Drupal\nnlm_rosters\HashTreeNode as HashTreeNode;
//use Drupal\nnlm_rosters\rest\SymfonyServer as SymfonyServer; //cannot use until php 5.3.9
/**
 * @file
 */

/**
 * Implements hook_init.
 *
 * @return null
 */
function nnlm_rosters_init() {
  // Installed via composer in the build step.
  require_once (join(DIRECTORY_SEPARATOR, array(
  drupal_get_path('module', 'nnlm_rosters'),
  'vendor',
  'autoload.php'
  )));
  \Twig_Autoloader::register();
  // All permissions implemented in system.
}
function nnlm_rosters_permission() {
  return array(
    'read rosters services data'=> array(
      'title' => t('Read NN/LM Rosters Services Data'),
      'description' => t('Perform READ operations on the services endpoint exposed by this module (at api/rosters/v1) (also grants INDEX).'),
      'restrict access' => FALSE
    )
  );
}
/**
 * Implements hook_help
 * returns help about this module.
 *
 * @param array $path
 *   The router menu path
 * @param array $arg
 *   An array that corresponds to the return value of the arg() function
 *
 * @see http://drupalcontrib.org/api/drupal/drupal%21modules%21help%21help.api.php/function/hook_help/7
 */
function nnlm_rosters_help($path, $arg) {
  if (strpos($path, 'admin/config/system/nnlm/rosters') === FALSE) {
    return;
  }
  $output = '';
  $output .= '<h3>' . t('Help') . '</h3>';
  $output .= '<p>' . t('Forthcoming...') . '</p>';
  return $output;
}

/**
 * Implements hook_admin
 * Settings provided:
 *   Turn on and off the display of rosters token data in nodes.
 */
function nnlm_rosters_admin() {
  $form = array();
  $form['nnlm_rosters_display_token_data'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display rosters token data'),
    '#default_value' => variable_get('nnlm_rosters_display_token_data', FALSE),
    '#description' => t('Adds rosters token data to the editing display'),
    '#required' => FALSE,
  );
  $form['nnlm_rosters_token_data_types'] = array(
    '#type' => 'checkboxes',
    '#options' => node_type_get_names(),
    '#title' => t('Eligible content types'),
    '#default_value' => variable_get('nnlm_rosters_token_data_types', FALSE),
    '#description' => t('Determines content types eligible for tokenization'),
    '#required' => FALSE,
  );
  return system_settings_form($form);
}

/**
 * Implements hook_form_BASE_FORM_ID_alter
 * Tasks handled by this hook instance:
 *   Conditional addition of rosters token information to the node editing
 *   interface.
 *
 * @param array $form
 *   The form to be modified
 * @param array $form_state
 *   The form state
 * @param string $form_id
 *   The id of the form
 *
 * @return null
 */
function nnlm_rosters_form_node_form_alter(&$form, &$form_state, $form_id) {
  // drupal_add_js(
  //   'https://cdnjs.cloudflare.com/ajax/libs/react/0.14.2/react.min.js',
  //   'external'
  // );
  if (!variable_get('nnlm_rosters_display_token_data', FALSE)) {
    return;
  }
  $tokens = array(
    '#type' => 'fieldset',
    '#title' => t('List of NN/LM Rosters tokens'),
    '#description' => t('The listed tokens may be used in the body field to generate NN/LM Rosters content.'),
    '#theme' => 'token_tree',
    '#attributes' => array('class' => array('collapsible', 'collapsed')),
    // The token types that have specific context. Can be multiple token types like 'term' and/or 'user'
    '#token_types' => array('rosters'),
    // A boolean TRUE or FALSE whether to include 'global' context tokens like [current-user:*] or [site:*]. Defaults to TRUE.
    '#global_types' => FALSE,
    // A boolean whether to include the 'Click this token to insert in into the the focused textfield' JavaScript functionality. Defaults to TRUE.
    '#click_insert' => FALSE,
    '#weight' => 4,
    '#attached' => array('js' => array('misc/collapse.js', 'misc/form.js')),
  );
  if (isset($form['#groups']['group_title_and_body'])) {
    $g = 'group_title_and_body';
    $form['tokens'] = &$tokens;
    $form['tokens']['#group'] = $g;
    $form['#group_children']['tokens'] = $g;
    array_push($form['#groups'][$g]->children, 'tokens');
  }
  else {
    $form['tokens'] = &$tokens;
  }
  // firep($form);
}

/**
 * Implements hook_menu
 * Creates the settings page in Drupal for the various Rosters configurable
 * settings.
 *
 * @return array The drupal definition for the settings page.
 */
function nnlm_rosters_menu() {
  $items = array();
  // drupal_set_message('firing nnlm_core_menu');
  //for info on types,
  //@see https://api.drupal.org/api/drupal/includes%21menu.inc/group/menu_item_types/7
  $items['admin/config/system/nnlm/rosters'] = array(
    'title' => 'Rosters Settings',
    'description' => 'Settings relevant to the NN/LM Rosters libraries',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nnlm_rosters_admin'),
    'access arguments' => array('administer rosters settings'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['rosters/people/autocomplete'] = array(
    'title' => 'Staff Autocomplete',
    'description' => 'Autocomplete path for rosters staff records',
    'page callback' => 'nnlm_rosters_people_autocomplete',
    'access arguments' => array('view the administration theme'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}
/**
 * Implements hook_field_info
 * Provides a rendering and a selector for people records from the rosters
 * database.
 *
 * @return array The field info as per the hook spec.
 */
function nnlm_rosters_field_info() {
  return array(
    'nnlm_rosters_record_person' => array(
      'label' => t('NN/LM Rosters Staff Record'),
      'description' => t('A staff record from the NN/LM Rosters System'),
      'default_widget' => 'nnlm_rosters_record_person_widget',
      'default_formatter' => 'nnlm_rosters_record_person_formatter',
    ),
  );
}

/**
 * Implements hook_field_widget_info
 * Part of the custom lookup field implementation for Rosters.
 * @return array A description of the widget.  See docs.
 */
function nnlm_rosters_field_widget_info() {
  return array(
    'nnlm_rosters_record_person_widget' => array(
      'label' => t('NN/LM Rosters Staff Record'),
      'field types' => array('nnlm_rosters_record_person'),
    ),
  );
}
/**
 * Implements hook_field_widget_form
 * Necessary for field api handling of custom field.
 *
 * Important parameters:
 * $items stores the data entered by the user. It is an array of $item arrays, keyed by each item's $delta. If you have a multi-value field instance, there will be more than one item; if your field instance has just one value, then there will be one item with the key 0.
 * $element is the value this function will return. It uses the Form API's form definition structure, so it will probably look familiar to you. It is passed to poutine_maker_field_widget_form() with some existing values documented here, so make sure you don't overwrite it.
 */
function nnlm_rosters_field_widget_form(&$form,
                                         &$form_state,
                                         $field,
                                         $instance,
                                         $langcode,
                                         $items,
                                         $delta,
                                         $element) {
/*  dpm($form, "Form");
  dpm($form_state, "Form state");
  dpm($field, "field");
  dpm($instance, "instance");*/
  // dpm($items, "Items");
  // dpm($delta, "Delta");
  if($instance['widget']['type'] !== 'nnlm_rosters_record_person_widget'){
    return $element;
  }
  $element['nnlm_rosters_fullname'] = array(
    '#title' => (isset($instance['label'])) ? $instance['label'] : t('NN/LM Staff Member Name'),
    '#description' => ($instance['description']) ?: t('An NN/LM staff member (autocomplete field)'),
    '#type' => 'textfield',
    '#required' => ($instance['required']) ?: 0,
    '#maxlength' => 100,
    // Use #default_value to prepopulate the element
    // with the current saved value.
    '#default_value' => isset($items[$delta]['nnlm_rosters_fullname']) ? $items[$delta]['nnlm_rosters_fullname'] : '',
    '#autocomplete_path' => 'rosters/people/autocomplete',
  );
  $element['nnlm_rosters_people_id'] = array(
    '#title' => t('Rosters PeopleID'),
    '#description' => t('The NN/LM Staff Rosters PeopleID. Populated with javascript from the bound autocomplete select action'),
    '#type' => 'hidden',
    // Use #default_value to prepopulate the element
    // with the current saved value.
    '#attributes' => array('class' => array('staff_member_peopleid')),
    '#default_value' => isset($items[$delta]['nnlm_rosters_people_id']) ? $items[$delta]['nnlm_rosters_people_id'] : '',
  );
  return $element;
}
/**
 * Note: copied from classfiles/server/utils.  DRY violation.
 *
 * @param string $query
 *   The string to be crunched into a name
 *
 * @return array        An array containing the following keys:
 *                         'Prefix', 'FirstName', 'MiddleName',
 *                         'LastName', 'Suffix', based on a
 *                         "best guess" from the way the
 *                         string was formatted.
 */
function nnlm_rosters_name_crunch($query) {
  $result = array();
  $result['join_type'] = db_or();
  $query = trim($query);
  if (strpos($query, ',')) {
    // 1 comma: [honorific] lastname, firstname middle.
    $parts = explode(',', $query);
    array_push($parts, array_shift($parts));
  }
  else {
    $parts = explode(' ', $query);
  }
  foreach ($parts as $k => $v) {
    if (empty($v)) {
      continue;
    }
    $parts[$k] = trim($v);
  }
  // 1: firstname only
  // 2: first, then last name (commas already processed)
  // 3: first, middle, last
  // 4: prefix, first, middle, last (had to pick one; will mess up some cases)
  // 5: prefix, first, middle, last, suffix
  // more: count only the first 5.
  if (count($parts) > 1) {
    $result['join_type'] = db_and();
  }
  switch (count($parts)) {
    case 1:
        $result['FirstName'] = ($parts[0]);
      $result['LastName'] = ($parts[0]);
      break;

    case 2:

      $result['FirstName'] = ($parts[0]);
      $result['LastName'] = ($parts[1]);
      break;

    case 3:
        $result['FirstName'] = ($parts[0]);
      $result['MiddleName'] = ($parts[1]);
      $result['LastName'] = ($parts[2]);
      break;

    case 4:
    case 5:
    default:
        $result['Prefix'] = ($parts[0]);
      $result['FirstName'] = ($parts[1]);
      $result['MiddleName'] = ($parts[2]);
      $result['LastName'] = ($parts[3]);
      if (isset($parts[4])) {
        $result['Suffix'] = ($parts[4]);
      }
  }

  return $result;
}
/**
 * Widget function used in nnlm_rosters_field_info
 **/
function nnlm_rosters_record_person_widget() {
  dpm(__FUNCTION__);
}
/**
 * Implements hook_field_formatter_info
 * @return array The array descriptor for the field.  See docs for info
 * Formatter info used by nnlm_rosters_record_person field
 */
function nnlm_rosters_field_formatter_info(){
  return array(
    // This formatter just displays the hex value in the color indicated.
    'nnlm_rosters_record_person_formatter' => array(
      'label' => t('Rosters Record Default Formatter'),
      'field types' => array('nnlm_rosters_record_person'),
    )
  );
}
/**
 * Implements hook_field_formatter_view
 * Formatter function used by nnlm_rosters_record_person field
 */
function nnlm_rosters_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  // dpm(__FUNCTION__);
  // dpm($items, "Items");
  switch ($display['type']) {
    // This formatter simply outputs the field as text and with a color.
    case 'nnlm_rosters_record_person_formatter':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          // We create a render array to produce the desired markup,
          // "<p style="color: #hexcolor">The color code ... #hexcolor</p>".
          // See theme_html_tag().
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $item['nnlm_rosters_fullname']
        );
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_field_is_empty.
 *
 * Necessary to skip validation and save step for empty fields.
 * TODO: implement.
 */
function nnlm_rosters_field_is_empty($item, $field) {
  if(empty($item['nnlm_rosters_fullname']) && empty($item['nnlm_rosters_people_id'])){
    return TRUE;
  }
  return FALSE;
}

/**
 * Autocomplete function for nnlm_rosters_record_person field.
 * @param  string $searchString The search string passed to the
 * text field and sent via the autocomplete mechanism.  Note that
 * it is a single value for the full name, and must be separated
 * (using the nnlm_rosters_name_crunch helper function)
 * @return  NULL (side effect: output search results in JSON format)
 */
function nnlm_rosters_people_autocomplete($string) {
  $people_map =& drupal_static('nnlm_rosters_people_autocomplete_data');
  $result = array();
  if (strlen($string) <= 3) {
    return drupal_json_output($result);
  }
  // The commented code below attempts to use a custom backend to pre-cache
  // the data.  Speed results were negligible, and this is maintained temporarily
  // for reference only.
  //
  // cache_clear_all('nnlm_rosters_people_autocomplete_data', 'cache'); //testing
  // if (empty($people_map)) {
  // if ($cache = cache_get('nnlm_rosters_people_autocomplete_data')) {
  // $people_map = $cache->data;
  // }
  // else {
  // dfb("Rebuilding cache");
  // $people_map = new \stdClass();
  // $people_map->data = array();
  // $people_map->FirstNameMap = new HashTreeNode();
  // $people_map->LastNameMap = new HashTreeNode();
  // $connection = \Database::getConnection('default', 'rosters');
  // $result = $connection->select('currentStaff', 'c')
  // ->fields('c', array('PeopleID', 'FirstName', 'LastName'))
  // ->orderBy('LastName')
  // ->orderBy('FirstName')
  // ->execute();

  // foreach ($result as $row) {
  // $people_map->data['_'.$row->PeopleID] = $row;
  // // Reference by both first and last name.
  // $people_map->FirstNameMap->add($row->FirstName, $row->PeopleID);
  // $people_map->LastNameMap->add($row->LastName, $row->PeopleID);
  // }
  // dfb($people_map->data, "PeopleMap");
  // cache_set('nnlm_rosters_people_autocomplete_data', $people_map, 'cache', time() + (3600*24)); //1 day cache
  // }
  // }
  // $name_parts = nnlm_rosters_name_crunch($string);

  // $data =& $people_map->data;

  // $result = array();
  // if (!empty($name_parts['FirstName'])) {
  // $map =& $people_map->FirstNameMap;
  // $keys = $map->find($name_parts['FirstName']);
  // foreach($keys as $k){
  // $value = $data['_'.$k]->LastName . ', ' . $data['_'.$k]->FirstName . ' ('.trim($k, '_').')';
  // $result[$value] = $value;
  // }
  // }
  // if (!empty($name_parts['LastName'])) {
  // $map =& $people_map->LastNameMap;
  // $keys = $map->find($name_parts['LastName']);
  // foreach($keys as $k){
  // $value = $data['_'.$k]->LastName . ', ' . $data['_'.$k]->FirstName . ' ('.trim($k, '_').')';
  // $result[$value] = $value;
  // }
  // }

  //dfb($string, "Autocomplete search string");
  $name_parts = nnlm_rosters_name_crunch($string);
  //dfb($name_parts, "Autocomplete parts");
  $join = $name_parts['join_type'];

  $join->condition("c.FirstName", '%' . $name_parts['FirstName'] . '%', 'LIKE');
  $join->condition("c.LastName", '%' . $name_parts['LastName'] . '%', 'LIKE');
  $connection = \Database::getConnection('default', 'rosters');
  $query_result = $connection->select('currentStaff', 'c')
    ->fields('c', array('PeopleID', 'FirstName', 'LastName'))
    ->condition($join)
    ->orderBy('LastName')
    ->orderBy('FirstName')
    ->execute()->fetchAll();
  foreach ($query_result as $row) {
    $fullName = $row->FirstName . ' ' . $row->LastName;
    $result[$row->PeopleID.'|'.$fullName] = $fullName;
  }
  asort($result);
  //dfb($result, "Autocomplete results");
  // Save the query to matches.
  // Return the result to the form in json.
  drupal_json_output($result);
}


/**
 * NOT USED.
 * A sample function that would theoretically connect and
 * authenticate to the sandy-dev drupal instance before
 * performing a request on the exposed REST endpoint.  This method
 * is not currently being used, and this function is retained
 * for reference only.
 */
// function nnlm_rosters_services_request($endpoint) {
//   $base_url = 'https://ws-sandy-dev.hsl.washington.edu/api/rosters/v1';
//   $login_url = 'https://ws-sandy-dev.hsl.washington.edu/user';
//   $data = array(
//     'name' => '[custom services username]',
//     'pass' => '[add password here]',
//   );
//   $data = drupal_json_encode($data);
//   $options = array(
//     'headers' => array(
//       'Content-Type' => 'application/json',
//     ),
//     'method' => 'POST',
//     'data' => $data,
//   );

//   $response = drupal_http_request($login_url, $options);
//   dfb("User login", $response);
//   $data = json_decode($response->data);

//   // Check if login was successful.
//   if ($response->code == 200) {
//     // Now recycle the login cookie we received in the first request.
//     $options['headers']['Cookie'] = $data->session_name . '=' . $data->sessid;

//     $options['headers']['Content-Type'] = 'application/x-www-form-urlencoded';
//     // Get info about a user.
//     $data = array();
//     $options['data'] = http_build_query($data, '', '&');
//     $options['method'] = 'GET';
//     $response = drupal_http_request($base_url . '/' . $endpoint, $options);
//     dfb("User info", $response);
//   }
// }
/**
 * Implements hook_ctools_plugin_api().
 * Declares the rosters services endpoint (stored in
 * nnlm_rosters.services.inc).  Note that the referenced ctools
 * hook obviates creating this endpoint through the UI.
 */
function nnlm_rosters_ctools_plugin_api($owner, $api) {
  if ($owner == 'services' && $api == 'services') {
    return array(
      'version' => 3,
      'file' => 'nnlm_rosters.services.inc',
      'path' => drupal_get_path('module', 'nnlm_rosters'),
    );
  }
}

/**
 * Implements hook_services_resources
 * Defines the resources available via services module (REST, in this case).
 * Note that this is not currently used, but is implemented in the anticipation
 * of leveraging said functionality in the not-so-distant future.
 * @return array The definition array
 */
function nnlm_rosters_services_resources() {
  watchdog('nnlm_rosters', __FUNCTION__, array(), WATCHDOG_NOTICE, 'link');
  return array(
    '#api_version' => 3002,
    'people' => array(
      'operations' => array(
        'retrieve' => array(
          'help' => t('Retrieves a single NN/LM staff member by id'),
          'file' => array(
            'type' => 'inc',
            'module' => 'nnlm_rosters',
            'name' => 'resources/people_resource'
          ),
          'callback' => 'nnlm_rosters_services_retrive_people_single',
          'access callback' => 'nnlm_rosters_services_retrive_people_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
          'args' => array(
            array(
              'name' => 'id',
              'type' => 'int',
              'description' => 'The PeopleID of the staff member',
              'source' => array('path' => '0'),
              'optional' => FALSE,
            ),
          ),
        ),
        'index' => array(
          'help' => t('An index of all current NN/LM staff'),
          'file' => array(
            'type' => 'inc',
            'module' => 'nnlm_rosters',
            'name' => 'resources/people_resource',
            'path' => drupal_get_path('module', 'nnlm_rosters'),
          ),
          'callback' => 'nnlm_rosters_services_retrive_people_index',
          'access callback' => 'nnlm_rosters_services_retrive_people_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
        ),
      ),
    ),
    'test' => array(
      'operations' => array(
        'retrieve' => array(
          'help' => t("A test of the REST api"),
          'file' => array(
            'type' => 'inc',
            'module' => 'nnlm_rosters',
            'name' => 'resources/test_resource',
            'path' => drupal_get_path('module', 'nnlm_rosters'),
          ),
          'access arguments' => array('access content'),
          'callback' => 'nnlm_rosters_services_test_id',
          'args' => array(
            array(
              'name' => 'id',
              'type' => 'int',
              'description' => t("a test of rest arguments"),
              'source' => array('path' => '0'),
              'optional' => FALSE,
            ),
          ),
        ),
        'index' => array(
          'help' => t('A test of the REST api index functionality'),
          'file' => array(
            'type' => 'inc',
            'module' => 'nnlm_rosters',
            'name' => 'resources/test_resource',
            'path' => drupal_get_path('module', 'nnlm_rosters'),
          ),
          'callback' => 'nnlm_rosters_services_test',
        ),
      ),
    ),
  );
}
