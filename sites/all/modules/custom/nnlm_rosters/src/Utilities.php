<?php

/**
 * @file
 */
namespace Drupal\nnlm_rosters;
use Drupal\nnlm_rosters\Exception\System as RostersSystemException;
/**
 * Helper class for functions used across this module.
 */
class Utilities {
  private static $has_firephp = FALSE;
  /**
   * Provides a json formatted output of an array for services.
   *
   * @param $result array The direct array result from a
   *                        data query to the db.
   *
   * @return NULL
   */
  public static function json_output($type, $data) {
    // Jsonapi format results.  See http://jsonapi.org/format/
    if (empty($data)) {
      $data = NULL;
    }
    else {
      foreach ($data as &$d) {
        $d->type = $type;
      }
    }

    $response = array(
      'data' => $data,
    );
    // Format array to jsonapi standards?
    //$callback = check_plain($_REQUEST['callback']);
    header("Content-Type: application/vnd.api+json");
    echo drupal_json_encode($response);
    // If its a jsonp callback request
    // if (isset($callback) && $callback != '') {
    //   $json_response = drupal_json_encode($response);
    //   echo $callback ."(". $json_response .");";
    // }
    // else {
    //   drupal_json_output($response);
    // }
    // drupal_json_output($result);
  }
  /**
   *
   */
  public static function msg($msg, $label, $level = NULL) {
    if (self::$has_firephp) {
      switch ($level) {
        case 'info':
          $level = \FirePHP::INFO;
          break;

        case 'warning':
          $level = \FirePHP::WARN;
          break;

        case 'error':
          $level = \FirePHP::ERROR;
          break;

        case 'notice':
        default:
          $level = \FirePHP::LOG;
          break;
      }
      dfb($msg, $label, \FirePHP::LOG);
      return;
    }
    nnlm_core_dump($msg, $label, $level);
  }
  /**
   *
   */
  public static function dump($msg = '', $label = '') {
    self::msg($msg, $label, 'notice');
  }
  /**
   *
   */
  public static function error($msg = '', $label = '') {
    self::msg($msg, $label, 'error');
  }
  /**
   *
   */
  public static function warn($msg = '', $label = '') {
    self::msg($msg, $label, 'warning');
  }
  /**
   *
   */
  public static function info($msg = '', $label = '') {
    self::msg($msg, $label, 'message');
  }

}
