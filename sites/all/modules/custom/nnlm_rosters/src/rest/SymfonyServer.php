<?php
namespace Drupal\nnlm_rosters\rest;

use \Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use \Symfony\Component\Config\Loader\LoaderInterface;
use \Symfony\Component\DependencyInjection\ContainerBuilder;
use \Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpKernel\Kernel;
use \Symfony\Component\Routing\RouteCollectionBuilder;
use Drupal\nnlm_rosters\Utilities as U;

/**
 * Requires the Drupal symfony module.  Development deferred until server has reached minimum php 5.3.9
 */
class SymfonyServer{
  function __construct(){
    U::dump("Constructing symfony server");
  }
}
