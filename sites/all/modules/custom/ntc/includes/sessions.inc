<?php
/**
 * @file
 * Custom session handling functions for handling ntrp sessions
 * 
 * This file is the drupalized version of legacy/ntc/ntrp/sessions.php
 *
 * Place this code in
 * /sites/all/ntc/includes/sessions.inc
 */

function get_session_expire(){
	return 3600;
}

//Drupalized 8-27-15
//if no valid session exists, a new session is started, returns the ntrp_sessid
function ntrp_session_start(){
	$session_expire = get_session_expire();
	ntrp_sessions_cleanup();
	if(ntrp_session_validate()){
		ntrp_session_update();
	}else{
		db_set_active('general');//switch to the 'general' database
		$sessid = random_ntrp_sessid();
		$data = 'nobody';
		$res = db_insert('class_sessions')
			-> fields(array(
				'session_id'=>$sessid,
				'set_time'=>date("Y-m-d H:i:s",time()),
				'data'=>$data
			))
			->execute();
		if($res){
			setrawcookie("ntrp_sessid",$sessid,time()+$session_expire,"/",$_SERVER['HTTP_HOST']);
			$_COOKIE["ntrp_sessid"] = $sessid;
			db_set_active();//return to the default database
			return $sessid;
		}else{
			echo("sql query failed");
			db_set_active();//return to the default database
			return false;
		}
	}
}

//Drupalized 8-27-15
//changes the ntrp_sessid of the current session to prevent hacks, returns the ntrp_sessid
function ntrp_session_update(){
	$session_expire = get_session_expire();
	db_set_active('general');//switch to the 'general' database
	$new_sessid = random_ntrp_sessid();
	$res = db_update('class_sessions')
		->fields(array(
			'session_id'=>$new_sessid,
			'set_time'=>date("Y-m-d H:i:s",time())
		))
		->condition('session_id',$_COOKIE["ntrp_sessid"],'=')
		->execute();
	if($res){
		setcookie("ntrp_sessid",$new_sessid,time()+$session_expire,"/",$_SERVER['HTTP_HOST']);
		$_COOKIE["ntrp_sessid"] = $new_sessid;
		db_set_active();//return to the default database
		return true;
	}else{
		db_set_active();//return to the default database
		return false;
	}
}

//Drupalized 8-27-15
//puts the username into the database that matches the current user.
function ntrp_session_associate_user($username){
	db_set_active('general');//switch to the 'general' database
	$res = db_update('class_sessions')
		->fields(array(
			'data'=>$username
		))
		->condition('session_id',$_COOKIE["ntrp_sessid"],'=')
		->execute();
	if($res){
		db_set_active();//return to the default database
		return true;
	}else{
		db_set_active();//return to the default database
		return false;
	}
}

//Drupalized 8-27-15
//returns true if the $_COOKIE['ntrp_sessid'] is valid, otherwise returns false.
function ntrp_session_validate(){
	$session_expire = get_session_expire();
	db_set_active('general');//switch to the 'general' database
	$query = db_select('class_sessions','csess')
		->fields('csess',array('session_id'))
		->condition('session_id',$_COOKIE["ntrp_sessid"],'=')
		->condition('set_time',date("Y-m-d H:m:s", time()-$session_expire),'>');
	$result = $query->execute();
	if($row = $result->fetchAssoc()){
		db_set_active();//return to the default database
		return true;
	}else{
		db_set_active();//return to the default database
		return false;
	}
}

//Drupalized 8-27-15
//removes any expired sessions from the database to clean up the database
function ntrp_sessions_cleanup(){
	$session_expire = get_session_expire();
	db_set_active('general');//switch to the 'general' database
	$num_deleted = db_delete('class_sessions')
	  ->condition('set_time', date("Y-m-d H:i:s", time()-$session_expire),'<')
	  ->execute();
	db_set_active();//return to the default database
}

//No Drupalization necessary
//generates a random session_id
function random_ntrp_sessid(){
	$validchars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$validcharmaxindex = strlen($validchars)-1;
	$result = "";

	for($i = 0; $i<128; $i++){
		$char = mt_rand(0,$validcharmaxindex);
		$result .= $validchars[$char];
	}
	return $result;
}

//Drupalized 8-27-15
//returns true if the current ntrp_sessid is logged in
function ntrp_is_logged_in(){
	$session_expire = get_session_expire();
	db_set_active('general');//switch to the 'general' database
	$query = db_select('class_sessions','csess')
		->fields('csess',array('data'))
		->condition('session_id',$_COOKIE["ntrp_sessid"],'=')
		->condition('set_time',date("Y-m-d H:m:s", time()-$session_expire),'>');
	$result = $query->execute();
	if($row = $result->fetchAssoc()){
		if($row["data"]!='nobody'){
			db_set_active();//return to the default database
			return true;
		}else{
			db_set_active();//return to the default database
			return false;
		}
	}else{
		db_set_active();//return to the default database
		return false;
	}
}

//Drupalized 8-27-15
//returns the username from the current ntrp_sessid
function ntrp_get_logged_in_username(){		
	$session_expire = get_session_expire();
	db_set_active('general');//switch to the 'general' database
	$query = db_select('class_sessions','csess')
		->fields('csess',array('data'))
		->condition('session_id',$_COOKIE["ntrp_sessid"],'=')
		->condition('set_time',date("Y-m-d H:m:s", time()-$session_expire),'>');
	$result = $query->execute();
	if($row = $result->fetchAssoc()){
		if($row["data"]!='nobody'){
			db_set_active();//return to the default database
			return $row["data"];
		}else{
			db_set_active();//return to the default database
			return false;
		}
	}else{
		db_set_active();//return to the default database
		return false;
	}
}

//Drupalized 9-10-15
//logs out the current user
function ntrp_session_end(){
	db_set_active('general');
	$res = db_update('class_sessions')
		->fields(array(
			'data'=>'nobody'
		))
		->condition('session_id',$_COOKIE["ntrp_sessid"],'=')
		->execute();
	if($res){
		db_set_active();//return to the default database
		return true;
	}else{
		db_set_active();//return to the default database
		return false;
	}
}