<?php

/**
 * @file Workbench
 * Provides extensions to the workbench and workbench access module particular
 * to the nnlm.gov environment.
 */

namespace Drupal\nnlm_core;

use Drupal\nnlm_core\Utilities as U;
/**
 *
 */
class Workbench {
  /**
   * Performs a check to make sure all the requirements needed for this
   * class to operate are present and accounted for.
   */
  private static function _requirements_check() {
    $result =& drupal_static('nnlm_core_workbench' . __FUNCTION__);
    if (!isset($result)) {
      $result = (module_exists('workbench_access') && module_exists('power_menu'));
    }
    if ($result === FALSE) {
      watchdog('nnlm_core', t("Critical error: requirements check failed for nnlm_core Workbench class"), array(), \WATCHDOG_CRITICAL);
      return FALSE;
    }
    return TRUE;
  }
  /**
   * Checks whether the current user has access to any of the passed workbench sections.
   *
   * @param array $sections
   *   an array of editorial sections (by section name) that we wish to confirm
   *   the current user has access to.
   *
   * @return boolean       TRUE if the user has access to the listed sections,
   *                       FALSE if they do not, or if no determination can be
   *                       made.
   */
  static function check_access($sections = array()) {
    global $user;
    if (!self::_requirements_check()) {
      return FALSE;
    }
    if (user_is_anonymous()) {
      return FALSE;
    }
    $current_user_sections = &drupal_static(__FUNCTION__);
    if (!$current_user_sections) {
      $current_user_sections = array_values(self::user_sections());
    }
    return (count(array_intersect($sections, $current_user_sections)) > 0);
  }
  /**
   * Custom access controls not easily implemented in the UI.
   *
   * @param object $node
   *   The node to be checked
   * @param string $op
   *   one of 'view', 'update', 'create', 'delete'
   * @param object $account
   *   The user object to perform the check on
   *
   * @return constant string
   *         NODE_ACCESS_ALLOW: if the operation is to be allowed.
   *         NODE_ACCESS_DENY: if the operation is to be denied.
   *         NODE_ACCESS_IGNORE: to not affect this operation at all.
   */
  public static function node_access($node, $op, $account) {
    nnlm_core_dump(__FUNCTION__);
    if (!function_exists('workbench_access_node_access') || (!defined('NODE_ACCESS_DENY'))) {
      nnlm_core_dump("Workbench access is either not installed, or needs to be elevated in the module load order.", 'Error');
      // This just means auth won't come from us, not that it is denied entirely.
      return NODE_ACCESS_IGNORE;
    }
    if (!isset($node->field_section) || empty($node->field_section)) {
      // nnlm_core_dump("Node ".$node->nid." does not have field_section", 'Error');
      return NODE_ACCESS_IGNORE;
    }
    $administrative_sections = array('drupal_help');
    $section_tid = $node->field_section['und'][0]['tid'];

    if (empty($section_tid)) {
      nnlm_core_dump("Section TID is empty");
      return NODE_ACCESS_IGNORE;
    }
    $section = taxonomy_term_load($section_tid);
    if (empty($section)) {
      nnlm_core_dump("Editorial section could not be determined", 'Error');
      return NODE_ACCESS_IGNORE;
    }
    if (in_array($section->name, $administrative_sections)) {
      if (!user_is_logged_in()) {
        return NODE_ACCESS_DENY;
      }
    }
    return NODE_ACCESS_IGNORE;
  }
  /**
   * Returns the list of workbench access sections the current user
   * is allowed to access.
   *
   * @return array array An array of section names from the 'sections' machine vocabulary
   * TODO: consider workbench_access_get_active_tree()
   */
  public static function user_sections() {
    global $user;
    if (!self::_requirements_check()) {
      return FALSE;
    }
    $result = &drupal_static(__FUNCTION__);
    if (!$result) {
      $result = array_map(
      function ($arr) {
          return $arr['name'];
      }, \workbench_access_get_user_tree()
      );
    }
    return $result;
  }
  /**
   * Returns a full list of editorial sections for this environment (e.g.
   * the contents of the 'sections' taxonomy).
   *
   * @param string $variant
   *   One of three possible values:
   *                         'full': result is a hash with section name as key
   *                         and the full section object as value
   *                         'assoc': result is a hash with section name as key
   *                         and section name as value.
   *                         'tid': result is a hash with section name as key
   *                         and taxonomy tid of the section term as value.
   *                         any other: result is an array populated with
   *                         section names and numerical indices.
   *
   * @return array          See above
   */
  public static function get_editorial_sections($variant = NULL) {
    static $sections = NULL;
    if (is_null($sections)) {
      $sections = taxonomy_vocabulary_machine_name_load('sections');
      $sections = taxonomy_get_tree($sections->vid, 0, NULL, TRUE);
    }
    $result = array();
    foreach ($sections as $section) {
      switch ($variant) {
        case 'full':
          $result[$section->name] = $section;
          break;

        case 'assoc':
          $result[$section->name] = $section->name;
          break;

        case 'tid':
          $result[$section->name] = $section->tid;
          break;

        default:
          $result[] = $section->name;
          break;
      }
    }
    return $result;
  }

  /**
   * @private
   * Limits the displayed menus based on allowed workbench access menus
   *
   * @param  array $form
   *   The form being considered
   *
   * @return NULL
   */
  public static function prune_menus($form) {
    if (!variable_get('nnlm_core_workbench_access_limit_menu', TRUE)) {
      return;
    }
    if (user_access("administer menus and menu items")) {
      // Admin users get the stock view.
      return;
    }
    $target_menus = Menu::get_allowed_menus();

    foreach ($form['menu']['link']['parent']['#options'] as $k => $v) {
      // Remove each menu entry if it's not in the list of allowed menus.
      foreach ($target_menus as $allowed_menu) {
        if (strpos($k, $allowed_menu) !== FALSE) {
          continue 2;
        }
      }
      // Menu was not found in allowed list.  unset from dropdown options.
      // nnlm_core_dump("pruning menu item " . $k);
      unset($form['menu']['link']['parent']['#options'][$k]);
    }
  }

}
