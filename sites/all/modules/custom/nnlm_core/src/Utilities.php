<?php

/**
 * @file
 */
namespace Drupal\nnlm_core;

/**
 * Utilities
 * Various non-hook functions invoked by the nnlm_core module.
 */
class Utilities {

  /**
   * Displays a message to the developer using the message display flavor of the minute.
   *
   * @param string $msg
   *   the message to display
   * @param string $label
   *   an optional label to prefix the message
   * @param string $level
   *   one of 'notice', 'warning', 'error'
   */
  public static function dump($msg, $label, $level='notice') {
    global $user;
    if (isset($user->name) && $user->name !== 'drupal_admin') {
      return;
    }
    if (empty($msg)) {
      return;
    }
    /*@$user->name === 'content_creator' || @$user->name === 'content_moderator' || */
    $watchdog_level = WATCHDOG_NOTICE;
    $dpm_type = 'status';
    $severity = 0;
    switch ($level) {
      case 'warning':
      case 'error':
        $watchdog_level = WATCHDOG_ERROR;
        $dpm_type = $level;
        $severity = 1;
        break;

      case NULL:
      default:
        break;
    }
    dpm($msg);

    if ($severity > 0) {
      watchdog('nnlm_core', "%label: %msg", array(
        '%label' => $label,
        '%msg' => $msg,
      ), $watchdog_level);
    }
  }
  /**
   * Fast in_array for sorted arrays.
   *
   * @param string $needle
   *   The needle to search for
   * @param array $haystack
   *   The haystack to be searched
   *
   * @return bool           TRUE if needle is found, FALSE otherwise
   */
  public static function nnlm_core_binary_in_array($needle, $haystack) {
    $high = count($haystack);
    $low = 0;

    while ($high - $low > 1) {
      $probe = ($high + $low) / 2;
      if ($haystack[$probe] < $needle) {
        $low = $probe;
      }
      else {
        $high = $probe;
      }
    }

    if ($high == count($haystack) || $haystack[$high] != $needle) {
      return FALSE;
    }
    else {
      return $high;
    }
  }
  /**
   * Public static function compile_sass($sass) {
   * $sass_path = variable_get('compass_path', '/usr/bin/compass');
   * $errors = _compass_check_path($compass_path);
   * if ($errors) {
   * watchdog('compass', '!errors', array('!errors' => implode('<br />', $errors)), WATCHDOG_ERROR);
   * return FALSE;
   * }.
   *
   * Specify Drupal's root as the working a working directory so that relative
   * paths are interpreted correctly.
   * $drupal_path = $_SERVER['DOCUMENT_ROOT'] . dirname($_SERVER['SCRIPT_NAME'] ? $_SERVER['SCRIPT_NAME'] : $_SERVER['SCRIPT_FILENAME']);
   *
   * TODO determine whether this module has any hope of ever working on Windows
   * if (strstr($_SERVER['SERVER_SOFTWARE'], 'Win32') || strstr($_SERVER['SERVER_SOFTWARE'], 'IIS')) {
   * Use Window's start command to avoid the "black window" from showing up:
   * http://us3.php.net/manual/en/function.exec.php#56599
   * Use /D to run the command from PHP's current working directory so the
   * file paths don't have to be absolute.
   * $compass_path = 'start "window title" /D' . escapeshellarg($drupal_path) . ' /B ' . escapeshellarg($compass_path);
   * }
   *
   * $descriptors = array(
   * stdin
   * 0 => array('pipe', 'r'),
   * stdout
   * 1 => array('pipe', 'w'),
   * stderr
   * 2 => array('pipe', 'w'),
   * );
   *
   * $env = array();
   * $gem_home = variable_get('compass_gem_home', NULL);
   * if (!empty($gem_home)) {
   * $env['GEM_HOME'] = variable_get('compass_gem_home', NULL);
   * }
   *
   * $gem_path = variable_get('compass_gem_path', NULL);
   * if (!empty($gem_path)) {
   * $env['GEM_PATH'] = variable_get('compass_gem_path', NULL) . ':' . shell_exec('gem env GEM_PATH');
   * }
   *
   * $env['HOME'] = '/';
   *
   * if ($h = proc_open($compass_path . ' ' . $command, $descriptors, $pipes, $drupal_path, $env)) {
   * stream_set_blocking($pipes[1], 0);
   * stream_set_blocking($pipes[2], 0);
   *
   * while (!feof($pipes[1]) || !feof($pipes[2])) {
   * if (!feof($pipes[1])) {
   * $output .= stream_get_contents($pipes[1]);
   * }
   * if (!feof($pipes[2])) {
   * $errors .= stream_get_contents($pipes[2]);
   * }
   * }
   *
   * fclose($pipes[0]);
   * fclose($pipes[1]);
   * fclose($pipes[2]);
   * $return_code = proc_close($h);
   *
   * Display debugging information to authorized users.
   * if (variable_get('compass_debugging', FALSE) && user_access('administer site configuration')) {
   * drupal_set_message(t('Compass command: @command', array('@command' => $compass_path . ' ' . $command)));
   * drupal_set_message(t('Compass output: !output', array('!output' => '<pre>' . $output . '</pre>')));
   * }
   *
   * if ($return_code != 0) {
   * If Compass returned a non-zero code, trigger a PHP error that will
   * be caught by Drupal's error handler, logged to the watchdog and
   * eventually displayed to the user if configured to do so.
   *
   * If $errors is empty, only report the error code.
   * if (empty($errors)) {
   * trigger_error(t('Compass reported error code !code.', array('!code' => $return_code)), E_USER_ERROR);
   * }
   * Otherwise report the error code, and the error message.
   * else {
   * trigger_error(t("Compass reported error code !code.\nMessage:\n!error", array('!code' => $return_code, '!error' => $errors)), E_USER_ERROR);
   * }
   * }
   * return $return_code;
   * }
   */

  /**
   * @deprecated.  Now in Workbench class.
   * See that class for descriptions.
   */
  public static function get_editorial_sections($variant = NULL) {
    return Workbench::get_editorial_sections($variant);
  }

  /**
   * Standardizes a link input to be useful as a link.
   * //TODO: revisit later to see how generally necessary this actually is.
   *
   * @param array $link
   *   A loaded menu link
   * @param array $optionsan
   *   array of one or more of the following:
   *   an array of one or more of the following:
   *                        use_node_title: uses the node title instead of the menu title for the link text
   *
   * @return array          The standardized link.
   */
  public static function standardize_link($link, $options = array()) {
    if (empty($link)) {
      return;
    }
    $options += array(
      // Defaults.
      'use_node_title' => FALSE,
    );
    // nnlm_core_dump($link, 'Standardizing link ');
    $loc = @$link['link_path'] ? : @$link['path'] ? : @$link['href'];
    $title = ($options['use_node_title'] && @$link['title']) ? @$link['title'] : @$link['link_title'];
    $link_required = array(
      'link_title' => $title,
      'title' => $title,
      'path' => $loc,
      'href' => $loc,
      'link_path' => $loc,
    );
    $link_defaults = array(
      'menu_name' => 'menu-national-main-menu',
      'weight' => 0,
      'options' => array(),
      'localized_options' => array(),
      'module' => 'menu',
    );
    if (empty($loc) || empty($title)) {
      // nnlm_core_dump($link, "Link has no path");
      // throw new Exception("Link has no path");
    }
    // Does not override if present.
    return ($link_required + $link + $link_defaults);
  }
  /**
   * Performs certain types of cache clears.
   * @param  string $type The level of clearing to be performed
   * @return NULL
   */
  public static function flush_cache($type="all"){
    switch($type){
      case 'page':
      cache_clear_all(NULL, 'cache_page');
      break;
      case 'all':
      default:
      drupal_flush_all_caches();
      break;
    }
    drupal_set_message(t("$type cache was cleared"), 'status', FALSE);
    drupal_goto("admin/config");
  }

}
