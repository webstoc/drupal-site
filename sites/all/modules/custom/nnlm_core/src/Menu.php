<?php

/**
 * @file
 */
namespace Drupal\nnlm_core;

use Drupal\nnlm_core\Utilities as U;
/**
 * @file Menu
 * Houses all the enhancements to the various modules used in the NN/LM Drupal
 * instances that have to do with menuing and breadcrumbs.
 */
class Menu {
  /**
   * Performs a check to make sure all the requirements needed for this
   * class to operate are present and accounted for.
   */
  private static function _requirements_check() {
    $result =& drupal_static('nnlm_core_workbench' . __FUNCTION__);
    if (!isset($result)) {
      $result = (module_exists('power_menu'));
    }
    if ($result === FALSE) {
      watchdog('nnlm_core', t("Critical error: requirements check failed for nnlm_core Menu class"), array(), \WATCHDOG_CRITICAL);
      return FALSE;
    }
    return TRUE;
  }
  /**
   *
   */
  public static function can_access_menu() {
    // dpm($_GET['q'], 'q');
    return TRUE;
  }
  /**
   * @param  array $entity
   *   The entity to retreive the menu for
   * @return  string The machine name of the menu for this entity,
   *                     or the default 'menu-national-main-menu' if the
   *                     machine name cannot be determined.
   */
  public static function get_menu($entity) {
    // nnlm_core_dump(__FUNCTION__);
    // Default value.
    $default_menu = 'menu-national-main-menu';
    if (!self::_requirements_check()) {
      return $default_menu;
    }
    $current_menu =& drupal_static('nnlm_core_menu' . __FUNCTION__);
    if (!empty($current_menu)) {
      return $current_menu;
    }
    // 1) See if menu is already established.
    if (is_array($entity) && isset($entity['menu_name'])) {
      $current_menu = $entity['menu_name'];
    }
    // 2) check if active menu is set by context. Override if so.
    $active_contexts = context_active_contexts();
    foreach ($active_contexts as $context_name => $context) {
      if (!preg_match("/theme/", $context_name)) {
        continue;
      }
      if (isset($context->reactions) && isset($context->reactions['menus']) && isset($context->reactions['menus']['main_menu'])) {
        $current_menu = $context->reactions['menus']['main_menu'];
        break;
      }
    }
    if (!empty($current_menu)) {
      return $current_menu;
    }
    // 3) No menu so far - check if active menu is set by power_menu
    $menu_trail = menu_get_active_trail();
    if (isset($menu_trail[1]['menu_name']) && ($menu_trail[1]['module'] != 'system')) {
      $current_menu = $menu_trail[1]['menu_name'];
      return $current_menu;
    }

    // 4) No menu - try to decide on menu based on taxonomy section assigned to entity.
    if (isset($entity->field_section)) {
      try {
        $sections = field_get_items('node', $entity, 'field_section');
        $section = field_view_value('node', $entity, 'field_section', $sections[0]);
        if (isset($section['#title'])) {
          $test_menu = 'menu-' . $section['#title'] . '-main-menu';
          // Make sure this actually exists.
          if (menu_load($test_menu) !== FALSE) {
            $current_menu = $test_menu;
            return $current_menu;
          }
        }
      }
      catch (\Exception$e) {
        nnlm_core_dump($e->getMessage(), __FUNCTION__ . ": Exception", 'error');
      }
    }
    return $default_menu;
  }
  /**
   * Utility function to build links from passed aliases (and menus).
   *
   * @param (string) $path
   *   - the url alias to the drupal node (or other)
   * @param (string) $regional_menu
   *   The name of the menu to build from
   *
   * @return arary A menu link for the passed alias, or a default
   *                 if it cannot be found.
   */

  private static function build_link ($options, $path, $regional_menu, $last_item = FALSE) {
    if (empty($path)) {
      return FALSE;
    }
    if ($path === '<front>') {
      return U::standardize_link(array(
          'menu_name' => 'menu-national-main-menu',
          'link_path' => '',
          'link_title' => 'NN/LM Home',
          'use_node_title' => FALSE,
          'hidden' => 0,
          'has_children' => 1,
          'options' => array(),
          'module' => 'menu',
        ));
    }
    $path = ltrim($path, '/');
    $internal_path = drupal_get_normal_path($path);
    // nnlm_core_dump($internal_path, "Internal path");
    if ($path === $internal_path) {
      // nnlm_core_dump("Cannot resolve path $path to a node", 'Error', 'error');
      return FALSE;
    }
    $path = $internal_path;
    // nnlm_core_dump($path, "building link for menu $regional_menu, path");
    $link = menu_link_get_preferred($path, $regional_menu);
    // nnlm_core_dump($link, "Loaded link for path $path");
    if (empty($link)) {
      // nnlm_core_dump("Link $path is empty, skipping", 'Error', 'error');
      return FALSE;
    }
    if (isset($link['hidden']) && intval($link['hidden']) === 1) {
      // nnlm_core_dump("Link $path is hidden, skipping", 'Notice');
      return FALSE;
    }
    // don't add links to the front, or nolink paths.
    if ($link['link_path'] === '<nolink>') {
      // nnlm_core_dump("Link $path is nolink, skipping", 'Notice');
      return FALSE;
    }
    return U::standardize_link($link);
  }
  /**
   * Internal function used by nnlm_core_menu_breadcrumb_alter.  Broken out
   * to allow other nnlm modules access.  May end up not being necessary.
   *
   * @param array $active_trail
   *   The current active breadcrumb trail
   *
   * @return array               The new active trail, or the original trail if
   *                                 a better trail could not be determined.
   */
  public static function better_menu_trail(&$active_trail, $item = NULL, $options = array()) {
    $current_alias = url(current_path());
    $replacement_trail = &drupal_static(__FUNCTION__);
    if (isset($replacement_trail) && isset($replacement_trail[$current_alias])) {
      // nnlm_core_dump("Static hit!", __FUNCTION__);
      // nnlm_core_dump($replacement_trail, "Replacement trail");
      return $replacement_trail[$current_alias];
    }
    if (!isset($replacement_trail)) {
      $replacement_trail = array();
    }
    if (!isset($replacement_trail[$current_alias])) {
      $replacement_trail[$current_alias] = array();
    }
    $current_menu_item = NULL;
    $regional_menu = 'menu-national-main-menu';
    $regional_home = 'national';
    $default_options = array(
      // Whether or not the last item should be hyperlinked.
      'link_last_item' => FALSE,
      // Whether or not the last item should be visible.
      'show_last_item' => FALSE,
      // Whether or not to include the national home at the front of the path.
      'show_national_home' => FALSE,
      // Whether to use the node title or menu title as the link text.
      'use_node_title' => FALSE,
    );
    try {
      /**
       * Contains options as to how the breadcrumbs should be rendered.
       * @var [type]
       */
      $options = $options + $default_options;
      if (!is_array($active_trail)) {
        throw new \Exception("Invalid argument for active trail passed to " . __FUNCTION__);
      }
      $current_menu_item = end($active_trail);
      if (empty($current_menu_item)) {
        throw new \Exception("Could not determine current menu item");
      }
      $regional_menu = self::get_menu($current_menu_item);
      // nnlm_core_dump($regional_menu, 'Regional menu');
      if (empty($regional_menu)) {
        $regional_menu = 'menu-national-main-menu';
      }
      if ($regional_menu === 'menu-national-main-menu') {
        $options['show_national_home'] = TRUE;
      }
      // $current_menu_item = self::standardize_link(end($active_trail));
      // $regional_home = preg_replace("/menu-([a-z]+)-main-menu/", "$1", $regional_menu);
      // nnlm_core_dump($regional_menu, "Regional menu");
      // nnlm_core_dump($regional_home, "Regional home");
      // spotfix for changed taxonomy term (Hack, I know.  Sorry, whomever inherits this.)
      // build a new path using the page alias instead of the menu heirarchy
      // nnlm_core_dump($current_alias, "Current path");
      $current_path_segments = explode('/', $current_alias);

      // Note that we are pushing in reverse order for speed.  Make sure to
      // reverse the array when done.
      while (!empty($current_path_segments)) {
        $ancestor_path = implode('/', $current_path_segments);
        // nnlm_core_dump($ancestor_path, "Ancestor path");
        array_pop($current_path_segments);
        if ($ancestor_path === $current_alias && !$options['show_last_item']) {
          // nnlm_core_dump("Skipping $ancestor_path, same");
          continue;
        }
        $link = self::build_link($options, $ancestor_path, $regional_menu);
        // nnlm_core_dump($link, "Built link");
        if (!empty($link)) {
          array_push($replacement_trail[$current_alias], $link);
        }
      }
      if ($options['show_national_home']) {
        // nnlm_core_dump("here");
        $link = self::build_link($options, '<front>', 'menu-national-main-menu');

        array_push($replacement_trail[$current_alias], $link);
      }
      $replacement_trail[$current_alias] = array_reverse($replacement_trail[$current_alias]);
      cache_set(__FUNCTION__, $replacement_trail, 'cache', \CACHE_TEMPORARY);
      // nnlm_core_dump($replacement_trail[$current_alias], "Replacement trail");
      $active_trail = $replacement_trail[$current_alias];
      return $replacement_trail[$current_alias];

    }
    catch (\Exception$e) {
      nnlm_core_dump($e->getMessage(), "Error in breadcrumb construction", 'error');
      nnlm_core_dump(t("Content does not appear to be assigned to a menu. Breadcrumb could not be established."), "Warning", 'warning');
      return $active_trail;
    }
    return $replacement_trail;
  }
  /**
   * Retrieves the allowed menus for the currently logged in user, as per
   * workbench access role and assignment.
   *
   * @return Array an array consisting of menu names the user is allowed
   * to access.  NOTE: this is highly dependent on menu naming standards
   * adopted in this implementation, and is not exportable.
   */
  public static function get_allowed_menus() {
    global $user;
    $all_menus = menu_load_all();
    $allowed_menus = array();
    if (!self::_requirements_check()) {
      return $allowed_menus;
    }
    $user_sections = array_values(Workbench::user_sections());
    if (in_array('drupal_help', $user_sections)) {
      $user_sections[] = 'help';
    }
    foreach ($all_menus as $machine_name => $human_readable_name) {
      @list(, $menu_section) = explode('-', $machine_name);
      if ((($user->uid == 1 || user_has_role('drupal administrator'))) || (!empty($menu_section) && in_array($menu_section, $user_sections))) {
        $allowed_menus[] = $machine_name;
      }
    }
    return $allowed_menus;
  }
  /**
   * Basically a straight duplication of menu_overview_page to add Workbench
   * Access additions.
   */
  public static function get_menu_overview_page() {
    global $user;
    $result = db_query("SELECT * FROM {menu_custom} ORDER BY title", array(), array('fetch' => \PDO::FETCH_ASSOC));
    $header = array(t('Title'), array('data' => t('Operations'), 'colspan' => '3'));
    $rows = array();
    $allowed_menus = self::get_allowed_menus();
    //dpm($allowed_menus, "Allowed menus");
    foreach ($result as $machine_name=>$menu) {
      if(!in_array($menu['menu_name'], $allowed_menus)){
        //dpm("Menu ".$menu['menu_name'] . " disallowed");
        continue;
      }
      //dpm($menu, "menu");
      $row = array(theme('menu_admin_overview', array('title' => $menu['title'], 'name' => $menu['menu_name'], 'description' => $menu['description'])));
      $row[] = array('data' => l(t('list links'), 'admin/structure/menu/manage/' . $menu['menu_name']));
      $row[] = array('data' => l(t('edit menu'), 'admin/structure/menu/manage/' . $menu['menu_name'] . '/edit'));
      $row[] = array('data' => l(t('add link'), 'admin/structure/menu/manage/' . $menu['menu_name'] . '/add'));
      $rows[] = $row;
    }

    return theme('table', array('header' => $header, 'rows' => $rows));
  }
  /**
   * Provides a list of all 'custom' menus.
   *
   * @return array a list of all 'custom' menus.
   * //TODO: expand this documentation.
   */
  public static function get_custom_menus() {
    return db_query("SELECT * FROM {menu_custom} ORDER BY title", array(), array('fetch' => \PDO::FETCH_ASSOC));
  }

}
