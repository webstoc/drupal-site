<?php

/**
 * @file
 */

namespace Drupal\nnlm_core;

use Drupal\nnlm_core\U as U;
/**
 * A centralized location for all javascript and js library additions.
 */
class Libraries {
  private static $singleton = NULL;
  private $scripts = array();
  /**
   * $this->scripts is a hash array of closure functions.  Loading a particular
   * named script consists of looking it up in the scripts hash, and invoking
   * the associated closure function (which does all the heavy lifting).
   * Functions are specified here in the constructor.
   */
  private function __construct() {
    $external_scripts = array(
      'google_jsapi' => '//www.google.com/jsapi',
      'd3'=> '//cdnjs.cloudflare.com/ajax/libs/d3/3.3.10/d3.min.js'
    );
    $this->scripts = array(
      'google_jsapi' => function() use ($external_scripts){
        dfb("Binding Google JSAPI");
        drupal_add_js(
            $external_scripts['google_jsapi'],
            array(
              'type' => 'external',
              'group' => JS_LIBRARY,
              'preprocess' => FALSE,
              'scope' => 'footer',
              'defer' => TRUE,
              'cache' => TRUE,
            ));
      },
      'd3'=> function() use ($external_scripts){
          dfb("Binding d3");
          //dpm("Binding d3");
          drupal_add_js(drupal_get_path('module', 'nnlm_core') . '/bower_components/d3/d3.min.js', array(
            'type' => 'file',
            'group' => JS_LIBRARY,
            'scope' => 'footer',
            'defer' => 'true',
            'cache' => TRUE,
          ));
      },
      'codemirror' => function()  use ($external_scripts){
        dfb("Binding Codemirror");
        drupal_add_js(
            libraries_get_path('codemirror') . '/lib/codemirror.js',
            array(
              'type' => 'file',
              'group' => JS_LIBRARY,
              'scope' => 'footer',
              'defer' => 'true',
              'cache' => TRUE,
            ));
        drupal_add_js(
            libraries_get_path('codemirror') . '/mode/javascript/javascript.js',
            array(
              'type' => 'file',
              'group' => JS_LIBRARY,
              'scope' => 'footer',
              'defer' => 'true',
              'cache' => TRUE,
            ));
        drupal_add_js(
            libraries_get_path('codemirror') . '/mode/sass/sass.js',
            array(
              'type' => 'file',
              'group' => JS_LIBRARY,
              'scope' => 'footer',
              'defer' => 'true',
              'cache' => TRUE,
            ));
        drupal_add_css(
            libraries_get_path('codemirror') . '/lib/codemirror.css',
            array(
              'type' => 'file',
              'basename' => 'codemirror.css',
              'group' => CSS_SYSTEM,
              'scope' => 'footer',
              'defer' => 'true',
            ));
        drupal_add_css(
            libraries_get_path('codemirror') . '/theme/midnight.css',
            array(
              'type' => 'file',
              'basename' => 'midnight.css',
              'group' => CSS_THEME,
              'scope' => 'footer',
              'defer' => 'true',
            ));
        Libraries::bind_custom_js('nnlm_codemirror', "Drupal.behaviors.NNLM.core.bind_code_mirror_editor(context);");
      },
    );
  }

  /**
   * Returns the singleton library object.  Creates it if necessary.
   *
   * @return Libraries The singleton instance of the Libraries class
   */
  private static function getLibrary() {
    if (is_null(self::$singleton)) {
      self::$singleton = new Libraries();
    }
    return self::$singleton;
  }
  /**
   * Bootstraps a javascript script into place.
   *
   * @param string $name
   *   The internal name of the script.
   *
   * @return NULL
   */
  public static function load($name) {
    $library = self::getLibrary();
    if (!isset($library->scripts[$name])) {
      watchdog('nnlm_core', t("Invalid script " . $name . " requested"), array(), \WATCHDOG_ERROR);
      return;
    }
    $library->scripts[$name]();
  }
  /**
   *
   */
  public static function bind_custom_js($namespace, $code) {
    $final_script = "
        (function($) {
          Drupal.behaviors.NNLM = Drupal.behaviors.NNLM || {};
          Drupal.behaviors.NNLM.$namespace = {
            attach: function(context, settings) {
              console.log('binding user script');
              $code;
            }
          }
        })(jQuery);";
    drupal_add_js($final_script,
        array(
          'type' => 'inline',
          'group' => JS_THEME,
          'scope' => 'footer',
          'defer' => 'true',
          'every_page' => FALSE,
          'cache' => TRUE,
        )
        );
  }
  /**
   *
   */
  public static function bind_custom_css($code) {
    drupal_add_js($code,
        array(
          'type' => 'inline',
          'group' => CSS_THEME,
          'scope' => 'footer',
          'defer' => 'true',
          'every_page' => FALSE,
          'cache' => TRUE,
        )
        );
  }
  /**
   * Looks for user-supplied javascript in the 'field_node_scripts' field,
   * and binds it to the interface.
   *
   * @return NULL
   */
  public static function bind_user_js($entity) {
    if (!isset($entity->field_node_scripts)) {
      return;
    }
    $entity = entity_metadata_wrapper('node', $entity);
    $entity_id = $entity->getIdentifier();
    if (is_null($entity_id)) {
      nnlm_core_dump("Entity id is null, cannot create css", "Error", 'error');
      return;
    }
    $scripts = $entity->field_node_scripts->value();
    if (is_array($scripts)) {
      $scripts = $scripts['value'];
    }
    //dpm("Binding user javascript", $scripts);
    if (!empty($scripts)) {
      // dfb($scripts, "Scripts");
      self::bind_custom_js('user_custom_scripts', $scripts);
    }

  }
  /**
   *
   */
  public static function bind_user_css($entity) {
    if (!isset($entity->field_node_styles)) {
      return;
    }
    $entity = entity_metadata_wrapper('node', $entity);
    $entity_id = $entity->getIdentifier();
    if (is_null($entity_id)) {
      nnlm_core_dump("Entity id is null, cannot create css", "Error", 'error');
      return;
    }
    $scss = $entity->field_node_styles->value();
    if (is_array($scss)) {
      $scss = $scss['value'];
    }
    //dpm("Binding user scss", $scss);
    if (empty($scss)) {
      // No custom sass has been applied.
      // nnlm_core_dump("No scss applied.");
      return;
    }
    $cache_scss = &drupal_static(__FUNCTION__ . 'node_' . $entity_id . '_scss_cache');
    $cache_css = &drupal_static(__FUNCTION__ . 'node_' . $entity_id . '_css_cache');
    static $added = FALSE;
    // Boostraps included composer package.
    require drupal_get_path('module', 'nnlm_core')."/vendor/autoload.php";
    if (!class_exists('scssc')) {
      nnlm_core_dump('Could not add custom CSS: SASS libraries not found.', 'ERROR', 'error');
      return;
    }
    // don't reprocess if sass is the same.
    if ($scss === $cache_scss) {
      // nnlm_core_dump('SCSS cache hit!');
    }
    else {
      // nnlm_core_dump($scss, "Raw SCSS");
      $cache_scss = $scss;
      try {
        $scss = 'body{#zone-content{.region-content{.node{' . $scss . '}}}}';
        $scssc = new \scssc();
        // Adding compass functionality: https://github.com/leafo/scssphp-compass
        new \scss_compass($scssc);
        $cache_css = $scssc->compile($scss);
        // nnlm_core_dump($cache_css, "Compiled CSS ");
      }
      catch (\Exception$e) {
        drupal_set_message(t("The styles you have entered in the node styles field could not be compiled into valid CSS.  Please check your syntax and try again"), 'warning', FALSE);
      }
    }
    if (!empty($cache_css) && !$added) {
      $added = TRUE;
      // nnlm_core_dump("CSS Added");
      drupal_add_css($cache_css,
      array('type' => 'inline', 'media' => 'all', 'scope' => 'footer', 'group' => CSS_THEME, 'every_page' => FALSE)
      );
    }
  }

}
