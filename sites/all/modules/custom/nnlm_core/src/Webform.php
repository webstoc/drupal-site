<?php

/**
 * @file
 */
namespace Drupal\nnlm_core;

use Drupal\nnlm_core\U as U;

/**
 * Provides a localized place to store webform related enhancements and updates.
 */
class Webform {
  /**
   * Performs a check to make sure all the requirements needed for this
   * class to operate are present and accounted for.
   */
  private static function _requirements_check() {
    $result =& drupal_static('nnlm_core_workbench' . __FUNCTION__);
    if (!isset($result)) {
      $result = (module_exists('webform') && module_exists('captcha'));
    }
    if ($result === FALSE) {
      watchdog('nnlm_core', t("Critical error: requirements check failed for nnlm_core Webform class"), array(), \WATCHDOG_CRITICAL);
      return FALSE;
    }
    return TRUE;
  }
  /**
   * Adds a captcha to all webforms when invoked.  Note that actual display of captcha
   * may be limited to anonymous users depending on captcha module settings.
   */
  public static function add_captcha($entity, $entity_type) {
    $captcha_type = 'default';
    if (!self::_requirements_check()) {
      return;
    }
    if (!variable_get('nnlm_core_webform_add_captcha', FALSE)) {
      return;
    }
    if (!empty($entity->webform)) {
      list($webform_id) = entity_extract_ids($entity_type, $entity);
      module_load_include('inc', 'captcha');
      $form_id = 'webform_client_form_' . $webform_id;
      captcha_set_form_id_setting($form_id, $captcha_type);
    }
  }
  public static function remove_captcha($entity, $entity_type){

  }
}