<?php
use Drupal\nnlm_commerce\Utilities as U;
use Drupal\nnlm_core\Utilities as Core_Utilities;
use Drupal\nnlm_commerce\Forms as F;
use Drupal\nnlm_commerce\Controller as C;

define("NNLM_COMMERCE_GENERIC_ERROR", t("An error has occurred, independent of your actions.  Please contact your RML and let them know, so we can fix this problem."));

/**
 * Implements hook_admin
 * Provides the admin interface for configuring this module via
 * the Drupal forms api
 */
function nnlm_commerce_admin() {
  $form = array();
  $form['nnlm_commerce_bookmark_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable commerce bookmark overrides'),
    '#default_value' => variable_get('nnlm_commerce_bookmark_enabled', TRUE),
    '#description' => t('Overrides the default Drupal bookmarks with a custom version that uses the region root and the checkout page.'),
    '#required' => FALSE,
  );
  return system_settings_form($form);
}

/**
 * Implementation of hook_views_api().
 * Tasks:
 *   //TODO: set up order filtering based on editorial section in views.inc file
 */
function nnlm_commerce_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'nnlm_commerce'). '/views',
  );
}

/**
 * Implements hook_commerce_cart_product_remove
 *
 * Tasks:
 *   Clear the entity reference of the original shopping experience.
 *
 * @param  array $order     The current commerce order
 * @param  array $product   The current product being added to the cart
 * @param  int $quantity    The quantity being added
 * @param  array $line_item Not used
 *
 * @return NULL
 */
function nnlm_commerce_commerce_cart_product_remove($order, $product, $quantity, $line_item) {
  if (empty($order->commerce_line_items)) {
    U::unset_shopping_origin($order);
  }
  nnlm_core_dump($order, __FUNCTION__ . " Order");
}

/**
 * Implements hook_commerce_cart_product_add
 * Performs the following actions:
 * Prevents the addition if the item being added is of a different product
 * type or editorial section than items currently in the cart.
 * Stores the editorial section of the shopping page in the 'data' element
 * of the new (or updated) order
 * Stores the active trail (breadcrumb) of the shopping page in the 'data'
 * element of the new (or updated) ordering
 *
 * @param  array $order     The current commerce order
 * @param  array $product   The current product being added to the cart
 * @param  int $quantity    The quantity being added
 * @param  array $line_item Not used
 *
 * @return NULL
 */
function nnlm_commerce_commerce_cart_product_add($order, $product, $quantity, $line_item) {
  //prevent addition if different product type or editorial section than
  //existing cart items.
  try {
    if (empty($order->field_commerce_origin_node)) {
      if (arg(0) == 'node' && is_numeric(arg(1))) {
        // Get the nid
        $nid = arg(1);
      }
      if (empty($nid)) {
        throw new \Exception("Could not find node nid of shopping experience");
      }
      U::set_shopping_origin($nid, $order);
    }
  }
  catch(\Exception$e) {
    nnlm_core_dump($e->getMessage(), "Exception while attempting to save reference to origin node in nnlm_commerce");
    commerce_line_item_delete($line_item->line_item_id);
    drupal_set_message(NNLM_COMMERCE_GENERIC_ERROR);
  }
  nnlm_core_dump($order, "Order");


  //Note: below this point is the code for limiting products to be the
  //same type as the type of the first product in the cart (same shopping
  //experience origin)
  //perform checking to make sure incoming product is of the same
  //editorial section and type as the first product
  /*$order_section = U::get_order_section($order);
  $order_type = U::get_order_type($order);
  nnlm_core_dump($order_section, "First product editorial section");
  //nnlm_core_dump($order_type, "First cart product type");

  if (
    (($product->field_section['und'][0]['tid'] !== $order_section->tid) &&
      (arg(0) !== $order_section->name)
    ) ||
    ($product->type !== $order_type)
  ) {

    //editorial section mismatch.  remove  most recently added item from
    drupal_set_message(t("You have attempted to add a product to a shopping cart that is already full of products from a different NN/LM store.  Please empty your cart and try again."), 'error');
    return FALSE;
  }*/

  //store the editorial section to maintain look and feel in checkout
  //process.
  //use custom version.
  /*

  //add a link to the terminal item.
  $current_item = menu_get_item();
  $current_page = &$active_trail[count($active_trail) - 1];
  $current_page['path'] = $current_item['path'] ?  : $current_item['link_path'];
  $current_page['link_path'] = $current_page['path'];
  $current_page['href'] = $current_page['path'];*/

  //Note: I'm not sure if the following will always be reliable.  It must be
  //the case that every shopping cart page has an editorial section as the
  //first path element.
  /*$order->data['editorial_section'] = arg(0);
  commerce_order_save($order);
  nnlm_core_dump($order, "Commerce order (modified)");
  drupal_set_message(t("Item added to cart."));*/
}

/**
 * Implements hook_commerce_checkout_router
 * The page that originated the shopping process is embedded with the order.
 * As a final operation, this router hook is used to send the user back to that
 * page after the order is complete.
 *
 * @param  array $order         The current order in the checkout process
 * @param  array $checkout_page The current page in the checkout process
 *
 * @return NULL
 */
function nnlm_commerce_commerce_checkout_router($order, $checkout_page) {
  nnlm_core_dump($order, __FUNCTION__.": order");
  nnlm_core_dump($checkout_page, __FUNCTION__.": checkout page");
  //$alias = drupal_get_path_alias('node/'.$origin->nid);
  switch ($checkout_page['page_id']) {
    case 'complete':

      $order_recipients = nnlm_commerce_fetch_order_recipients($order);
      if (empty($order_recipients)) {
        drupal_set_message(t("An error has occurred in the ordering process.  We could not determine the appropriate NN/LM contact to send your order request to.  Please contact your RML and notify them of this error."), "error");
      }
      else {
        drupal_set_message(t("Order complete. A confirmation email will be sent to the email address you provided during the order.  To modify or cancel the order, please contact your RML."), 'status', FALSE);
        nnlm_core_dump(implode(',', array_values($order_recipients)), "Order recipients");
        //TODO: send emails.
      }
      $origin_link = end($order->data['active_trail']);
      unset($order->data['history']);
      /* We must now send emails to the people responsible for this order. Assume for
    now that it is just one person, as we may be applying restrictions so that
    different product types or different editorial sections cannot coexist. */

      drupal_goto($origin_link['link_path']);
      break;

    default:
      break;
  }
}

function nnlm_commerce_product_type_info(&$product_types) {
  nnlm_core_dump($product_types, "Product types");
}

/**
 * Implements hook_form_FORM_ID_alter
 * Fires during the checkout page display
 *
 * @param  array $form       The commerce form
 * @param  array $form_state The state of the commerce form
 * @param  string $form_id   The id of the commerce form
 *
 * @return NULL
 */
function nnlm_commerce_form_commerce_checkout_form_checkout_alter(&$form, &$form_state, $form_id) {
  nnlm_core_dump(__FUNCTION__);
  U::set_theme();
  F::update_redirect($form, $form_state);
}
function nnlm_commerce_cart_checkout_form_cancel_submit(&$form, &$form_state){
  nnlm_core_dump(__FUNCTION__);
  if(isset($form_state['commerce_form_cancel_submit'])){
    call_user_func($form_state['commerce_form_cancel_submit'], $form, $form_state);
  }
}
/**
 * Implements hook_form_FORM_ID_alter
 * Fires during the review page display
 *
 * @param  array $form       The commerce form
 * @param  array $form_state The state of the commerce form
 * @param  string $form_id   The id of the commerce form
 *
 * @return NULL
 */
function nnlm_commerce_form_commerce_checkout_form_review_alter(&$form, &$form_state, $form_id) {
  nnlm_core_dump(__FUNCTION__);
  U::set_theme();
}
function nnlm_commerce_submit_handler(){

}
function nnlm_commerce_form_views_form_commerce_cart_form_default_alter(&$form, &$form_state, $form_id) {
  nnlm_core_dump(__FUNCTION__);
  U::set_theme();
  F::update_redirect($form_state);
}

function nnlm_commerce_commerce_customer_profile_type_info() {
  $profile_types = array();
  try {
    $sections = Core_Utilities::get_editorial_sections('full');
    foreach (commerce_product_types() as $type) {
      //nnlm_core_dump($type, "Product type");
      $profile_types[$type['type'] . '_default_billing'] = array(
        'type' => $type['type'] . '_default_billing',
        'name' => $type['name'] . t(" Default"),
        'description' => t("The default billing profile for the $type[name] product type.   Used on checkout and order forms."),
        'help' => t("This billing profile contains the fields agreed upon by all regions for the necessary information products of type $type[name]"),
      );
      /*foreach ($sections as $section) {
        $w_section = entity_metadata_wrapper('taxonomy_term', $section);
        $section->full_name = $w_section->field_full_section_name->value();
        $profile_types[$type['type'].'_'.$section->name.'_billing'] = array(
          'type' => $type['type'].'_'.$section->name.'_billing',
          'name' => $section->full_name . ' ' . $type['name'] . ' Billing Profile',
          'description' => t("A custom billing profile for the $section->full_name editorial section and $type[name] product type.  Used on checkout and order forms."),
          'help' => t("This billing profile is necessary to satisfy the extra requirements for customer information gathered for this region and product type"),
        );
      }*/
    }
  }
  catch(\Exception$e) {
    nnlm_core_dump($e->getMessage(), "exception", 'error');
  }
  //nnlm_core_dump($profile_types, "Profile types");
  return $profile_types;
}

/**
 * Implements hook_help
 * returns help about this module
 *
 * @param  array $path The router menu path
 * @param  array $arg An array that corresponds to the return value of the arg() function
 * @see  http://goo.gl/ggKxBd
 */
function nnlm_commerce_help($path, $arg) {
  if (strpos($path, 'admin/config/system/nnlm/commerce') === FALSE) {
    return;
  }
  $output = '';
  $output .= '<h3>' . t('Help') . '</h3>';
  $output .= '<p>' . t('This module is designed to make modifications to the commerce module, in order to get it to function correctly for the various regions as a product ordering system.') . '</p>';
  return $output;
}

/**
 * Implements hook_menu().
 *
 * Provides the menu for this module.
 *
 * @return  NULL
 * @see https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu/7
 */
function nnlm_commerce_menu() {
  $items = array();
  $items['admin/config/system/nnlm/commerce'] = array(
    'title' => 'NN/LM Commerce Settings',
    'description' => 'The NN/LM Commerce module consists of all custom code required to make the commerce module work appropriately for the NN/LM environment',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nnlm_commerce_admin'),
    'access arguments' => array('administer nnlmcore settings'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/permissions'] = array(
    'title' => 'Permissions',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  return $items;
}

/**
 * Implements hook_menu_breadcrumb_alter
 *
 * Modifies the breadcrumb to point to the region root instead of
 * the national home
 *
 * @param  array $active_trail An array containing breadcrumb links
 * for the current page.
 * @param  array $item The menu router item of the current page.
 * @see  http://goo.gl/h6y3ua
 *
 * @return  NULL
 */
function nnlm_commerce_menu_breadcrumb_alter(&$active_trail, $item) {
  return;
  if (!variable_get('nnlm_commerce_bookmark_enabled', TRUE)) {
    return;
  }
  if (!context_isset('context', 'commerce_active')) {
    return $active_trail;
  }
  $origin = U::get_shopping_origin();
  $active_trail = array(Core_Utilities::standardize_link($origin));
  return;

  $active_order = &Controller::load_active_order();
  if (isset($active_order->data['active_trail'])) {
    $active_trail = $active_order->data['active_trail'];
    //set to only the editorial section root.
    $active_trail = array(reset($active_order->data['active_trail']));
    $current_page = array(
      'menu_name' => 'menu-national-main-menu',
      'link_path' => '<nolink>',
      'weight' => 0,
      'link_title' => check_plain(drupal_get_title(), array()),
      'hidden' => 0,
      'has_children' => 0,
      'expanded' => 0,
      'options' => array(),
      'module' => 'menu',
      'customized' => 0,
      'updated' => 0,
    );
    $current_page = Core_Utilities::standardize_link($current_page);
    $active_trail[] = $current_page;
  }
}

/**
 * The following are placeholder functions for the various
 * static class methods of the same name
 */
function nnlm_commerce_submit_helper($element) {
  nnlm_core_dump(__FUNCTION__);
  $callback = $element['#nnlm_commerce_submit_callback'];
  call_user_func($callback, $element);
}

