<?php
namespace Drupal\nnlm_commerce;
use Drupal\nnlm_core\Utilities as Core_Utilities;

/**
 * Provides reusable functions that make modifications via the Forms API.
 */
class Forms {

  /**
   *
   * @internal Changes the default "Go Back" hyperlink to a Back button
   * in the form.
   *
   * @param  array $form       The commerce form the button belongs to
   * @param  array $form_state The state of the commerce form the button belongs
   *                           to
   * @param  string $form_id   The id of the form
   *
   * @return NULL
   */
  public static function add_back_button(&$form, &$form_state, $form_id) {
    nnlm_core_dump(__FUNCTION__);
    if (empty($form['buttons']['back']['#value'])) {
      return;
    }
    $form['buttons']['back'] = array(
      '#type' => 'submit',
      '#value' => 'Back',
      '#submit' => array(
        'nnlm_commerce_submit_helper',
      ),
      '#nnlm_commerce_submit_helper' => array(
        __NAMESPACE__ . '\\Forms::back_button_submit',
      ),
      '#limit_validation_errors' => array(),
    );
  }
  public static function update_redirect(&$form, &$form_state){
    if(isset($form['buttons']['cancel'])){
      $button =& $form['buttons']['cancel'];
      if(!isset($button['#submit'])){
        //no submit handler, do not change.
        return;
      }
      nnlm_core_dump("Changing submit handler of cancel button");
      $form_state['commerce_form_cancel_submit'] = $button['#submit'];
      $button['#submit'] = 'nnlm_commerce_cart_checkout_form_cancel_submit';
    }

    nnlm_core_dump($form, __FUNCTION__.": form");
    nnlm_core_dump($form_state, __FUNCTION__.": form state");
  }
  /**
   * Tasks:
   *   - set the cancel redirect location in the form state to the location
   *     of the commerce experience start page
   *
   * @param array $form_state The state of the commerce form
   */
  public static function update_cancel_redirect(&$form_state) {
    try {
      if (isset($form_state['cancel_redirect'])) {
        $form_state['cancel_redirect'] = Utilities::get_redirect_alias();
      }
    }
    catch(\Exception$e) {
      nnlm_core_dump($e->getMessage(), __FUNCTION__, 'error');
      drupal_set_message(NNLM_COMMERCE_GENERIC_ERROR, 'error');
    }
  }
  /**
   * Tasks:
   *   - set the back button location in the checkout page state to the location
   *     of the commerce experience start page
   *
   * @param array $form_state The state of the commerce form
   */
  public static function update_back_button(&$form_state) {
    try {
      if(!isset($form_state['checkout_page'])){
        return;
      }
      $form_state['checkout_page']['prev_page'] = Utilities::get_redirect_alias();
    }
    catch(\Exception$e) {
      nnlm_core_dump($e->getMessage(), __FUNCTION__, 'error');
      drupal_set_message(NNLM_COMMERCE_GENERIC_ERROR, 'error');
    }
  }

  /**
   * @internal  Adds a cancellation button that takes the user back to
   *  the page that contained the user's shopping cart and product list.
   *
   * @param  array $form       The commerce form the button belongs to
   * @param  array $form_state The state of the commerce form the button belongs
   *                           to
   * @param  string $form_id   The id of the form
   *
   * @return NULL
   */
  public static function add_cancellation_button(&$form, &$form_state, $form_id) {
    nnlm_core_dump(__FUNCTION__);
    $form['buttons']['cancel'] = array(
      '#type' => 'submit',
      '#value' => 'Cancel',
      '#submit' => array(
        'nnlm_commerce_submit_helper',
      ),
      '#nnlm_commerce_submit_helper' => array(
        __NAMESPACE__ . '\\Forms::cancel_button_submit',
      ),
      '#limit_validation_errors' => array(),
    );
  }

  /**
   * @internal  Updates the 'Continue to next step' button to be consistent,
   * and to call an internal hook in this module for further processing.
   *
   * @param  array $form       The commerce form the button belongs to
   * @param  array $form_state The state of the commerce form the button belongs
   *                           to
   * @param  string $form_id   The id of the form
   *
   * @return NULL
   */
  public static function add_next_button(&$form, &$form_state, $form_id) {
    nnlm_core_dump(__FUNCTION__);
    $form['buttons']['continue'] = array(
      '#value' => 'Next',
      '#submit' => array(
        'nnlm_commerce_submit_helper',
      ),
      '#nnlm_commerce_submit_helper' => array(
        __NAMESPACE__ . '\\Forms::next_button_submit',
      ),
      '#limit_validation_errors' => array(),
    );
  }

  /**
   * Executed whenever the user presses the back button during the checkout
   * process
   *
   * @param  array $form       The commerce form the button belongs to
   * @param  array $form_state The state of the commerce form the button belongs
   *                           to
   * @param  string $form_id   The id of the form
   *
   * @return NULL
   */
  public static function back_button_submit(&$form, &$form_state) {
    nnlm_core_dump(__FUNCTION__);
    try {
      $active_order = &Controller::load_active_order();
      $last_page = array_pop($active_order->data['history']);
      Controller::save_active_order($active_order);
      $form_state['redirect'] = $last_page;
    }
    catch(\Exception$e) {
      drupal_set_message(t($e->getMessage()), 'error', FALSE);
    }
  }

  /**
   * Executed whenever the user clicks the cancel button during the
   * checkout process
   *
   * @param  array $form       The commerce form the button belongs to
   * @param  array $form_state The state of the commerce form the button belongs
   *                           to
   * @param  string $form_id   The id of the form
   *
   * @return NULL
   */
  public static function cancel_button_submit(&$form, &$form_state) {
    nnlm_core_dump(__FUNCTION__);
    // Update the order to the cart status.
    drupal_set_message(t('Checkout of your currentz order has been canceled and may be resumed when you are ready.'));
    try {
      // can throw Exception]
      drupal_set_message(t("one"), 'status', FALSE);
      nnlm_core_dump($form, __FUNCTION__ . ": form");
      drupal_set_message(t("two"), 'status', FALSE);
      $order = &Controller::load_active_order();
      nnlm_core_dump($order, "Active order");
      $form_state['order'] = commerce_order_status_update($order, 'cart', TRUE);

      // Skip saving in the status update and manually save here to force a save
      // even when the status doesn't actually change.
      if (variable_get('commerce_order_auto_revision', TRUE)) {
        $form_state['order']->revision = TRUE;
        $form_state['order']->log = t('Customer manually canceled the checkout process.');
      }
      Controller::save_active_order($form_state['order']);

      // Redirect to conference/register on cancel.
      // the page you want to redirect to.
      nnlm_core_dump("Redirecting");
      $form_state['redirect'] = array(
        'node/' . U::get_shopping_origin($order)->nid,
      );
    }
    catch(\Exception$e) {
      nnlm_core_dump($e->getMessage(), 'error', FALSE);
      drupal_set_message(NNLM_COMMERCE_GENERIC_ERROR);
    }
  }

  /**
   * Executed whenever the user presses the next button during the checkout
   * process
   *
   * @param  array $form       The commerce form the button belongs to
   * @param  array $form_state The state of the commerce form the button belongs
   *                           to
   * @param  string $form_id   The id of the form
   *
   * @return NULL
   */
  public static function next_button_submit($form, $form_state) {
    nnlm_core_dump(__FUNCTION__);
    try {
      $active_order = &Controller::load_active_order();
      $active_order->data['history'][] = drupal_get_destination();
      Controller::save_active_order($active_order);
    }
    catch(\Exception$e) {
      nnlm_core_dump($e->getMessage(), 'error', FALSE);
    }
  }
}

