
(function ($) {
  Drupal.behaviors.slideshow = {
    attach: function (context, settings) {
      $('.example', context).click(function () {
        $(this).next('ul').toggle('show');
      });
      console.log("Slideshow initialized");
    }
  };
})(jQuery);