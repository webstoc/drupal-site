/* Todo: add js */
(function($) {
  Drupal.behaviors.NNLM = Drupal.behaviors.NNLM || {};
  Drupal.behaviors.NNLM.google_cse = {
    /**
     * Invoked on page load
     */
    attach: function(context, settings) {
      console.log("NN/LM Google CSE javascript initialized");
      if(!window.google){
        console.error("Google element not available for callback");
      }
    }
  };
})(jQuery);