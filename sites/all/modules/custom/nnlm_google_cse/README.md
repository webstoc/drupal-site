##Google CSE
--------------


This module works in conjunction with the [NN/LM Drupal Theme](https://bitbucket.org/webstoc/nn-lm-drupal-theme) module to provide a search interface for all NN/LM Drupal hosted pages.  It primarily provides the target pages for each of the regions as landing pages for the Google Custom Search Engine, using Google's search [refinement mechanism](https://cse.google.com/cse/search/refinements?cx=000959108599181809099%3A5oawdbpcf3o).  NN/LM's CSE instances may be managed using the webstoc@gmail.com account, and [this](https://cse.google.com/cse/all) url.

Search results are expected at the relative url search-results/%, where % is one of the regional sub-strings (e.g. 'gmr', 'mcr', et cetera).  The url 'search-results' by itself should display the results for all regions combined.  The query is expected via parameter "google", rather than Google's default value of "q".

###Usage notes

This package is maintained in git using a simplified version [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow/) workflow methodology.  It currently has 2 distinct branches, 'develop' and 'master'.  All modifications should be done either on the develop branch, or a branch cloned from develop.

This package uses Grunt for build tooling, and should *not* be directly copied into place.  To deploy this module to the NN/LM development instance of Drupal, first [set up your system](http://gruntjs.com/getting-started) to handle Grunt build tooling, and then invoke 'grunt sandy-dev'.  To deploy to staging and production, commit all changes to the 'master' branch, and push to the remote repository.  The CI server will detect the change and test and deploy the code from there.