<?php
namespace NNLM\Migration\Migration\Evaluation;
use NNLM\Migration\Migration\BasicNodeMigration as BasicNodeMigration;
use NNLM\Migration\Utilities as U;
/**
 * BasicNodeMigration moves all basic page content that exists in this region.
 **/
class EvaluationBasicNodeMigration extends BasicNodeMigration {
  // A Migration constructor takes an array of arguments as its first parameter.
  // The arguments must be passed through to the parent constructor.
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->setEditorialSection('evaluation');

    //Replace query with custom condition for this migration
    $query = $this->getSource()->query();
    $query->condition('m.loc', 'http://nnlm.gov/evaluation/%', 'LIKE');
    //recreate the migration source based on the updated query
    $this->source = new \MigrateSourceSQL($query, $this->getSource()->fields());

    //replace relevant field mappings
    //NONE for national - uses defaults
  }
}
