<?php
namespace NNLM\Migration\Migration\General;
use NNLM\Migration\Utilities as U;

/**
 * migration of all users into drupal.  Restricted to users in the rosters system
 * that have a current position, and that have assigned content in the migration
 * map.  See RostersMigrationSource for query details.
 */
class UserMigration extends \Migration {
  public function __construct($arguments) {
    // The basic setup is similar to BeerTermMigraiton
    parent::__construct($arguments);
    //https://drupal.org/node/1117454
    //only updates existing fields if the node already exists.
    foreach (array('content creator', 'content moderator', 'drupal administrator') as $role) {
      $this->role_id_map[$role] = U::get_role_id($role);
    }
    $this->description = t('All staff members');
    $this->map = new \MigrateSQLMap($this->machineName,
      array('peopleid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'PeopleID.',
        ),
      ),
      \MigrateDestinationUser::getKeySchema()
    );
    $source_fields = array(
      'peopleid' => t("The NN/LM Rosters system PeopleID value that uniquely defines a person"),
      'name' => t('NN/LM / Drupal username'),
      'mail' => t('Email address.'),
      'status' => t('The status of the user account - 1 for active or 0 for blocked. (decided on import by whether the rosters db has a current position for the user.)'),
    );
    $additional_fields = array(
      'picture' => t("O for no picture (default during import, for now)"),
    );

    //Requirement: custom view that retrieves exact results we want: all users
    //with content. We then limit by the current region code.
    $query = \Database::getConnection('default', 'rosters')->select('users_with_content', 'u');
    $query->fields('u', array_keys($source_fields));

    $this->source = new \MigrateSourceSQL($query, $source_fields);
    $this->destination = new \MigrateDestinationUser();

    $this->addFieldMapping('uid', 'peopleid')->description($source_fields['peopleid']);
    //The Drupal username.
    $this->addFieldMapping('name', 'name');
    $this->addFieldMapping('pass')->defaultValue("my default drupal password");
    //The email address associated with the user account.
    $this->addFieldMapping('mail', 'mail')->description($source_fields['mail']);
    //initial email address for the drupal account
    $this->addFieldMapping('init', 'mail')->description($source_fields['mail']);
    //The status of the user account - 1 for active or 0 for blocked.
    $this->addFieldMapping('status', 'status')->description($source_fields['status']);
    $this->addFieldMapping('is_new')->defaultValue(FALSE);
    $this->addUnmigratedDestinations(array(
        'access',
        'created',
        'data',
        'language',
        'login',
        'path',
        'pathauto',
        'picture',
        // roles are added in the prepare phase.
        'roles',
        'role_names',
        'signature',
        'signature_format',
        'theme',
        'timezone',
        // TODO: flush out this variable
      ), t('Do Not Migrate'));
    //U::dump($query->__toString());
  }
  public function prepare($node, $row) {
    $old_account = user_load($node->uid, TRUE);
    if (!empty($old_account)) {
      //if an account currently exists in Drupal, dynamically change system
      //of record to update the existing record rather than create a new one.
      //Why Migrate doesn't have a setting for this is beyond me, but whatever.
      $this->systemOfRecord = \Migration::DESTINATION;
      $node->is_new = FALSE;
    }
    else{
      $node->is_new = TRUE;
    }
    $content_moderators = array(
      '4003' => 'Aron Beal',
      '4' => 'Michael Boer',
      '81' => 'Karla Borque',
      '4585' => 'Emily Hurst',
      '4039' => 'John Bramble',
      '13'=> "Susan Barnes",
      '88'=>'Colette B',
      '4801'=>'mjharvey',
      '4903'=>'mahria',
      '4763'=>'myrna m',
      '4781'=>'matt s.',
      '4137'=>'marco t',
      '4947'=>'tiffany t'
    );
    $drupal_administrators = array(
      '4003' => 'Aron Beal',
      '4' => 'Michael Boer',
    );
    //status of 1 is an active user.  It is also a user that has at least one
    //active position, as those are the terms of the import process.
    if (intval($node->status) === 1) {
      $node->roles[$this->role_id_map['content creator']] = 'content creator';
      if (in_array(strval($node->uid), array_keys($content_moderators))) {
        $node->roles[$this->role_id_map['content moderator']] = 'content moderator';
      }
      if (in_array(strval($node->uid), array_keys($drupal_administrators))) {
        $node->roles[$this->role_id_map['drupal administrator']] = 'drupal administrator';
      }
    }
    $this->systemOfRecord = \Migration::SOURCE;
  }
}

