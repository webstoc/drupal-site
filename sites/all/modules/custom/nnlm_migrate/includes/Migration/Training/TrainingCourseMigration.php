<?php
namespace NNLM\Migration\Migration\Training;
use NNLM\Migration\Migration\CourseHomePageMigration;
use NNLM\Migration\Utilities as U;
/**
 * BasicNodeMigration moves all basic page content that exists in this region.
 **/
class TrainingCourseMigration extends CourseHomePageMigration {
  // A Migration constructor takes an array of arguments as its first parameter.
  // The arguments must be passed through to the parent constructor.
  public function __construct($arguments) {
    $this->description = t("Training Course Homepage Migration.");
    $this->setEditorialSection('training');
    $this->buildBaseQuery();
    $this->query->condition(db_or()
      ->condition('m.loc', 'http://nnlm.gov/training/%', 'LIKE')
      //OR???
    );
    //$this->query->condition('m.id', '11332');
    //$this->query->range(0,1);

    parent::__construct($arguments);
  }
}
