<?php
namespace NNLM\Migration\Migration\Training;
use NNLM\Migration\Migration\BasicNodeMigration as BasicNodeMigration;
use NNLM\Migration\Utilities as U;
/**
 * BasicNodeMigration moves all basic page content that exists in this region.
 **/
class TrainingBasicNodeMigration extends BasicNodeMigration {
  // A Migration constructor takes an array of arguments as its first parameter.
  // The arguments must be passed through to the parent constructor.
  public function __construct($arguments) {
    $this->description = t("Training Basic Node Migration.");
    $this->setEditorialSection('training');
    $this->buildBaseQuery();
    $this->query->condition(db_or()
      ->condition('m.loc', 'http://nnlm.gov/training/%', 'LIKE')
      //OR???
    );
    parent::__construct($arguments);
    //$this->query->range(0,1);

    //replace relevant field mappings
    //NONE for Training - uses defaults
  }
}
