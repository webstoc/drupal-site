<?php
namespace NNLM\Migration\Migration\PSR;
use NNLM\Migration\Migration\BasicNodeMigration as BasicNodeMigration;
use NNLM\Migration\Utilities as U;
/**
 * BasicNodeMigration moves all basic page content that exists in this region.
 **/
class PSRBasicNodeMigration extends BasicNodeMigration {
  // A Migration constructor takes an array of arguments as its first parameter.
  // The arguments must be passed through to the parent constructor.
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->setEditorialSection('psr');
    //Replace query with custom condition for this migration
    $query = $this->getSource()->query();
    //TODO: expand once more national pages are identified
    $query->condition('m.loc', 'http://nnlm.gov/' . $this->section->name . '/%', 'LIKE');
    //$query->range(0,1);
    //recreate the migration source based on the updated query
    $this->source = new \MigrateSourceSQL($query, $this->getSource()->fields());
  }
}
