<?php

/**
 *
 * After registering this autoload function with SPL, the following line
 * would cause the function to attempt to load the \Foo\Bar\Baz\Qux class
 * from /path/to/module/includes/Baz/Qux.php:
 *
 *      new \NNLM\Migration\Baz\Qux;
 *
 * @param string $class The fully-qualified class name.
 *
 * @return void
 */
spl_autoload_register(
function ($class) {
    //
    // project-specific namespace prefix
    $prefix = 'NNLM\\Migration\\';
    // base directory for the namespace prefix
    $base_dir = join(DIRECTORY_SEPARATOR, array(
        drupal_get_path('module', 'nnlm_migrate'),
        'includes',
      ));
    // does the class use the namespace prefix?
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
      // no, move to the next registered autoloader
      return;
  }
    // get the relative class name
    $relative_class = substr($class, $len);
    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = join(DIRECTORY_SEPARATOR, array(
        $base_dir,
        str_replace('\\', DIRECTORY_SEPARATOR, $relative_class) . '.php',
      )
    );
    // if the file exists, require it
    if (file_exists($file)) {
      require_once ($file);
  }
  else {
      $trace = debug_backtrace();
      $caller = array_slice($trace, 0, 4);
      print "Exception call stack:";
      foreach($trace as $arr){
        print $arr['function']."\n";
      }
      throw new \Exception("File $file could not be found (required by class $class)");
  }
}
);

