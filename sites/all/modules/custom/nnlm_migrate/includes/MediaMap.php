<?php

/**
 * @file
 */
namespace NNLM\Migration;
use NNLM\Migration\Utilities as U;

/**
 * For sophisticated tracking of media items during the migration process.
 */
class MediaMap {

  // In seconds.
  const MEDIA_MAP_CACHE_LIFETIME = 3600;
  protected $map = NULL;
  protected $indices = array(
    'loc'=>array(),
    'file_md5'=>array(),
    'scald_id'=>array(),
    'drupal_node_id'=>array()
  );
  protected static $allowed_types = array('file', 'image', 'audio', 'video',
  );
  protected static $field_map = array(
    'fid' => 'drupal_node_id',
    'scald_id' => 'scald_id',
    'md5' => 'file_md5',
  );
  protected $log = array();
  /**
   *
   */
  public function __construct($rebuild = FALSE) {
    if (!$rebuild) {
      $cache = cache_get('nnlm_migration_hash_map');
      if (isset($cache) && !empty($cache->data)) {
        $this->map = $cache->data;
        return;
      }
    }
    $this->buildMap();
    U::dump(count($this->map), "Total number of map entries");
  }

  /**
   * Returns the entry located at the index $loc.
   *
   * @param string $uri
   *   case insensitive url of the media item
   *
   * @return mixed      The object containing the item data, or NULL if not found.
   */
  public function getMapEntry($uri, $options = array()) {
    return $this->findMapEntry($uri, $options);
  }
  /**
   * Finds an entry in the map, searching by any valid index.  Currently,
   * this is limited to scald_id, file_md5 and loc
   * @param  string $key   The key to search by
   * @param  string $value The value to be searched for
   * @return mixed        The entry, or NULL if none is found.
   */
  public function findMapEntry($key, $value, $options=array()){
    if($key === 'loc'){
      $value = strtolower($value);
    }
    if(!isset($this->indices[$key])){
      if($options['log_failure']){
        U::log("MediaMap::findMapEntry: error - '".  $key . "'' is not a valid searchable index");
        return NULL;
      }
    }
    $index = $this->indices[$key][$value];
    if(empty($index)){
      return NULL;
    }
    return $this->map[$index];
  }
  /**
   * Updates the migration map to reflect the values stored in the media map.  Note:
   * this uses the migration map defined in the settings.php file, which is different
   * between dev and prod (dev uses migration_map_test).
   *
   * @param string $uri
   *   The uri of the media item to be persisted
   * @param array $valuesA
   *   list of keys and values to persist to the database.
   *   A list of keys and values to persist to the database.
   *
   * @return NULL
   *
   * @throws MigrateException If the uri $uri is not a legitimate url in the migration map db.
   */
  protected function updateMigrationMap($entry) {
    U::dump($entry, "Updating migration map with entry:");
    // Map values to correct db field names.
    $values = array();
    $md5_value = $entry->file_md5;
    $persistable_values = array("loc", "file_md5", "drupal_node_id", "scald_id");
    foreach ($entry as $k => $v) {
      if (isset(self::$field_map[$k])) {
        if(!in_array(self::$field_map[$k], $persistable_values)){
          continue;
        }
        $values[self::$field_map[$k]] = $v;
      }
      else{
        if(!in_array($k, $persistable_values)){
          continue;
        }
        $values[$k] = $v;
      }
    }
    unset($values['file_md5']); //never persist the md5 value to the db.

    // $original_connection = \Database::setActiveConnection('migration_map');
    $fields = array();
    foreach ($values as $k => $v) {
      $fields["$k"] = "$v";
    }
    U::dump("Pre-transaction");
    $connection = \Database::getConnection('default', 'migration_map');
    $query = $connection->select('migration_map', 'm')->fields('m')->condition('file_md5', $md5_value);
    $result = $query->countQuery()->execute();

    foreach($result as $row){
      foreach($values as $k=>$v){
        if(!isset($row->{$k})){
          unset($values[$k]);
        }
      }
      break;
    }
    U::dump($entry, "Updating entry");
    U::dump($values, "Final values");
    $txn = $connection->startTransaction(NULL);
    try {
      $query = $connection->update('migration_map')->fields($fields)->condition('file_md5', $md5_value);
      $num_updates = $query->execute();
      U::dump("Updated $num_updates entries in the migration map.");
      U::dump("Transaction complete");
    }
    catch (Exception$e) {
      U::dump("Transaction exception, rolling back");
      // Something went wrong somewhere, so roll back now.
      $txn->rollback();
      // Log the exception to watchdog.
      throw new \MigrateException("Exception: " . $e->getMessage() . " (loc $uri)");
    }

    return TRUE;
  }

  /**
   * Adds a new entry to the map.
   *
   * @param string $uri
   *   The url of the media item.  Case insensitive
   * @param array $valuesAny
   *   initial values to pass to the item.  Required
   *   Any initial values to pass to the item.  Required
   *                      values not passed will be set to be NULL.
   *
   * @return Returns a copy of the newly created item, or NULL if there
   *  was an issue during creation.
   */
  public function addMapEntry($values = array(), $persist=FALSE) {
    if(!isset($values['loc'])){
      throw new \Exception("Required value 'loc' not found in addMapEntry function call");
    }
    $lower_url = strtolower($values['loc']);
    $existing_entry = $this->getMapEntry($lower_url);
    if (!empty($existing_entry)) {
      return $existing_entry;
    }
    $new_entry = new \stdClass();
    // Map values to correct db field names.
    foreach ($values as $k => $v) {
      // Note the reverse order here.  It is correct.
      if (isset(self::$field_map[$v])) {
        unset($values[$v]);
        $values[self::$field_map[$v]] = $k;
      }
    }
    $values = $values + array(
      'file_md5' => NULL,
      'loc' => NULL,
      'scald_id' => NULL,
      'drupal_node_id' => NULL,
      'filesystem_uri' => str_replace("http://nnlm.gov/", "/var/www/html/", $lower_url),
      'case_corrected_filesystem_uri' => U::real_filename($filesystem_uri),
    );
    foreach ($values as $k => $v) {
      if (empty($v)) {
        $new_entry->{$k} = NULL;
        continue;
      }
      $new_entry->{$k} = $v;
    }
    $this->map[] =& $new_entry;
    $index = (count($this->map) - 1);
    foreach ($this->indices as $k=>$arr) {
      if($k == 'loc'){
        $this->indices['loc'][$lower_url] = $index;
        continue;
      }
      if (!empty($entry->{$k})) {
        $this->indices[$k][$entry->{$k}] = $index;
      }
    }
    if($persist){
      $this->updateMigrationMap($new_entry);
    }
    return $new_entry;
  }

  /**
   * Verifies all required parts of a map entry are non-null.
   *
   * @param string $uri
   *   The url index of the media item
   *
   * @return boolean      TRUE if verified, FALSE otherwise
   */
  public static function verifyMapEntry($uri) {
    if (is_string($uri)) {
      $entry = $this->getMapEntry($uri);
    }
    elseif (is_object($uri)) {
      $entry = $uri;
      $uri = $entry->loc;
    }
    else {
      throw new \Exception("Invalid type " . gettype($uri) . " passed to verifyMapEntry");
    }
    foreach ($entry as $k => $v) {
      switch ($k) {
        case 'file_md5':
          if (empty($v)) {
            U::dump("Map entry for $uri failed verification: md5 value is empty", 'Error', 'error');
            return FALSE;
          }
          break;

        case 'scald_id':
          if (empty($v)) {
            U::dump("Map entry for $uri failed verification: scald_id is empty", 'Error', 'error');
            return FALSE;
          }
          break;

        case 'drupal_node_id':
          if (empty($v)) {
            U::dump("Map entry for $uri failed verification: drupal_node_id (fid) is empty", 'Error', 'error');
            return FALSE;
          }
          break;

        default:
          break;
      }
    }
    return TRUE;
  }

  /**
   * Updates the map file entry if missing.
   *
   * @param string $uri
   *   The uri of the media item
   *
   * @return boolean      True if the $uri entry of the map item refers to an
   *                           actual mangaged drupal file, FALSE otherwise.
   */
  public function verifyFileEntry($uri) {
    $filepath = U::real_filename($uri);
    return (empty($filepath)) ? FALSE : TRUE;
  }
  /**
   *
   */
  public function importFileEntry($uri) {
    U::dump("Attempting to import new file $uri");
    if (empty($uri)) {
      throw new \MigrateException("URI was empty in MediaMap::importFileEntry");
    }

    // U::log($uri, "File entry for file not found. Importing", 'notice');
    $lower_url = strtolower($uri);
    $filesystem_uri = str_replace("http://nnlm.gov/", "/var/www/html/", $lower_url);
    $case_corrected_filesystem_uri = U::real_filename($filesystem_uri);
    if (!$case_corrected_filesystem_uri) {
      throw new \MigrateException("File $filesystem_uri does not exist on filesystem.  Cannot import");
    }

    // Check md5 value to make sure it doesn't already exist in Drupal.
    $md5_value = md5_file($case_corrected_filesystem_uri);
    if (!empty($md5_value)) {
      $query = db_select('filehash', 'f')->fields('f', array('fid', 'md5'))->condition('md5', $md5_value, '=')->range(0, 1);
      $result = $query->execute();
      if ($record = $result->fetchAssoc()) {
        U::dump($md5_value, "Import not required - file found.  Hash sum");
        // Confirm the atom exists along with the file.
        $sid = scald_search(array('base_id' => $result->fid), FALSE, TRUE);
        if (empty($sid)) {
          throw new \MigrateException("File $uri exists in Drupal, but has no scald atom.  Cannot import");
          return FALSE;
        }
        else {
          U::dump("Scald atom found with sid $sid");
        }
        $values = array(
          'scald_id' => $sid,
          'drupal_node_id' => $record['fid'],
          'file_md5' => $md5_value,
        );
        if (!$this->updateMapEntry($uri, $values)) {
          throw new \Exception("Couldn't update migration map!");
        }
        return TRUE;
      }
    }
    else {
      throw new \MigrateException("No hashsum can be obtained for file $uri.  Cannot import");
    }
    $finfo = new \finfo(FILEINFO_MIME);
    $type = array_shift(explode('/', $finfo->file($case_corrected_filesystem_uri)));
    if (!in_array($type, self::$allowed_types)) {
      $type = 'file';
    }
    $valid_editorial_sections = nnlm_core_get_editorial_sections(array('tid' => TRUE));

    // field_section will be something like 'pnr', 'psr', 'scr'
    $field_section = (explode('/', str_replace('http://nnlm.gov/', '', $lower_url)));
    if (is_array($field_section)) {
      $field_section = reset($field_section);
    }
    if (!array_key_exists($field_section, $valid_editorial_sections)) {
      U::log($uri, "Section $field_section does not exist (using all_regions default)", 'warning');
      $field_section = 'all_regions';
    }
    $handle = fopen($case_corrected_filesystem_uri, 'r');
    if (!$handle) {
      throw new \MigrateException("Could not open handle to file $case_corrected_filesystem_uri.");
    }
    $file = file_save_data($handle, $file_uri);
    fclose($handle);
    if (empty($file)) {
      throw new \MigrateException("Could not save file date from file $case_corrected_filesystem_uri.");
    }
    U::dump($file->fid, "New managed file created from uri $case_corrected_filesystem_uri.  Fid");

    $atom = U::create_scald_atom($file);
    if (!$atom) {
      throw new \MigrateException("Could not create scald atom from file $case_corrected_filesystem_uri.");
    }
    U::log($atom, "Created new atom with id $atom->sid", 'notice');
    $this->updateMapEntry($uri, array(
    'scald_id' => $atom->sid,
    'file_md5' => $md5_value,
    'drupal_node_id' => $file->fid,
      ));
    return TRUE;
  }

  /**
   * Updates any missing scald entry, if possible.
   *
   * @param string $uri
   *   The uri of the media item..
   *
   * @return boolean      the atom if the item could be updated with a current
   *                           scald atom id, FALSE otherwise
   */
  //TODO: Fix
  public function verifyScaldEntry($uri) {
    $uri = strtolower($uri);
    $entry = &$this->map[$uri];
    if (empty($entry)) {
      return FALSE;
    }
    $data = array();
    if (empty($entry->scald_id)) {
      if (isset($entry->drupal_node_id)) {
        $atom = scald_search(array('base_id' => $entry->drupal_node_id), FALSE, TRUE);
        if (!empty($atom)) {
          $entry->scald_id = $atom->sid;
          return $atom;
        }
      }
      else {
        return FALSE;
      }
    }
    $atom = scald_atom_load($entry->scald_id);
    if (!empty($atom)) {
      // U::dump($atom, "verified scald atom ");
      return $atom;
    }
    return FALSE;
  }

  /**
   * Updates a map entry to include the new values.
   *
   * @param string $uri
   *   The uri index of the entry.  Case insensitive
   * @param array $valuesAssociative
   *   array of the k/v pairs to update
   *   Associative array of the k/v pairs to update
   *
   * @return bool         TRUE if the item was found and updated, FALSE otherwise
   */
  public function updateMapEntry($uri, $values = array()) {

    // TODO: add value checking.
    $entry = $this->getMapEntry($uri);
    if (!$entry) {
      U::dump($values, "Adding new map entry.  Values: ");
      $entry = $this->addMapEntry($values, TRUE);
      return $entry;
    }
    $dirty = FALSE;
    $index = NULL;
    foreach ($values as $k => $v) {
      if (empty($v)) {
        if (!empty($entry->{$k})) {
          $dirty = TRUE;
          if(isset($this->indices[$k])){
            if($index === NULL){
              $index = $this->indices[$k][$entry->{$k}];
            }
            unset($this->indices[$k][$entry->{$k}]);
          }
        }
        $entry->{$k} = NULL;
        continue;
      }
      if (!isset($entry->{$k}) || $entry->{$k} !== $v) {
        $entry->{$k} = $v;
        if(isset($this->indices[$k])){
          if($index !== NULL){
            $this->indices[$k][$entry->{$k}] = $index;
          }
        }
      }
    }
    //Update the actual map entry (we've not been working by reference, and
    //have been modifying a copy)
    if($index !== NULL){
      $real_entry =& $this->map[$index];
      foreach($entry as $k=>$v){
        $real_entry->{$k} = $v;
      }
    }
    if ($dirty) {
      $this->updateMigrationMap($entry);
    }
    return $entry;
  }
  /**
   * Confirms all the migration map entries match what's in the current
   *  Drupal instance.
   * @return BOOL false if matching fails, true otherwise
   */
  protected function verifyMigrationMap(){
    global $databases;
    $result = db_select('filehash', 'fh')
      ->fields('fh', array('fid', 'md5'))
      ->execute();
    // $migration_db = $databases['migration_map']['default']['database'];
    // $drupal_db = $databases['default']['default']['database'];
    foreach($result as $row){
      $entry = $this->findMapEntry(array('file_md5'=>$row->md5));
      if(empty($entry)){
        U::dump($row);
        U::dump("media item for md5 value $row->md5 not found in migration map", 'Warning', 'warning');
        continue;
      }
      $expected_atom_sid = scald_search(array('base_id' => $row->fid), FALSE, TRUE);
      if(empty($expected_atom_sid)){
        U::dump($row);
        U::dump("No atom found for base id $row->fid", 'Warning', 'warning');
        continue;
      }
      if($entry->scald_id != $expected_atom_sid){
        U::dump($row);
        U::dump("Mismatch on atom ids", 'Error', 'error');
        //TODO: update map to correct value
        return FALSE;
      }
    }
    return TRUE;
  }
  /**
   * Performs a verification of all hash sums against the scald ids, correcting
   * the migration map in case a discrepancy is found.
   *
   * @return [type] [description]
   */
  protected function verifyHashsums() {
    global $databases;
    $connection = new \PDO('mysql:host=' . $databases['default']['default']['host'] . ';dbname=' . $databases['default']['default']['database'], $databases['default']['default']['username'], $databases['default']['default']['password']);
    $update_connection = \Database::getConnection('default', 'migration_map');
    if (!$connection || !$update_connection) {
      throw new \MigrateException("Could not connect to db: " . print_r($connection->errorInfo(), TRUE));
    }
    $migration_db = $databases['migration_map']['default']['database'];
    $drupal_db = $databases['default']['default']['database'];
    $execution_count = 0;
    $stmt = $connection->prepare("SELECT m.loc, fh.fid, fh.md5
          FROM $migration_db.migration_map m
          INNER JOIN $drupal_db.filehash fh
          ON (fh.md5 = m.file_md5)
          WHERE m.file_md5 = :file_md5");
    if (!$stmt) {
      throw new \Exception("Could not create statement");
    }
    foreach ($this->map as $loc => & $entry) {
      if ($entry->file_md5 != '7db2388aff3f32ffe6ca8df844681dd6') {
        continue;
      }
      $stmt->bindParam(":file_md5", $entry->file_md5);
      $stmt->execute();
      $hashfile_result = $stmt->fetchAll(\PDO::FETCH_OBJ);
      $stmt->closeCursor();
      U::dump($entry, "Map entry");
      foreach ($hashfile_result as $hfr) {

        U::dump($hfr, "HFR");
        foreach ($hfr as $prop => $value) {
          if (isset(self::$field_map[$prop])) {
            $prop = self::$field_map[$prop];
          }
          if ($prop === 'loc') {
            // We don't care about locations.  We know there are
            // multiple copies.  What really matters is that all
            // md5 sums and all scald ids are the same.
            continue;
          }
          if (!isset($entry->{$prop}) || empty($entry->{$prop})) {
            // U::dump($hfr, "HFR");
            // U::dump($entry, "Map entry");
            $entry->{$prop} = $value;
            U::dump("Correcting migration map value for property $prop.  Old value: empty, new value: $value");

            $num_updated = $connection->update('migration_map')->fields(array(
              "$prop" => $value,
            ))->condition("file_md5", $hfr->md5, '=')->execute();
            U::dump($num_updated, "Records updated");

          }
          if ($entry->{$prop} != $value) {
            if ($prop === 'drupal_node_id') {
              // This means we have two copies of the file in production. This
              // needs to be fixed, but not right now.
              U::dump($entry->loc, "Duplicate copies of file $value were found in the Drupal database", 'error');
              continue;
            }
            U::dump($hfr, "HFR");
            U::dump($entry, "Map entry");
            throw new \Exception("Property $prop has mismatched values.  Map value: " . $entry->prop . ", hfr value: " . $value);

          }
        }
      }
      if (empty($entry->scald_id)) {
        continue;
      }
      $atom = scald_atom_load($entry->scald_id);
      if (empty($atom)) {
        throw new \Exception("No scald entry found for media item $loc");
      }
      // U::dump($atom, "Atom");
      // U::dump($loc, "Checking atom for loc");
      if (!isset($atom->md5)) {
        // U::dump($atom, "Atom");
        U::dump("No md5 value present on atom.  This is an error, attempting to correct");
        $stmt->bindParam(":file_md5", $entry->file_md5);
        $stmt->execute();
        $hashfile_result = $stmt->fetchAll(\PDO::FETCH_OBJ);
        $stmt->closeCursor();

        foreach ($hashfile_result as $hfr) {
          //U::dump($hfr, "HFR");
          $corrected_atom_sid = scald_search(array('base_id' => $hfr->fid), FALSE, TRUE);
          //U::dump($corrected_atom_sid, "Correction search results");
          $entry->scald_id = $corrected_atom_sid;
          U::dump($corrected_atom_sid, "Correcting atom sid from $atom->sid to ");
          $num_updated = $update_connection->update('migration_map')->fields(array(
              "scald_id" => $corrected_atom_sid,
            ))->condition("file_md5", $hfr->md5, '=')->execute();
          U::dump($num_updated, "Records updated");
          break;
          // Only care about one result.
        }

        // if(empty($corrected_atom_sid)){
        //   throw new \Execption("An atom we thought was there simply isn't present");
        // }
        // $entry->scald_id = $corrected_atom_sid;
        // U::dump($corrected_atom_sid, "Correcting atom sid from $atom->sid to ");
        // $num_updated = $update_connection->update('migration_map')->fields(array(
        //       "scald_id" => $corrected_atom_sid,
        //     ))->condition("file_md5", $entry->file_md5, '=')->execute();
        // U::dump($num_updated, "Records updated");
      }
      // If ($atom->base_id != $entry->drupal_node_id) {
      //   //map and atom point to different files, correct.
      // if (!empty($atom)) {      //     $entry->scald_id = $atom->sid;
      //     return $atom;
      //   }
      //   U::dump($entry, "Entry");
      //   U::dump($atom, "Atom");
      //   throw new \Exception("Mismatch found for atom");
      // }
    }

  }
  /**
   * Builds an initial copy of the map.
   *
   * @return [type] [description]
   */
  protected function buildMap() {
    global $databases;
    U::dump($databases['migration_map']['default']['database'], "Using database");
    U::dump("Generating media map (intensive operation)...");
    $this->map = array();
    $hash_query = \Database::getConnection('default', 'migration_map');
    if (!$hash_query) {
      throw new Exception("Could not establish db connection");
    }
    $hash_query = $hash_query->select('migration_map', 'm');
    $hash_query->fields('m', array('file_md5', 'id', 'drupal_node_id', 'loc', 'scald_id',
      ))->distinct();
    $hash_query->condition('content_type', 'Media Item', '=');
    $hash_query->isNotNull('m.file_md5');
    $hash_query->condition('m.file_md5', '0', '<>');

    // Ignoring the migrate condition for media items.  It's never updated enough.
    // $hash_query->condition('migrate', 'y', '=');
    $hash_query->orderBy('m.loc');
    $query_result = $hash_query->execute();
    // print_r($hash_query->__toString());
    // print_r($hash_query->arguments());
    // Index both by loc and by md5 sum.  Use references for when
    // multiple locations point to an identical file.
    $id_list = array();
    $count = 0;
    foreach ($query_result as $record) {
      $count++;
      if ($record->id == '11334') {
        U::dump($record, "Test record");
      }
      $existing_entry = FALSE;
      foreach($this->indices as $k=>$arr){
        if(isset($this->indices[$k][$record->{$k}])){
          $existing_entry = $this->indices[$k][$record->{$k}];
          break;
        }
      }
      // Ensure the item exists, mapped both by location and by md5 sum.
      if ($existing_entry === FALSE) {
        $existing_entry = $this->addMapEntry(array(
          'id' => $record->id,
          'drupal_node_id' => $record->drupal_node_id,
          'file_md5' => $record->file_md5,
          'loc' => strtolower($record->loc),
          'scald_id' => $record->scald_id,
          ));
      }
    }
    unset($hash_query);
    //U::dump($this->map, "Media Map");
    U::dump($count, "Map entry count");
    cache_set('nnlm_migration_hash_map', $this->map, 'cache', time() + MediaMap::MEDIA_MAP_CACHE_LIFETIME);
    // Correct entries where the migration map scald id points to the wrong
    // file.  MUST use filehash Drupal instance, as the map cannot be trusted in this regard.
    if (U::confirm("Perform hashsum comparison and repair?")) {
      $this->verifyMigrationMap();
    }
    // Populate items with scald ids where present.  Use PDO directly, as
    // Drupal does not support cross-database joins.
    cache_set('nnlm_migration_hash_map', $this->map, 'cache', time() + MediaMap::MEDIA_MAP_CACHE_LIFETIME);
  }

}
