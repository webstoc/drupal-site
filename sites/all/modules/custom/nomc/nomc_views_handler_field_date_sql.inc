<?php
// $Id$


/**
 * This handler is necessary because views_handler_field_date apparently
 * expects a unix timestamp, and I'm feeding it SQL date strings.  This
 * handler does the conversion, and calls its parent.
 *
 * @ingroup views_field_handlers
 */
class nomc_views_handler_field_date_sql extends views_handler_field_date {
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['date_format']['#options']['sql'] = t('SQL Date Format');
  }

  function render($values) {
    $alias = isset($field) ? $this->aliases[$field] : $this->field_alias;
    if (!isset($values->{$alias})) {
      //I don't know the conditions under which this might occur,
      //so I'm leaving it live for now.
      throw new Exception("Alias not set");
    }
    // YYYY-MM-DD format, or NULL
    $date = date_create($values->{$alias});
    if ($date === FALSE) {
      return FALSE;
    }
    $values->{$alias} = $date->format('U');
    return parent::render($values);
  }
}

