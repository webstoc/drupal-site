<?php
/**
 * @file
 * course_home_page.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function course_home_page_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_administrative|node|course_home_page|form';
  $field_group->group_name = 'group_administrative';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'course_home_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Administrative',
    'weight' => '4',
    'children' => array(
      0 => 'field_migration_errors',
      1 => 'field_migration_notes',
      2 => 'field_origin_url',
      3 => 'field_section',
      4 => 'field_tags',
      5 => 'path',
      6 => 'redirect',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-administrative field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_administrative|node|course_home_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_course_home_page_fields|node|course_home_page|form';
  $field_group->group_name = 'group_course_home_page_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'course_home_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Course Specific Fields',
    'weight' => '2',
    'children' => array(
      0 => 'field_course_developer',
      1 => 'field_internal_course_materials',
      2 => 'field_external_course_materials',
      3 => 'field_mla_clearinghouse_entry',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Course Specific Fields',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-course-home-page-fields field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_course_home_page_fields|node|course_home_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_course_info|node|course_home_page|default';
  $field_group->group_name = 'group_course_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'course_home_page';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '1',
    'children' => array(
      0 => 'group_course_materials',
      1 => 'group_course_summary',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-course-info',
        'wrapper' => 'section',
        'description' => '',
      ),
      'formatter' => '',
    ),
  );
  $export['group_course_info|node|course_home_page|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_course_materials|node|course_home_page|default';
  $field_group->group_name = 'group_course_materials';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'course_home_page';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_course_info';
  $field_group->data = array(
    'label' => 'Course Materials',
    'weight' => '9',
    'children' => array(
      0 => 'field_external_course_materials',
      1 => 'field_internal_course_materials',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => '',
        'id' => '',
      ),
    ),
  );
  $export['group_course_materials|node|course_home_page|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_course_summary|node|course_home_page|default';
  $field_group->group_name = 'group_course_summary';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'course_home_page';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_course_info';
  $field_group->data = array(
    'label' => 'Course Summary',
    'weight' => '7',
    'children' => array(
      0 => 'field_mla_clearinghouse_entry',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => '',
        'id' => '',
      ),
    ),
  );
  $export['group_course_summary|node|course_home_page|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_customization|node|course_home_page|form';
  $field_group->group_name = 'group_customization';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'course_home_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Customization',
    'weight' => '3',
    'children' => array(
      0 => 'field_node_scripts',
      1 => 'field_node_styles',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-customization field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_customization|node|course_home_page|form'] = $field_group;

  return $export;
}
