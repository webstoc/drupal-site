<?php
/**
 * @file
 * feature_nnlm_content_types.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function feature_nnlm_content_types_user_default_permissions() {
  $permissions = array();

  // Exported permission: access admin theme.
  $permissions['access admin theme'] = array(
    'name' => 'access admin theme',
    'roles' => array(
      'content creator' => 'content creator',
      'content moderator' => 'content moderator',
    ),
    'module' => 'admin_theme',
  );

  // Exported permission: access administration menu.
  $permissions['access administration menu'] = array(
    'name' => 'access administration menu',
    'roles' => array(
      'content creator' => 'content creator',
      'content moderator' => 'content moderator',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: access ckeditor link.
  $permissions['access ckeditor link'] = array(
    'name' => 'access ckeditor link',
    'roles' => array(
      'content creator' => 'content creator',
      'content moderator' => 'content moderator',
    ),
    'module' => 'ckeditor_link',
  );

  // Exported permission: administer ckeditor.
  $permissions['administer ckeditor'] = array(
    'name' => 'administer ckeditor',
    'roles' => array(),
    'module' => 'ckeditor',
  );

  // Exported permission: administer ckeditor link.
  $permissions['administer ckeditor link'] = array(
    'name' => 'administer ckeditor link',
    'roles' => array(),
    'module' => 'ckeditor_link',
  );

  // Exported permission: customize ckeditor.
  $permissions['customize ckeditor'] = array(
    'name' => 'customize ckeditor',
    'roles' => array(),
    'module' => 'ckeditor',
  );

  return $permissions;
}
