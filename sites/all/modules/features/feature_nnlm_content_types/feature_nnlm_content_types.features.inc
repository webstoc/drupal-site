<?php
/**
 * @file
 * feature_nnlm_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_nnlm_content_types_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function feature_nnlm_content_types_image_default_styles() {
  $styles = array();

  // Exported image style: icon.
  $styles['icon'] = array(
    'name' => 'icon',
    'effects' => array(
      17 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 32,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
      19 => array(
        'label' => 'Crop',
        'help' => 'Cropping will remove portions of an image to make it the specified dimensions.',
        'effect callback' => 'image_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_crop_form',
        'summary theme' => 'image_crop_summary',
        'module' => 'image',
        'name' => 'image_crop',
        'data' => array(
          'width' => 32,
          'height' => 32,
          'anchor' => 'center-center',
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function feature_nnlm_content_types_node_info() {
  $items = array(
    'basic_node' => array(
      'name' => t('Basic node'),
      'base' => 'node_content',
      'description' => t('Basic nodes comprise the majority of the content on nnlm.gov.  A basic node is static content; not often subject to change, and unlikely to repeat.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Basic nodes comprise the majority of the content on nnlm.gov.  A basic node is static content; not often subject to change, and unlikely to repeat.'),
    ),
    'funding_award' => array(
      'name' => t('Funding Award'),
      'base' => 'node_content',
      'description' => t('This content type is meant to cover pages that describe funding awards.  It is a separate content type to allow easier grouping, and for the custom fields that funding awards contain that may be useful to sort by in lists.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('This content type is meant to cover pages that describe funding awards.  It is a separate content type to allow easier grouping, and for the custom fields that funding awards contain that may be useful to sort by in lists.'),
    ),
    'nodeblock' => array(
      'name' => t('NodeBlock'),
      'base' => 'node_content',
      'description' => t('Small bits of content that can be placed in landing page sections, or in page sidebars (with assistance from WebSTOC on the latter)'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('A nodeblock is designed to be content that lives in the sidebar or footer of your site.  Currently, it is simple snippets of html; no widgets or javascript (these are better done with custom modules. WebSTOC is still investigating the best method of offering "widget" content in drupal).
An example would be the "Quick links" section of a RML homepage, or perhaps the funding information block in the RML\'s footer.'),
    ),
    'vsx_slide' => array(
      'name' => t('Slide'),
      'base' => 'node_content',
      'description' => t('Used for making Slideshow slides.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('A slideshow on Drupal is a custom view that rotates through Slide nodes.  By creating a Slide node here, and adding the appropriate tags to the slide, it will automatically be added to the rotation of any page featuring a slideshow set to display slides with the same tag.'),
    ),
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Create a new form or questionnaire. Submission results and statistics are recorded and accessible to privileged users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Webforms are suitable for forms where the interaction is very simple - "Contact Us" forms would be a good example, or simple surveys.  More advanced usage (such as NLM promotional product display pages) is being implemented via the Commerce module.'),
    ),
  );
  return $items;
}
