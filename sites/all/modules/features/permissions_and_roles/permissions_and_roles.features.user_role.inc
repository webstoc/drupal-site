<?php
/**
 * @file
 * permissions_and_roles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function permissions_and_roles_user_default_roles() {
  $roles = array();

  // Exported role: content creator.
  $roles['content creator'] = array(
    'name' => 'content creator',
    'weight' => 3,
  );

  // Exported role: content moderator.
  $roles['content moderator'] = array(
    'name' => 'content moderator',
    'weight' => 2,
  );

  // Exported role: drupal administrator.
  $roles['drupal administrator'] = array(
    'name' => 'drupal administrator',
    'weight' => 0,
  );

  return $roles;
}
