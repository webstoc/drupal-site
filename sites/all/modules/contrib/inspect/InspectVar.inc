<?php
/**
 * @file
 * Drupal Inspect module variable inspection class
 */

/**
 * Variable inspector
 * @static
 * @singleton
 * @category Drupal
 * @package Developer
 * @subpackage Inspect */
final class InspectVar extends InspectBs {
  /**
   * Prevents erratic instantiation of this entirely static class
   */
  private function  __construct() {}

  /**
   * Inspect variable and log to database (watchdog), if permitted.
   *
   *   Like PHP native var_dump()/var_export() except:
   *   - is controlled by permissions
   *   - has much more options, and far better formatting (JSONesque)
   *   - doesnt fail on inspecting $GLOBALS
   *   - aborts if producing too large output, or taking too much time
   *   - doesnt echo, logs to database log
   *
   *   Integer value of $options (depth):
   *   - the value controls how deeply the variable is going to be analyzed
   *   - default: 10 (InspectBs::DEPTH_DEFAULT)
   *   - max: 10 (InspectBs::DEPTH_MAX)
   *
   *   Array/object value of $options (any number of options):
   *   - (integer) depth (default 10, max 10 ~ InspectBs::DEPTH_DEFAULT|DEPTH_MAX)
   *   - (integer) truncate (default 1000, max 10000 ~ InspectBs::TRUNCATE_MIN|TRUNCATE_DEFAULT)
   *   - (bool) hide_scalars: hide values of scalar vars, and type of resource (also supported as value 'hide_scalars')
   *   - (bool) hide_paths: only relevant for log (paths always hidden for get) (also supported as value 'hide_paths')
   *   - (string) name: '$var_name' or "\$var_name", must be escaped, will be truncated to 255 (default empty, use 'none' to omit)
   *   - (string) message, will be truncated to 255 (default empty)
   *   - (string) category: logging category (default inspect)
   *   - (integer) severity: default WATCHDOG_DEBUG
   *   - (string|array) filter: filter out that|those key name(s) (default empty)
   *   - (array) needles: list of char|string needles to be used in str_replace (default newline carriage return < > " ')
   *   - (array) replacers: list of char|string replacers to be used in str_replace (default equivalent xml entities)
   *   - (boolean) one_lined: create one-lined formatting (getting only, and also supported as value 'one_lined')
   *   - (boolean) no_folding: dont make output expansible/collapsible (also supported as value 'no_folding')
   *   - (boolean) file_line: display file and line of call to inspect function/method (default do)
   *
   *   String truncation:
   *   - truncate option less than one (hide strings) doesnt apply to strings that solely consist of digits (stringed integers)
   *   - a string bucket of an object/array keyed pass or password (lowercase) will always be hidden (no matter it's content)
   *
   *   Removes paths at the beginning of variables:
   *   - always removes document root
   *   - always removes relative and absolute paths when getting (optional when logging)
   *
   *   Reports non-integer numbers:
   *   - double as float
   *   - NaN as NaN and infinite as infinite
   *
   *   Reports string lengths and path removal (path reduced to filename):
   *   - un-truncated string, and when hiding scalar values: (multibyte length|ascii length)
   *   - truncated string: (multibyte length|ascii length|truncation length)
   *   - if path removed: there will be a fourth flag (exclamation mark), if no truncation truncation length will be hyphen
   *
   *   Recursive:
   *   - detects if a bucket of an object references the object itself (marked *RECURSION*)
   *   - detects GLOBALS array's self-referencing GLOBALS bucket (marked *RECURSION*)
   *   - doesnt check identity for arrays (not possible in PHP), nor 'same-ness' (too heavy, and prone to err)
   *
   * Executes variable analysis within try-catch.
   *
   * @param mixed $var
   * @param mixed $options
   *   - default: NULL
   *   - array|object: list of options
   *   - integer: interprets to suggested depth
   *   - string: interprets to message
   *   - other type: ignored
   * @return bool|NULL
   *   - NULL if user isnt permitted to log inspections
   *   - FALSE on error (other error than exceeding max. output length)
   */
  public static function log($var, $options = NULL) {
    if (!($opts = self::_checkPermissionsAndOptions('log', $options)) ) {
      return NULL;
    }
    $nl = $opts['newline_log'];
    try {
      self::$_outputMax = self::$_outputMax_log;
      self::$_outputLength = 0;
      $nspct = self::_inspect($var, $opts);
    }
    catch (Exception $xc) {
      switch ((int)$xc->getCode()) {
        case 100: // Output length exceeding maximum.
          $nspct = $xc->getMessage();
          $opts['severity'] = WATCHDOG_WARNING;
          //  Try again?
          $t = $opts['truncate'];
          if (($d = $opts['depth']) > 2 || $t > 100) {
            $opts['depth'] = $d > 2 ? ceil($d / 2) : $d;
            $opts['truncate'] = $t > 100 ? ceil($t / 2) : $t;
            $nspct .= $nl . 'Trying again, using options depth[' . $opts['depth'] . '] and truncate[' . $opts['truncate'] . '].';
            try {
              self::$_outputLength = 0;
              $nspct .= $nl . self::_inspect($var, $opts);
            }
            catch (Exception $xc1) {
              if ($xc->getCode() != 100) { // 100 ~ Output length exceeding maximum.
                return '';
              }
              $nspct .= $nl . $xc1->getMessage();
            }
          }
          break;
        case 101: // Max percent of max_execution_time passed.
          $nspct = $xc->getMessage();
          $opts['severity'] = WATCHDOG_WARNING;
          break;
        default:
          return FALSE;
      }
    }
    $tagStart = $tagEnd = '';
    if (($u = $opts['enclose_tag_log'])) {
      $tagStart = '<' . $u . (empty($opts['no_folding']) ? ' class="module-inspect-collapsible"' : '') . '>';
      $tagEnd = '</' . $u . '>';
    }
    return self::log_db(
        $tagStart
          . ( empty($opts['message']) ? '' : ($opts['message'] . ':' . $nl) )
          . ( empty($opts['no_preface']) ? ('[Inspect - ' . self::$_sessionNo . ':' . self::$_requestNo . ':' . (++self::$_logNo)
              . ' - depth:' . $opts['depth'] . ']') : '')
          . ( empty($opts['name']) || $opts['name'] == 'none' ? '' : (' ' . $opts['name']) ) . $nl
          . ( empty($opts['file_line']) ? '' : (self::_fileLine(debug_backtrace(), $opts['hide_paths']) . $nl) )
          . $nspct
          . $tagEnd,
        $opts['category'],
        array_key_exists('severity', $opts) ? $opts['severity'] : -1
    );
  }

  /**
   * Inspect variable and get the output as string, if permitted.
   *
   *   Behaviour and arguments like InspectVar::log(), except for $options:
   *   - enclose_tag_get instead of enclose_tag_log
   *   - newline_get instead of newline_log
   *
   * @param mixed $var
   * @param mixed $options
   *   - default: NULL
   *   - array|object: list of options
   *   - integer: interprets to suggested depth
   *   - string: interprets to message
   *   - other type: ignored
   * @return string
   *   - empty if user isnt permitted to get inspections, or on error (other error than exceeding max. output length)
   */
  public static function get($var, $options = NULL) {
    if (!($opts = self::_checkPermissionsAndOptions('get', $options)) ) {
      return '';
    }
    $nl = $opts['newline_get'];
    try {
      self::$_outputMax = self::$_outputMax_get;
      self::$_outputLength = 0;
      $nspct = self::_inspect($var, $opts);
    }
    catch (Exception $xc) {
      switch ((int)$xc->getCode()) {
        case 100: // Output length exceeding maximum.
          $nspct = $xc->getMessage();
          //  Try again?
          $t = $opts['truncate'];
          if (($d = $opts['depth']) > 2 || $t > 100) {
            $opts['depth'] = $d > 2 ? ceil($d / 2) : $d;
            $opts['truncate'] = $t > 100 ? ceil($t / 2) : $t;
            $nspct .= $nl . 'Trying again, using options depth[' . $opts['depth'] . '] and truncate[' . $opts['truncate'] . '].';
            try {
              self::$_outputLength = 0;
              $nspct .= $nl . self::_inspect($var, $opts);
            }
            catch (Exception $xc1) {
              if ($xc->getCode() != 100) { // 100 ~ Output length exceeding maximum.
                return '';
              }
              $nspct .= $nl . $xc1->getMessage();
            }
          }
          break;
        case 101: // Max percent of max_execution_time passed.
          $nspct = $xc->getMessage();
          break;
        default:
          return FALSE;
      }
    }
    $tagStart = $tagEnd = '';
    if (($u = $opts['enclose_tag_get'])) {
      $tagStart = '<' . $u . (empty($opts['no_folding']) ? ' class="module-inspect-collapsible"' : '') . '>';
      $tagEnd = '</' . $u . '>';
    }
    return $tagStart
        . ( empty($opts['message']) ? '' : ($opts['message'] . ':' . $nl) )
        . ( empty($opts['no_preface']) ? ('[Inspect - depth:' . $opts['depth'] . ']') : '')
        . ( empty($opts['name']) || $opts['name'] == 'none' ? '' : (' ' . $opts['name']) ) . $nl
        . ( empty($opts['file_line']) ? '' : (self::_fileLine(debug_backtrace(), TRUE) . $nl) )
        . $nspct . $tagEnd;
  }

  /**
   * Obsolete function since 7.x-2.5, logs trace as error to database log (if permitted) - and the function will be removed some day.
   *
   * @deprecated
   * @param mixed $var
   * @param mixed $options
   *   - default: NULL
   *   - array|object: list of options
   *   - integer: interprets to suggested depth
   *   - string: interprets to message
   *   - other type: ignored
   * @return string
   *   - empty
   */
  public static function log_get($var, $options = NULL) {
    //  @todo: remove this method from some version > 7.x-2.5, obsolete since that version. Remove when few use <2.5.
    InspectTrc::log(
        NULL,
        array('severity' => WATCHDOG_ERROR, 'message' =>
          'InspectVar::log_get() is obsolete since Inspect v. 7.x-2.5, and the method will be removed some day'
        )
    );
    self::log($var, $options);
    return '';
  }


  /**
   * Inspect arguments of surrounding function/method and log to database (watchdog), if permitted.
   *
   * $options like InspectVar::log().
   *
   * @param mixed $options
   *   - default: NULL
   *   - array|object: list of options
   *   - integer: interprets to suggested depth
   *   - string: interprets to message
   *   - other type: ignored
   * @return bool|NULL
   *   - NULL if user isnt permitted to log inspections
   *   - FALSE on error (other error than exceeding max. output length)
   */
  public static function args($options = NULL, $_get = FALSE) {
    $trc = debug_backtrace();
    $frame = NULL;
    for ($i = 0; $i < 4; $i++) {
      if (isset($trc[$i]['class']) && strlen($s = $trc[$i]['class'])) {
        if (strpos($s, 'Inspect') === 0) {
          continue;
        }
      }
      elseif (isset($trc[$i]['function']) && strlen($s = $trc[$i]['function'])
            && ($s == 'inspect' || strpos($s, 'inspect_') === 0)) {
        continue;
      }
      if (isset($trc[$i]['args'])) {
        $frame = $trc[$i];
        break;
      }
    }
    if ($frame) {
      if ($options === 'one_lined') {
        $options = array('one_lined' => TRUE);
      }
      //  Set options category and name, if empty.
      $opts = self::_resolveOptions($options);
      if (empty($opts['category'])) {
        $opts['category'] = 'inspect args';
      }
      if (empty($opts['name'])) {
        $opts['name'] = (empty($frame['class']) ? '' : ($frame['class'] . (!empty($frame['type']) ? $frame['type'] : '::') ) )
              . (!empty($frame['function']) ? ($frame['function'] . '()') : '' );
      }
      if (!$_get) {
        return self::log($frame['args'], $opts);
      }
      //  If get is one_lined, we want to get rid of the 'array'
      if (($s = self::get($frame['args'], $opts))
          && !empty($opts['one_lined'])) {
        return preg_replace('/(\(\)\s)?\(array\:/', '(', $s, 1);
      }
      return $s;
    }
    return !$_get ? FALSE : '';
  }

  /**
   * Inspect arguments of surrounding function/method and get the output as string, if permitted.
   *
   * Usable for error messages.
   *
   * $options like InspectVar::get().
   *
   * @param mixed $var
   * @param mixed $options
   *   - default: 'one_lined' (~ create short single lined format usable for error messages, using depth:1)
   *   - array|object: list of options
   *   - integer: interprets to suggested depth
   *   - string: interprets to message (except for 'one_lined')
   *   - other type: ignored
   * @return string
   *   - empty if user isnt permitted to get inspections, or on error (other error than exceeding max. output length)
   */
  public static function args_get($options = 'one_lined') {
    return self::args($options, TRUE);
  }

  /**
   * Inspect constants and log to database (watchdog), if permitted.
   *
   *   $options like InspectVar::log(), except:
   *   - (bool) sort: if truthy, sorts constants by name (except if inspecting all defined constants, then only category is sorted)
   *
   * @see InspectVar::log()
   * @param string|object|falsy $cat_or_instance_or_classname
   *   - default: 'user' (~ the user category of defined constants)
   *   - falsy: all defined constants, listed by category
   *   - 'class SomeClassName': class constants of class SomeClassName
   *   - other strings: that category of defined constants
   *   - object: class constants of that object's class
   * @param mixed $options
   *   - default: NULL
   *   - array|object: list of options
   *   - string: interprets to message
   *   - boolean: sort constants
   *   - other type: ignored
   * @return bool|NULL|string
   *   - NULL if user isnt permitted to log inspections
   *   - FALSE on error (other error than exceeding max. output length)
   *   - string if truthy arg $_get
   */
  public static function constants($cat_or_instance_or_classname = 'user', $options = NULL, $_get = FALSE) {
    $sort = FALSE;
    if (($t = gettype($options)) == 'object') {
      $opts = get_object_vars($options);
      $t = 'array';
    }
    else {
      $opts = $options;
    }
    switch ($t) {
      case 'array':
        if(!empty($opts['sort'])) {
          $sort = TRUE;
          unset($opts['sort']);
        }
        break;
      case 'string':
        $opts = array('message' => $opts);
        break;
      case 'boolean':
        $sort = $opts;
        $opts = array();
        break;
      default:
        $opts = array();
    }
    if (!($a = self::_constants($cat_or_instance_or_classname, $sort))) {
      return !$_get ? FALSE : '';
    }
    $opts['name'] = $a[1];
    return !$_get ? self::log($a[0], $opts) : self::get($a[0], $opts);
  }

  /**
   * Inspect constants and get the output as string, if permitted.
   *
   *   Behaviour and arguments like InspectVar::constants(), except for $options:
   *   - enclose_tag_get instead of enclose_tag_log
   *   - newline_get instead of newline_log
   *
   * @see InspectVar::get()
   * @param string|object|falsy $cat_or_instance_or_classname
   *   - default: 'user' (~ the user category of defined constants)
   *   - falsy: all defined constants, listed by category
   *   - 'class SomeClassName': class constants of class SomeClassName
   *   - other strings: that category of defined constants
   *   - object: class constants of that object's class
   * @param mixed $options
   *   - default: NULL
   *   - array|object: list of options
   *   - string: interprets to message
   *   - boolean: sort constants
   *   - other type: ignored
   * @return string
   *   - empty if user isnt permitted to get inspections, or on error (other error than exceeding max. output length)
   */
  public static function constants_get($cat_or_instance_or_classname = 'user', $options = NULL) {
    return self::constants($cat_or_instance_or_classname, $options, TRUE);
  }
}