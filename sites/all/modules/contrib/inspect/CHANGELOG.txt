inspect 7.x-2.5.x, 2012-06-15
-----------------------------
* Combined output functions/methods deprecated and logging trace as error: inspect_log_get()/InspectVar::log_get(), inspect_trace_log_get()/InspectTrc::log_get().

inspect 7.x-2.5.x, 2012-06-09
-----------------------------
* Rewritten documentation, for help and settings pages.

inspect 7.x-2.5.x, 2012-06-08
-----------------------------
* Timeout fuse (percentage of max_execution_time) implemented.

inspect 7.x-2.5.x, 2012-06-03
-----------------------------
* inspect_args_get/InspectVar::args_get now defaults to return one_lined output.
* Sorted out settings and help pages, and fixed some comments.
* Tuned profiler a bit, and created examples.

inspect 7.x-2.5.x, 2012-06-02
-----------------------------
* Options name and message are now being truncated to 255, and logging category to 64.
* Fixed inspector counting some output lengths more than once, and added check in tracer.
* Implemented auto-configuring max output lengths, and max output length for tracer and profiler.
* Frontend folding now highlights string truncation (better), object/array truncation (depth), and errors.
* Frontend folding now highlights path removal.
* Fixed backend document root algo, now supports symlinking.

inspect 7.x-2.5.x, 2012-05-27
-----------------------------
* Implemented maximum inspection character output length (1,000,000), to make inspector err gracefully for huge arrays/objects.

inspect 7.x-2.5.x, 2012-05-26
-----------------------------
* Made frontend fold() more tolerant of single-lined output (content truncation, 'Read more') may shorten output to a single line.
* Frontend fold() non-greedy when finding bucket name - otherwise breaks for string containing 'whatever: ('
* fold() now renders newlines (as _NL_newline), and highlights string truncation.
* Backend, changed default string truncation to 1000 (from 100).

inspect 7.x-2.4, 2012-05-06
-------------------------------------------------------------------------------
Released 7.x-2.4.x as 7.x-2.4.

inspect 7.x-2.4.x, 2012-05-06
-----------------------------
* Fixed bug in InspectBs::requestTimeMilli() - REQUEST_TIME was only multiplied by 100 (not 1000).

inspect 7.x-2.3, 2012-05-06
-------------------------------------------------------------------------------
Released 7.x-2.3.x as 7.x-2.3.

inspect 7.x-2.3.x, 2012-05-06
-----------------------------
* Added inspect_request_time_milli()/InspectBs::requestTimeMilli(), which uses custom Apache request header when no server var REQUEST_TIME_FLOAT.
* Removed constant MODULE_INSPECT_REQUEST_TIME_MILLI.
* Got rid of the need/usefulness of calling inspect_profile_init()/InspectProfile::init() before actual profiling.
* inspect_profile_init() and InspectProfile::init() now log a trace (error) when called.

inspect 7.x-2.2, 2012-05-05
-------------------------------------------------------------------------------
Released 7.x-2.2.x as 7.x-2.2.

inspect 7.x-2.2.x, 2012-05-05
-----------------------------
* Profile for .csv file done, but dont like it.
* Removed administrative option MS Office compatible .csv-file.
* Profiler now subtracts own time from calculations; compares againts total request time; calculates truncated means and finds longest/shortest interval.
* Parameter signature changed for inspect_profile()/InspectProfile::event(), and an error is logged when old-style argument value detected.

inspect 7.x-2.2.x, 2012-05-04
-----------------------------
* The module's hook_init() now sets constant MODULE_INSPECT_REQUEST_TIME_MILLI.
* Added administrative option MS Office compatible .csv-files.
* Changed profiler directory to private://module/inspect/profiles.

inspect 7.x-2.1, 2012-05-02
-------------------------------------------------------------------------------
Released 7.x-2.0.x as 7.x-2.1.

inspect 7.x-2.0.x, 2012-05-02
-----------------------------
* Added warning in profiler when private files directory isnt defined.

inspect 7.x-2.0, 2011-04-29
-------------------------------------------------------------------------------
Released 7.x-1.3.x as 7.x-2.0.

inspect 7.x-1.3.x, 2012-04-29
-----------------------------
* Added request method and uri to profiler log.
* Reversed backend inspection option of displaying call file and line - to something you can turn off instead on (turns out debug_backtrace footprint is negligible).
* Frontend inspection to console now includes call file and line.
* Fixed frontend trace formatting.
* Frontend inspection string qoute is now same as backend.

inspect 7.x-1.3.x, 2012-04-21
-----------------------------
* Session no now uses cookie as fallback when no session.
* Finished profiler, including frontend formatting.

inspect 7.x-1.3.x, 2012-04-21
-----------------------------
* Created profiler class.
* Added permission 'inspect profile'.
* Moved InspectBs initilization into separate method.
* Added horizontal tab to inspectors list of string replacements.

inspect 7.x-1.3.x, 2012-04-01
-----------------------------
* Added backend inspection option of displaying call file and line, and added administrative option of making that behavior default.
* Added backend functions/methods for inspecting arguments, and removed list functions/methods.
* Removed hide_paths conditionals for get in InspectTrc (because paths are always hidden for get).
* Moved hook_menu() implementation from administrative source file to .module, and set negative weight.

inspect 7.x-1.3.x, 2012-03-24
-----------------------------
* Backend: logging categories new default to inspect, inspect trace, inspect log.
* Made message default string interpretion of the options arg for simple variable inspectors (again).
* Simplified options resolving somewhat.
* Renamed InspectVar::constants_log() to InspectVar::constants().
* Added functions/methods inspect_list(), InspectVar::list_log(), inspect_list_get(), InspectVar::list_get()
* Changed the (string) delimiter option to (array) delimiters, and added options newline_log and newline_get.

inspect 7.x-1.3.x, 2012-03-22
-----------------------------
* Frontend: fixed .typeOf bug (when no argument), reduced folding overhead somewhat and set a max limit of 10000 lines for folding.

inspect 7.x-1.3.x, 2012-03-21
-----------------------------
* Backend: fixed severe character set bug (http://drupal.org/node/1493404) which sometimes resulted in PDOException when logging inspection.
* Backend: string truncation is now UTF-8 safe, and strings are also being checked for UTF-8 validity.
* Frontend: folding, line highlighted etc.

inspect 7.x-1.3.x, 2012-03-20
-----------------------------
* Frontend: better folding (no jumping, and simpler events).

inspect 7.x-1.3.x, 2012-03-17
-----------------------------
* Backend: changed interpretation of all options arguments, to support direct message arg (string) and list arg as object.
* Backend: added name option, and made that default options as string interpretion for simple variable inspectors.
* Frontend: use the name option for better key path generation.

inspect 7.x-1.3.x, 2012-03-16
-----------------------------
* Frontend: now displays key path.

inspect 7.x-1.3.x, 2012-03-07
-----------------------------
* Frontend: removed obsolete and undeclared var t in variableGet.

inspect 7.x-1.3.x, 2012-02-27
-----------------------------
* Output button now toggles display on normal page versus folding on log report details page.
* Added notices, that folding doesnt work in IE<9.
* Fixed folding bug, when message option used.

inspect 7.x-1.3.x, 2012-02-26
-----------------------------
* Removed hover title on folding buttons, but kept it on general toggling button.

inspect 7.x-1.3.x, 2012-02-25
-----------------------------
* Hover title on dump output buttons (folding).

inspect 7.x-1.3.x, 2012-02-20
-----------------------------
* Added toggle button to dump outputs, and reduced frontend toggle methods to one (toggleOutput).

inspect 7.x-1.3.x, 2012-02-16
-----------------------------
* Removed permission 'inspect log paths'. _Logging_ paths is now always allowed, but may still be prevented by using the 'hide_paths' option.
* Removed permission 'inspect log scalars'. _Logging_ scalars is now always allowed, but may still be prevented by using the 'hide_scalars' option.
* Removed permission 'inspect get scalars'. _Getting_ scalars is now always allowed, but may still be prevented by using the 'hide_scalars' option.
* Removed administrative option to omit linking dummy javascript inspector for users missing the 'inspect frontend' permission.
* Removed administrative option to use source instead of minified javascript files.
* Changed return value of getting methods to empty string, when error or missing permission. To prevent '0's in output (FALSE/NULL translates to zero when echoed).
* Moved javascript files to separate folder.
* Changed inspect_init()'s permissions conditions algo for javascript file inclusion.
* Changed default delimiter between object/array buckets from carriage return to newline. Not because it doesnt work, but CR would is only relevant for logging to csv/tsv files (which we dont do).
* Implemented frontend inspect.toggle_output() and inspect.toggle_folding().

inspect 7.x-1.3.x, 2012-02-15
-----------------------------
* Moved log session number from log start into [Inspect... block.
* Now strings are also escaped using htmlspecialchars(), aside from the needles/replacer options replacement.
* Now the message option value is escaped.
* Removed empty divs from admin page form.
* Admin pages; swapped sequences of nl2br() and t() calls, to make t() detection easier for the Localization Project.
* Adding dummy frontend inspector for user missing the permission to use frontend inspector is now an option to turn off instead of on.
* Added option to turn off making variable dumps expandable/collapsible.
* Now supports expandable/collapsible variable dumps.

inspect 7.x-1.3.x, 2012-02-01
-----------------------------
* Typo in comments.


inspect 7.x-1.2, 2011-01-28
-------------------------------------------------------------------------------
Released 7.x-1.2.x as 7.x-1.2.


inspect 7.x-1.2.x, 2012-01-28
-----------------------------
* Fixed .js compression errors.

inspect 7.x-1.2.x, 2012-01-27
-----------------------------
* Frontend: re-introduced dummy ('noaction') version of frontend inspector, to prevent errors for users missing the frontend inspection permission.
* Added configuration option: add surrogate 'noaction' frontend inspect when user misses the frontend inspection permission
* Frontend: added minified versions of the frontend scripts.
* Added configuration option: use source versions of frontend scripts instead of minified versions.
* Added .install, which removes configuration variables when un-installing.
* Added copyright notice in separate txt-file. See related discussion in groups.drupal, 'Copyright lines on source files' (http://groups.drupal.org/node/109699).

inspect 7.x-1.2.x, 2012-01-25
-----------------------------
* Frontend: fixed bug in _nspct(), which prevented the (max) depth parameter from counting down (in effect making every sub inspection run to depth 10).
* Frontend: changed parameter signatures of inspect(), .variable, .variableGet; options object instead of 3 args of fairly arbitrary sequence.


inspect 7.x-1.1, 2012-01-07
-------------------------------------------------------------------------------
Released 7.x-1.1.x as 7.x-1.1.


inspect 7.x-1.1.x, 2011-01-07
-----------------------------
* Frontend: renamed console logging function to console (instead of log).

inspect 7.x-1.1.x, 2011-01-06
-----------------------------
* Backend, implemented severity option for logging methods.

inspect 7.x-1.1.x, 2011-01-05
-----------------------------
* Frontend, fixed max depth default (10 instead of zero).
* Frontend, dumping elements now also displays name attribute (if such exists).

inspect 7.x-1.1.x, 2011-01-02
-----------------------------
* Frontend _oKeys counted keys, now collects keys.

inspect 7.x-1.1.x, 2011-12-31
-----------------------------
* Reversed changelog order.
* Frontend, implemented depth parameter, and added method argsGet().

inspect 7.x-1.1.x, 2011-12-26
-----------------------------
* Rewrote readme's installation part
* Frontend .typeOf() now returns 'inspect' when given no arguments.
* Frontend .typeOf(): fixed bad procedure when checking a subject that has a typeOf method.

inspect 7.x-1.1.x, 2011-12-17
-----------------------------
* Minor syntax changes in frontend .typeOf()


inspect 7.x-1.0, 2011-12-12
-------------------------------------------------------------------------------
Released 7.x-1.x as 7.x-1.0.


inspect 7.x-1.x, 2011-11-26
---------------------------
* Wrapped frontend singleton in function, to allow Inspect() (as well as Inspect.variable).
* Made frontend function/singleton name case-insensitive (inspect and Inspect).
* Added support for type jquery in frontend function/singleton.

inspect 7.x-1.x, 2011-11-11
---------------------------
* Fixed wrong image detection in frontend typoOf()
* Frontend typoOf(), fixed missing detection of RegExp when browser actually sees a RegExp as Object (and not Function)

inspect 7.x-1.x, 2011-11-07
---------------------------
* Added Date type recognition to frontend typeOf method

inspect 7.x-1.x, 2011-11-06
---------------------------
* Session number makeover, now uses dedicated number instead of hashing global session id
* Fixed entirely wrong InspectTrc::log_get.
* Fixed error in InspectTrc formatting, just another remnant from module renaming :-(
* Fixed top frame removal for InspectTrc.

inspect 7.x-1.x, 2011-11-05
---------------------------
* Fixed fatal typo (introduced when renaming from dump to inspect)

inspect 7.x-1.x, 2011-10-21
---------------------------
* Changed version number again, according to Drupal conventions
* Fixed various formatting issues

inspect 7.x-1.2, 2011-10-21
---------------------------
* Changed version number according to Drupal conventions

inspect 7.x-0.9.14, 2011-10-17
------------------------------
* Removed breaks and non-breaking spaces from admin form output.
* Removed version number in .info.
* Removed copyright and license comments.
* Removed LICENSE.txt
* Rewrote the Hooks and Extendability chapter in the readme, to make it more polite.
* Added stylesheet to admin help page.

inspect 7.x-0.9.13, 2011-10-15
------------------------------
* Drupalized code formatting.

inspect 7.x-0.9.12, 2011-10-15
------------------------------
* Finished readme and help.

inspect 7.x-0.9.11, 2011-10-10
------------------------------
* Renamed module etc. to inspect, due to naming conflict (someone else registered the dump namespace ;-)

dump 7.x-0.9.10, 2011-10-09
---------------------------
* Check for key password extended to also check for key pass (user objects use pass, whereas database settings use password)

dump 7.x-0.9.9, 2011-09-26
--------------------------
* Moved the permission administer dump to top of permissions

dump 7.x-0.9.8, 2011-09-26
--------------------------
* Added boolean .pmuD property to fronted Dump singleton, usable for checking if Dump exists
* Fixed wrong arguments in frontend Dump.traceLog()
* Fixed stack tracer

dump 7.x-0.9.7, 2011-09-21
--------------------------
* Fixed more wrong options category var for DumpTrace methods

dump 7.x-0.9.7, 2011-09-18
--------------------------
* Added session hash and log number to logs
* Fixed wrong options category var for DumpTrace log methods

dump 7.x-0.9.7, 2011-09-16
--------------------------
* Fixed quoting error for $_docRoot
* Added logging category option
* Changed default dumping depth to 10 (except for tracing)
* Fixed tracing for old depth pattern (depth zero ~ hide values, new pattern works with hide_scalars)
* Changed version number

dump 7.x-0.9.6, 2011-09-15
--------------------------
* Added simple loggin method

dump 7.x-0.9.6, 2011-09-14
--------------------------
* Fixed bad iteration error for object (iterating by explicit reference)

dump 7.x-0.9.6, 2011-09-12
--------------------------
* Fixed missing reference to self::$_permissions
* Eliminating weird depth option (zero meant hide values), and introduced hide scalars option - except for DumpTrace

dump 7.x-0.9.6, 2011-09-07
--------------------------
* Fixed bad error in frontend array bucket resolving.

dump 7.x-0.9.5, 2011-09-04
--------------------------
* Introduced private initialized flag in backend Dump, to reduce the risk
of methods of custom extending class calling _dump() without having
permissions and options checked.
* Declared DumpVar and DumpTrace final, to reduce the risk of malicious
ad hoc extensions.
* Created configuration form,
including option to forbid admin user 1 getting dumps and traces.
* Created help page.
* Rewrote the readme text.

dump 7.x-0.9.4, 2011-09-03
--------------------------
Added frontend javascript library.

dump 7.x-0.9.3, 2011-08-29
--------------------------
Document root removal from strings and filenames.

dump 7.x-0.9.2, 2011-08-25
--------------------------

dump 7.x-0.9.1, 2011-08-23
--------------------------
Enabled Trace logging

dump 7.x-0.9, 2011-08-18
------------------------
Started
