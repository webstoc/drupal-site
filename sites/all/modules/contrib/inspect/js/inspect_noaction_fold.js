/**
 * @name window
 * @type {obj} ms
 */
if(window.Inspect === undefined) {
  //  jQuery not required, except for output folding/toggling (which doesnt work, but nor fails, without jQuery).
  (function($) {
    var cls = "module-inspect-collapsible", shw = 1, fld = -1;
    /**
     * Noaction inspector.
     * @function
     */
    window.Inspect = (function() {
      /**
       * @name Inspect
       * @constructor
       * @class
       * @singleton
       */
      var
      f = function() {
      };
      f.tcepsnI = 1; // true for real inspect
      f.console = function() {
      };
      f.typeOf = function() {
        return "";
      };
      f.variable = function() {
      };
      f.variableGet = function() {
        return "";
      };
      f.trace = function() {
      };
      f.traceGet = function() {
        return "";
      };
      f.argsGet = function() {
        return "";
      };
      /**
       * Toggle display of all inspection outputs on current page.
       * This function is always implemented, even for no-action no-fold.
       * @function
       * @name Inspect.toggleOutput
       * @return {void}
       */
      f.toggleDisplay = function() {
        var u;
        if(shw > 0) {
          $("." + cls).hide();
        }
        else if(fld < 0) {
          $("pre."+ cls).show();
        }
        else {
          $("pre."+ cls).each(function(){
            $((u = $(this).prev("div."+ cls).get(0)) ? u : this).show();
          });
        }
        shw *= -1;
      };
      /**
       * Toggle inspection folding, if folding is enabled.
       * Folding is not supported for IE<9.
       * @function
       * @name Inspect.toggleFolding
       * @return {void}
       */
      f.toggleFolding = function() {
      };
      return f;
    })();
    if(window.inspect === undefined) {
      window.inspect = window.Inspect;
    }
    //  fold output ------------------------------
    if(!$ ||
        ($.browser.msie && parseFloat($.browser.version.replace(/^(\d+)\..+/, "$1")) < 9)) { // no good in IE<9
      return;
    }
    $(document).bind("ready", function(){
      fld = 1;
      window.Inspect.toggleFolding = function() {
        if(fld > 0) {
          $("div." + cls).hide();
          $("pre." + cls).show();
        }
        else {
          $("pre." + cls).hide();
          $("div." + cls).show();
        }
        fld *= -1;
        shw = 1;
      };
      var linesMax = 10000,
      nl = "<br/>", ds = "<div>", de = "</div>", sp = "<span>", se = "</span>",
      ps = "<div class=\"inspect-cllpsbl cllpsd\"><div class=\"inspect-prnt\">",
      b1 = "<span class=\"inspect-xpnd\">\u25b6" + se,
      b2 = "<span class=\"inspect-xpnd-all\">\u25b6\u25b6" + se,
      cs = "<div class=\"inspect-chldrn\">",
      cc = "<div class=\"inspect-chld\">",
      kp = "<span class=\"inspect-kypth\" title=\"",
      k1 = "\">" + sp, // span for measuring width
      k2 = se + "<form><div><input type=\"text\" value=\"",
      k3 = "\"/><div title=\"close\">&#215;</div></div></form>",
      sw = "<span class=\"warn\">",
      st = "<span class=\"trunc\">",
      tglCls = "module-inspect-toggle",
      rprts = location.href.indexOf("admin/reports/event/") > -1,
      fTgl = window.inspect[ "toggle" + (rprts ? "Folding" : "Display") ],
      tg = "<div class=\"" + tglCls + "\"><div title=\"toggle " + (rprts ? "folding" : "display") + "\">&#0161;!" + de + de,
      font = "font-family:Menlo,Consolas,Andale Mono,Lucida Console,Nimbus Mono L,DejaVu Sans Mono,monospace,Courier New;",
      ndntRgx = /^(\.\s\s)?(\.\s\s)?(\.\s\s)?(\.\s\s)?(\.\s\s)?(\.\s\s)?(\.\s\s)?(\.\s\s)?(\.\s\s)?(\.\s\s)?.*$/,
      ndntRpl = "$1$2$3$4$5$6$7$8$9$10",
      trprRgx = /\(string\:(\d+)\|(\d+)\|(\d+)\|\!\) &gt;&gt;\.{3}(.*)\.{3}&lt;&lt;$/,
      trRgx = /\(string\:(\d+)\|(\d+)\|(\d+)\) &gt;&gt;(.*)\.{3}&lt;&lt;$/,
      prRgx = /\(string\:(\d+)\|(\d+)\|\-\|\!\) &gt;&gt;\.{3}(.*)&lt;&lt;$/,
      ctRgx = /\(([a-zA-Z_][a-zA-Z\d_]*)\:(\d+)\) (\[\.{3}\]|\{\.{3}\})/,
      erRgx = /\(([a-zA-Z_][a-zA-Z\d_]*)(\:)?([\d\?\|]+)*\) \*([A-Z\d_]+)\*/,
      first = true,
      prfCls = "inspect-profile", // profiler
      /** Toggle key path display.
       * @return {void}
       */
      tglKyPth = function() {
        var jqFrm = $("form", this), elm, w;
        if(jqFrm.css("display") === "none") {
          elm = ($("input", this).
              unbind("click"). // remove slctKyPth
              click(slctKyPth).
              css("width", (w = $("span", this).innerWidth()) + "px")).get(0);
          $("form>div", this).css("width", (w + 16) + "px");
          jqFrm.css("display", "inline");
          $(this).click(tglKyPth); // enable single click close
          elm.focus(); // unselect double clicked word
        }
        else {
          jqFrm.css("display", "none");
          $(this).unbind("click"); // prevent single click open
        }
      },
      slctKyPth = function(e) {
        e.stopPropagation();
        this.select();
        $(this).unbind("click", slctKyPth). // prevent continuous selection
            click(function(e){e.stopPropagation();}); // prevent single click close (on input, not form)
      },
      /**
       * Folding of backend generated dump outputs on page.
       * @ignore
       * @param {arr} lines
       * @param {int} first
       * @param {int} last
       * @param {arr} path
       * @return {arr|false}
       *  - [ (str) buffer, (int) first, (bool|undef) deep ]
       */
      fold = function(lines, first, last, path, top) {
        var start = first, j = 0, buf = "", line, obj, arr, i, stop, ndnt, a, deep, pth = path.concat(), kyPth;
        while(start <= last && (++j) < 10000) { // not linesMax, simply loop limiter
          obj = arr = stop = false;
          line = lines[start];
          //  find bucket name
          pth.push(
              (!/^\.\s\s/.test(line) ? "" : line.replace(/^(\.\s\s)+(.*?)\:\s\(.+$/, "$2")) +
              (!/\[\'$/.test(pth[pth.length - 1]) ? "" : "']")
          );
          if((obj = /\x7B$/.test(line)) || (arr = /\x5B$/.test(line))) { // ends with { or [
            if(last === start) {
              return false; // container start and end cant be on same line
            }
            deep = true;
            //  add container type
            pth[ pth.length - 1 ] += ((obj && !/\(arra[y]\:/.test(line)) ? "->" : "['");

            buf += ps +
                (ndnt = line.indexOf(".  ") !== 0 ? "" : line.replace(ndntRgx, ndntRpl)) + // remove indentation
                b1;
            ++start;
            //  find end
            for(i = start; i < last + 1; i++) {
              if((obj && lines[i] === ndnt + "}") || (arr && lines[i] === ndnt + "]")) {
                stop = i;
                if(!(a = fold(lines, start, stop - 1, pth))) {
                  return false; // error
                }
                buf += (!a[2] ? "" : b2) + "&nbsp;" +
                    (!top ?
                      (kp + (kyPth = pth.join("").replace(/\[\'$|\-\>$/, "").replace(/\[\'(\d+)\'\]/g, "[$1]")) +
                        k1 + kyPth + k2 + kyPth + k3) :
                      sp // no key path for top var
                    ) +
                    line.replace(/^(\.  )+/, "") + se +
                    de + cs +
                    a[0] +
                    cc +
                    lines[i] +
                    de + de + de;
                pth.pop();
                start = a[1] + 1; // 1 to prevent double ending
                break;
              }
            }
            if(!stop) {
              return false; // error, found no ending } or ]
            }
          }
          else { // non-container var
            ++start;
            ndnt = line.indexOf(".  ") !== 0 ? "" : line.replace(ndntRgx, ndntRpl);
            buf += cc +
              (ndnt = line.indexOf(".  ") !== 0 ? "" : line.replace(ndntRgx, ndntRpl)) + // remove indentation
              (!top ?
                (kp + (kyPth = pth.join("").replace(/\[\'(\d+)\'\]/g, "[$1]")) + k1 + kyPth + k2 + kyPth + k3) :
                (!/^Inspection aborted/.test(line) ? sp : sw) // no key path for top var
              ) +
              line.replace(/^(\.  )+/, "").
                  replace( // highlight string truncation and path removal
                      trprRgx,
                      "(string:$1|$2|" + st + "$3|!" + se + ") " + st + "&gt;&gt;..." + se + "$4" + st + "...&lt;&lt;" + st
                  ).
                  replace( // highlight string truncation
                      trRgx,
                      "(string:$1|$2|" + st + "$3" + se + ") " + st + "&gt;&gt;" + se + "$4" + st + "...&lt;&lt;" + st
                  ).
                  replace( // highlight path removal (non-truncated)
                      prRgx,
                      "(string:$1|$2|-|" + st + "!" + se + ") " + st + "&gt;&gt;..." + se + "$3" + st + "&lt;&lt;" + st
                  ).
                  replace(ctRgx, "($1:$2) " + st + "$3" + se). // highlight container truncation
                  replace(erRgx, "($1$2$3) " + sw + "*$4*" + se). // highlight error
                  replace(/_NL_/g, "_NL_<br/>") +
                  se + de;
            pth.pop();
          }
        }
        return [buf, start, deep];
      },
      prof = function(lines) {
        var s = "", le = lines.length, i, line, j = 0, ttl;
        for(i = 0; i < le; i++) {
          if(/^.+\|/.test(line = lines[i])) {
            //  truncate log, set as title
            if((ttl = !/^([\s\d]{0,4}\d)/.test(line) || /.+\|\s$/.test(line) ? "" : line.replace(/^.+\|\s([^\|]+)$/, "$1"))) {
              line = line.replace(/^(.+\|\s)[^\|]+$/, "$1") + ttl.substr(0, 32);
              ttl = " title=\"" + ttl + "\"";
            }
            s += "<div class=\"inspect-" + (((++j) % 2) ? "odd" : "even") + "\"" + ttl + ">"; // even/odd
          }
          else {
            j = 0;
            s += ds;
          }
          s += line.replace(/\s/g, "\u00A0") + de;
        }
        return s;
      },
      t;
      //  goes thru all outputs, adds div.module-inspect-collapsible, hides pre.module-inspect-collapsible
      $("pre." + cls).each(function(){
        var buf = "", jq = $(this), lines = jq.html().replace(/\r\n/, "\n").split(/\n/), le = lines.length,
          i, j = -1, a, css = "", nm = "", prf = false;
        //  filter message (everything before [Inspect)
        for(i = 0; i < le; i++) {
          if(/^\[Inspect/.test(lines[i])) {
            j = i;
            nm = (lines[i]).replace(/^\[Inspect[^\]]+\]\s?(.+)?/, "$1");
            break;
          }
        }
        if(j === -1) {
          return; // error
        }
        //  add lines before [Inspect...
        for(i = 0; i < j; i++) {
          buf += (i == 0 ? "" : nl) + lines[i];
        }
        //  remove lines before [Inspect...
        lines.splice(0, j);
        buf += ds + lines[0] + de; // [Inspect...
        //  inspect profile: dont fold
        if(jq.hasClass(prfCls)) {
          prf = true;
          lines.splice(0, 1);
          buf += prof(lines);
        }
        else {
          //  remove file_line
          if(lines.length > 1 && // content truncation (Read more) may shorten output to a single line
              lines[1].indexOf("@") === 0) {
            buf += ds + lines[1] + de;
            lines.splice(1, 1);
          }
          if((le = lines.length) <= linesMax) {
            if(!(a = fold(lines, 1, le - 1, [nm], true))) {
              return; // error
            }
            buf += a[0];// + de;
          }
        }
        if(first) {
          first = false;
          css = "<style type=\"text/css\">" +
"div." + tglCls + "{position:relative;display:block;background:#FFF;font-size:1px;}" +
"div." + tglCls + " div{position:absolute;display:inline-block;left:-2px;top:-2px;padding:0 2px;line-height:100%;cursor:pointer;" +
"background:#FFF;border-radius:2px;border:1px solid #CC6060;box-shadow:0 0 10px #CC0000;-moz-box-shadow:0 0 6px #CC0000;" + font +
"text-align:center;vertical-align:top;font-size:10px;font-weight:bold;color:#000;" +
"-moz-user-select:none;-khtml-user-select:none;-ms-user-select:none;}" +
"div." + tglCls + " div:hover{color:#FFF;background:#666;}" +
"div." + cls + "{display:block;background:#FFF;padding:7px;border:1px solid #999;font-size:11px;" + font + "}" +
"div.inspect-prnt:hover,div.inspect-chld:hover{background:#EEE;}" +
"div.inspect-chld span.warn{color:#CC0000;}div.inspect-chld span.trunc{color:#0000FF;}" +
"span.inspect-xpnd,span.inspect-xpnd-all{display:inline-block;cursor:pointer;color:#999;padding:0 2px;}" +
"span.inspect-xpnd.hover,span.inspect-xpnd-all.hover{color:#000;}div.inspect-cllpsbl.cllpsd div.inspect-chldrn{display:none}" +
"span.inspect-kypth,span.inspect-kypth span,span.inspect-kypth form,span.inspect-kypth div,span.inspect-kypth input{" +
"background:transparent;border:0;padding:0;margin:0;font-size:11px;" + font + "}" +
"span.inspect-kypth{cursor:pointer;}" +
"span.inspect-kypth:hover{background:#D8E8F7;}" +
"span.inspect-kypth span{position:absolute;visibility:hidden;}" +
"span.inspect-kypth span.trunc{position:static;visibility:visible;color:#0000FF;}" +
"span.inspect-kypth span.warn{position:static;visibility:visible;color:#CC0000;}" +
"span.inspect-kypth form{display:none;position:relative;}" +
"span.inspect-kypth form>div{display:inline-block;position:absolute;box-shadow:0 0 3px #2F78C0;background:#94B8D8;border-radius:2px;}" +
"span.inspect-kypth input{padding:0px 1px;border-radius:2px;background:#FFF;}" +
"span.inspect-kypth div div{display:inline-block;" +
"color:#FFF;font-weight:bold;font-size:12px;line-height:100%;width:10px;text-align:right;background:transparent;}" +
"span.inspect-kypth div div:hover{color:#000;}" +
"div." + prfCls + " div{white-space:nowrap;}" +
"div." + prfCls + " div.inspect-even{background:#EEE;}div." + prfCls + " div.inspect-even:hover{background:#D8E8F7;}" +
"div." + prfCls + " div.inspect-odd:hover{background:#ECF4FB;}" +
"</style>";
        }
        jq.before(
            css + tg +
            //  no div.module-inspect-collapsible if no folding
            (le <= linesMax ? ("<div class=\"" + cls + (!prf ? "" : (" " + prfCls)) + "\">" + buf + de) : "")
          );
        if(le <= linesMax) {
          jq.hide();
        }
        else {
          jq.prepend("   * Folding off, number of output lines " + le + " exceeds max " + linesMax + ", try less depth *\n");
        }
      });
      //  adds button events, and locks width
      t = setTimeout(function(){
        //  set .toggleFolding() or .toggleDisplay() on
        $("div." + tglCls + " div").click(fTgl);
        //  fixate width of expansible inspection outputs' outer container
        $("div." + cls).each(function(){
          var jq = $(this), w, p;
          if(!jq.hasClass(prfCls)) {
            jq.css({
              width: w = (jq.innerWidth() - 16) + "px",
              "max-width": w
            });
          }
          else if((p = jq.css("position")) !== "absolute" && p !== "fixed") { // force optimal width
            jq.css("position", "absolute");
            w = (jq.innerWidth() - 8) + "px";
            jq.css({
              position: p,
              width: w,
              "max-width": "auto"
            });
          }
        });
        //  for each collapsible: set expand/collapse events on all buttons
        $("div.inspect-cllpsbl").each(function(){
          var par = this,
            bt = $("span.inspect-xpnd", par).get(0), btAll = $("span.inspect-xpnd-all", par).get(0),
            btChldrn = [], chld;
          $("div.inspect-cllpsbl", par).each(function(){
            if((chld = $("span.inspect-xpnd", this).get(0))) {
              btChldrn.push(chld);
            }
          });
          $(bt).click(function(event, on, off){
            var jq = $(par), show = on ? true : (off ? false : jq.hasClass("cllpsd"));
            $(this).text(show ? "\u25bc" : "\u25b6");
            jq[show ? "removeClass" : "addClass"]("cllpsd");
            if(!show && btAll) { // if hide: collapse sibling button
              $(btAll).text("\u25b6\u25b6");
            }
          }).mouseover(function(){
            $(this).addClass("hover");
          }).mouseout(function(){
            $(this).removeClass("hover");
          });
          //  works via the single level buttons
          if(btAll) {
            $(btAll).click(function(){
              var show = $(this).text() === "\u25b6\u25b6" ? true : false;
              if(show) {
                $(this).text("\u25bc\u25bc");
              }
              $(bt).trigger("click", [show, !show]); // click sibling
              $(btChldrn).each(function(){ // click children
                var btAll; // local
                $(this).trigger("click", [show, !show]);
                //  if has expand-all sibling, make it show that all is expanded/collapsed
                if((btAll = $(this).next("span.inspect-xpnd-all").get(0))) {
                  $(btAll).text(show ? "\u25bc\u25bc" : "\u25b6\u25b6");
                }
              });
            }).mouseover(function(){
              $(this).addClass("hover");
            }).mouseout(function(){
              $(this).removeClass("hover");
            });
          }
        });
        //  add show key path behaviour
        $("span.inspect-kypth").dblclick(tglKyPth).bind("contextmenu", function(e){
          e.preventDefault();
          tglKyPth.apply(this, []);
        });
      },100);
    });
  })(jQuery);
}