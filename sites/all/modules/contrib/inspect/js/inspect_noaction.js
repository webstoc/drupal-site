/**
 * @name window
 * @type {obj} ms
 */
if(window.Inspect === undefined) {
  //  jQuery not required, except for output folding/toggling (which doesnt work, but nor fails, without jQuery).
  (function($) {
    var cls = "module-inspect-collapsible", shw = 1, fld = -1;
    /**
     * Noaction inspector.
     * @function
     */
    window.Inspect = (function() {
      /**
       * @name Inspect
       * @constructor
       * @class
       * @singleton
       */
      var
      f = function() {
      };
      f.tcepsnI = 1; // true for real inspect
      f.console = function() {
      };
      f.typeOf = function() {
        return "";
      };
      f.variable = function() {
      };
      f.variableGet = function() {
        return "";
      };
      f.trace = function() {
      };
      f.traceGet = function() {
        return "";
      };
      f.argsGet = function() {
        return "";
      };
      /**
       * Toggle display of all inspection outputs on current page.
       * This function is always implemented, even for no-action no-fold.
       * @function
       * @name Inspect.toggleOutput
       * @return {void}
       */
      f.toggleDisplay = function() {
        var u;
        if(shw > 0) {
          $("." + cls).hide();
        }
        else if(fld < 0) {
          $("pre."+ cls).show();
        }
        else {
          $("pre."+ cls).each(function(){
            $((u = $(this).prev("div."+ cls).get(0)) ? u : this).show();
          });
        }
        shw *= -1;
      };
      /**
       * Toggle inspection folding, if folding is enabled.
       * Folding is not supported for IE<9.
       * @function
       * @name Inspect.toggleFolding
       * @return {void}
       */
      f.toggleFolding = function() {
      };
      return f;
    })();
    if(window.inspect === undefined) {
      window.inspect = window.Inspect;
    }
  })(jQuery);
}